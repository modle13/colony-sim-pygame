#!/usr/bin/env python

"""
adapted from https://realpython.com/pygame-a-primer
"""

from math import hypot
import pygame
import random

# Import pygame.locals for easier access to key coordinates
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_w,
    K_a,
    K_s,
    K_d,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

# Define constants for the screen width and height
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
WHITE = (255, 255, 255)
RED = (255, 0, 0)
NUM_OBSTACLES = 40
NUM_TILES = 1


"""
draw line between player and target
have line update each frame
have line adjust around obstacles
have target move along line toward player
"""

class Game():
    def __init__(self):
        # Setup the clock for a decent framerate
        self.clock = pygame.time.Clock()
        # Create the screen object
        self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
        self.events = Events()
        # Create groups to hold enemy sprites and all sprites
        # - all_sprites is used for rendering
        self.all_sprites = pygame.sprite.Group()
        self.tile_sprites = pygame.sprite.Group()
        self.obstacle_sprites = pygame.sprite.Group()
        # - enemies is used for collision detection and position updates
        self.player = Player()
        self.all_sprites.add(self.player)
        self.generate_tiles()
        self.generate_obstacles()
        #self.line = Line(self.player.rect.center, self.target.rect.center)

    def generate_tiles(self):
        for i in range(NUM_TILES):
            tile = Tile()
            self.all_sprites.add(tile)
            self.tile_sprites.add(tile)

    def generate_obstacles(self):
        for i in range(NUM_OBSTACLES):
            obstacle = Obstacle()
            self.all_sprites.add(obstacle)
            self.obstacle_sprites.add(obstacle)

    def update_frame(self):
        pygame.display.flip()
        self.clock.tick(30)

    def run(self):
        self.events.update()
        # Get the set of keys pressed and check for user input
        pressed_keys = pygame.key.get_pressed()
        # Update the player sprite based on user keypresses
        self.player.update(pressed_keys)

        for entry in self.tile_sprites:
            entry.move()

        self.draw()

        self.update_frame()

    def draw(self):
        # Fill the screen with black
        self.screen.fill((0, 0, 0))
        # Draw all sprites
        for entity in self.all_sprites:
            self.screen.blit(entity.surf, entity.rect)
            if hasattr(entity, 'draw_line'):
                entity.draw_line(self.player.rect.center, self.obstacle_sprites)


class Events():
    def update(self):
        # Look at every event in the queue
        for event in pygame.event.get():
            # Did the user hit the escape key?
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                game.running = False

            # Did the user click the window close button? If so, stop the loop.
            elif event.type == QUIT:
                game.running = False


class Obstacle(pygame.sprite.Sprite):
    def __init__(self):
        super(Obstacle, self).__init__()
        self.surf = pygame.Surface((15, 15))
        self.surf.fill(RED)
        self.rect = self.surf.get_rect(center=(random.randrange(SCREEN_WIDTH), random.randrange(SCREEN_HEIGHT)))


class Tile(pygame.sprite.Sprite):
    def __init__(self):
        super(Tile, self).__init__()
        self.surf = pygame.Surface((10, 10))
        self.surf.fill(WHITE)
        self.rect = self.surf.get_rect(center=(random.randrange(SCREEN_WIDTH), random.randrange(SCREEN_HEIGHT)))
        self.line_segments = []

    def draw_line(self, other, obstacles):
        self.line_segments = [self.rect.center]
        self.check_for_obstacles(self.rect.center, other, obstacles)

        for i, entry in enumerate(self.line_segments):
            try:
                first = self.line_segments[i]
                second = self.line_segments[i + 1]
            except IndexError:
                continue
            pygame.draw.line(game.screen, WHITE, first, second, width=1)

    def move(self):
        try:
            first = self.line_segments[0]
            second = self.line_segments[1]
            xf, yf = first
            xs, ys = second
            distance = hypot(xs - xf, ys - yf)
            third = None
            if len(self.line_segments) > 2:
                third = self.line_segments[2]
            target = third if distance < 5 else second
            if not target:
                return
            self.move_toward_target(self, target)
        except IndexError:
            return

    def check_for_obstacles(self, start, end, obstacles):
        completed = False
        current_start = start
        adjusted_end = end
        count = 0
        while count < 100:
            for obstacle in obstacles:
                clipped = obstacle.rect.clipline(current_start, end)
                if clipped:
                    xs, ys = current_start
                    x1, y1 = clipped[0]
                    x2, y2 = clipped[1]
                    # move clip position away from clip rect
                    x1 += (5 if x1 < xs else -5)
                    y1 += (5 if y1 < ys else -5)
                    # calculate line perpendicular to clip line through rect
                    # this is a line from origin
                    perp = (-y2 + y1, x2 - x1)
                    # offset perpendicular line to clip position
                    # and give it some breathing room
                    adjusted_end = (1.5 * perp[0] + x1, 1.5 * perp[1] + y1)

                    # add the clip point and the new end point to the line_segments list
                    self.line_segments.append((x1, y1))
                    self.line_segments.append(adjusted_end)
                    current_start = adjusted_end
                else:
                    continue
            count += 1
        self.line_segments.append(end)
    
    def move_toward_target(self, obj, target):
        # Move the sprite based on speed
        x_speed, y_speed = self.get_relative_speed(obj, target)
        xo, yo = obj.rect.center
        xt, yt = target
        if xo < xt:
            obj.rect.move_ip(x_speed, 0)
        elif xo > xt:
            obj.rect.move_ip(-x_speed, 0)
        if yo < yt:
            obj.rect.move_ip(0, y_speed)
        elif yo > yt:
            obj.rect.move_ip(0, -y_speed)

    def get_relative_speed(self, obj, target):
        speed = 2
    
        # possibly better way to achieve this with use of hypot function
        x_src, y_src = obj.rect.center
        x_tgt, y_tgt = target
        # get the diffs
        x_diff = abs(x_src - x_tgt)
        y_diff = abs(y_src - y_tgt)
        # calculate the speed ratio
        x_ratio = 1
        y_ratio = 1
        if x_diff and y_diff and x_diff != y_diff:
            y_ratio = y_diff / x_diff
            # x ratio is just the inverse of y ratio
            x_ratio = 1 / y_ratio
    
        # calculate the speed
        y_speed = speed if y_ratio > 1 else y_ratio * speed
        x_speed = speed if x_ratio > 1 else x_ratio * speed
        return x_speed, y_speed


# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.Surface((10, 10))
        self.surf.fill(WHITE)
        self.rect = self.surf.get_rect()
        #self.speed = 30
        self.speed = 5

    # Move the sprite based on user keypresses
    def update(self, pressed_keys):
        if pressed_keys[K_UP] or pressed_keys[K_w]:
            self.rect.move_ip(0, -self.speed)
        if pressed_keys[K_DOWN] or pressed_keys[K_s]:
            self.rect.move_ip(0, self.speed)
        if pressed_keys[K_LEFT] or pressed_keys[K_a]:
            self.rect.move_ip(-self.speed, 0)
        if pressed_keys[K_RIGHT] or pressed_keys[K_d]:
            self.rect.move_ip(self.speed, 0)
        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT


def init_game():
    # Setup for sounds. Defaults are good.
    pygame.mixer.init()

    # Initialize pygame
    pygame.init()

    game = Game()
    return game


if __name__ == '__main__':
    game = init_game()
    # Variable to keep the main loop running
    game.running = True
    # Main loop
    while game.running:
        game.run()
    game.quit()


