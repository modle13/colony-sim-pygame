#!/usr/bin/env python

"""
adapted from https://realpython.com/pygame-a-primer
"""

import pygame
import random

# Import pygame.locals for easier access to key coordinates
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_w,
    K_a,
    K_s,
    K_d,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

# Define constants for the screen width and height
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
WHITE = (255, 255, 255)


"""
draw line between player and target
have line update each frame
have line adjust around obstacles
have target move along line toward player
"""

class Game():
    def __init__(self):
        # Setup the clock for a decent framerate
        self.clock = pygame.time.Clock()
        # Create the screen object
        self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
        self.events = Events()
        # Create groups to hold enemy sprites and all sprites
        # - all_sprites is used for rendering
        self.all_sprites = pygame.sprite.Group()
        # - enemies is used for collision detection and position updates
        self.player = Player()
        self.all_sprites.add(self.player)
        self.target = Tile()
        self.all_sprites.add(self.target)
        #self.line = Line(self.player.rect.center, self.target.rect.center)
        self.line = Line()
        self.all_sprites.add(self.line)

    def update_frame(self):
        pygame.display.flip()
        self.clock.tick(30)

    def run(self):
        self.events.update()
        # Get the set of keys pressed and check for user input
        pressed_keys = pygame.key.get_pressed()
        # Update the player sprite based on user keypresses
        self.player.update(pressed_keys)
        self.line.update(self.player.rect.center, self.target.rect.center)

        self.draw()

        self.update_frame()

    def draw(self):
        # Fill the screen with black
        self.screen.fill((0, 0, 0))
        # Draw all sprites
        for entity in self.all_sprites:
            self.screen.blit(entity.surf, entity.rect)


class Events():
    def update(self):
        # Look at every event in the queue
        for event in pygame.event.get():
            # Did the user hit the escape key?
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                game.running = False

            # Did the user click the window close button? If so, stop the loop.
            elif event.type == QUIT:
                game.running = False


class Line(pygame.sprite.Sprite):
    def __init__(self, width=1):
        super(Line, self).__init__()
        self.width = width

    def update(self, player, target):
        player_x, player_y = player
        target_x, target_y = target
        x = target_x - player_x
        y = target_y - player_y
        topleft_x = player_x if x > 0 else target_x
        topleft_y = player_y if y > 0 else target_y
        surf_topleft = (topleft_x, topleft_y)
        self.surf = pygame.Surface((abs(x), abs(y)), pygame.SRCALPHA)
        #self.surf.fill((255, 255, 255, 50))
        self.rect = self.surf.get_rect(topleft=surf_topleft)
        start_x = 0 if x > 0 else -x
        start_y = 0 if y > 0 else -y
        end_x   = x if not start_x else 0
        end_y   = y if not start_y else 0
        start   = (start_x, start_y)
        end     = (end_x  , end_y  )
        pygame.draw.line(self.surf, WHITE, start, end, width=self.width)


class Tile(pygame.sprite.Sprite):
    def __init__(self):
        super(Tile, self).__init__()
        self.surf = pygame.Surface((10, 10))
        self.surf.fill(WHITE)
        self.rect = self.surf.get_rect(center=(400, 300))


# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.Surface((10, 10))
        self.surf.fill(WHITE)
        self.rect = self.surf.get_rect()
        self.speed = 30

    # Move the sprite based on user keypresses
    def update(self, pressed_keys):
        if pressed_keys[K_UP] or pressed_keys[K_w]:
            self.rect.move_ip(0, -self.speed)
        if pressed_keys[K_DOWN] or pressed_keys[K_s]:
            self.rect.move_ip(0, self.speed)
        if pressed_keys[K_LEFT] or pressed_keys[K_a]:
            self.rect.move_ip(-self.speed, 0)
        if pressed_keys[K_RIGHT] or pressed_keys[K_d]:
            self.rect.move_ip(self.speed, 0)
        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT


def init_game():
    # Setup for sounds. Defaults are good.
    pygame.mixer.init()

    # Initialize pygame
    pygame.init()

    game = Game()
    return game


if __name__ == '__main__':
    game = init_game()
    # Variable to keep the main loop running
    game.running = True
    # Main loop
    while game.running:
        game.run()
    game.quit()
