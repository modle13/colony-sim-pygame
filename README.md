# TABLE OF CONTENTS

[[_TOC_]]

# Overview

A `pygame` colony sim with minimalist graphics.

If you're not familiar with colonysims, here are some examples:
* [SimAnt](https://en.wikipedia.org/wiki/SimAnt), A classic you can find on the web to play for free
* [Banished](https://store.steampowered.com/app/242920/Banished/), solid but feels somewhat unfinished and limited
* [Autonauts](https://store.steampowered.com/app/979120/Autonauts/), scratch programming meets super chill and gentle gameplay, no survival concerns
* [Planetbase](https://store.steampowered.com/app/403190/Planetbase/), an unforgiving foray into remote planet base building

The basic premise is generally: issue commands to have your colonists perform actions like gathering resources, producing goods, building stuff, with some stat or set of stats that need to be satisfied for their continued survival. Depending on the level of control and the type of sim, balance can be critical. For example, overpopulation can mean everyone starves, and the really harsh sims will let you set up unsurvivable scenarios that you don't detect for a long time, until it's too late to recover.

The goals for this game are currently vague, but will likely trend toward middling levels of forgiveness, where survival is still a critical element.

# Contributing

1. Grab an issue from the planning column of the [issue board](https://gitlab.com/modle13/colony-sim-pygame/-/boards).
2. On the issue details page, create a new branch and a draft merge request with the 'Create merge request' button.
	1. This automatically creates a feature branch with `$ISSUE_NUM-title-of-issue`
3. Check out the new feature branch.
4. When your changes are ready, click 'Mark as ready' on the merge request.
5. Propose features!

# The components of a game

A game is not like a normal script.

It's simple to make a `while True:` loop and cram all your code inside, but you will end up sad. The sadness arises when there are more instructions than can be processed within your desired frame duration, causing frames to be delayed, which results in jittering and lag.

Some engines, such as pygame, deal with this frame overflow via events and coroutines. An action can be recorded as an event, which will mutate a state, and processing such as drawing can be deferred. With a coroutine, the game only processes instructions between frame loads, which eliminates the frame-to-frame slowdown with long instructions.

## The `game` object

This object is instantiated when `main.py` loads and is attached to the `settings.config` object, which is imported into all modules.

All sprite collections, game objects (such as inventory and reticle), and high level state (such as UI toggles) are accessible via this object.

## Rendering sprites with pygame's `blit` function

Pygame provides a `display` object, which is the layer of the game that every other sprite will get drawn onto. `blit` is a function on the `pygame.display` object that facilitates that drawing.

`blit` can also be used to layer one sprite onto another, using relative positioning.

## The '`camera`'

Camera management in pygame is a bit strange, and not strictly necessary unless a pannable effect is desired. There's no built-in function for this.

The position of the 'Camera' is really just the position of some focal point, such as a player sprite, or in this case, a 'reticle' sprite that doesn't get drawn. This focal point can be moved through space with directional keys.

The top left of the screen is the origin, with `x` increasing to the right, and `y` increasing downward. This is mind-bending if you're used to thinking in cartesian terms. The maximum available values for `x` and `y` are the screen width and height, respectively, in pixels. This means only objects within this range will ever be visible, so if we want to 'see' areas outside this range, we need to bring those areas into the range with offset math.

To achieve a panning camera effect, the current position of the reticle or player is applied to the camera object during the camera's update function. Then, all other sprites are drawn relative to the camera's position.

Don't worry too much about it, just be sure to call `camera.apply` on the target entity to get the updated position, and pass that position into the `screen.blit` function.

## The main loop

`main.py`
```
if __name__ == '__main__':
    game = init_game()
    # Main loop
    while game.running:
        game.run()
    game.quit()
```

The main loop operations are:
1. event processing (`modules/events.py`)
2. sprite updates (tells sprites to update their properties via their `update` functions)
3. state collection (such as keyboard and mouse presses)
4. sprite draws (draws all sprites and text on the display; the frame doesn't get shown yet)
5. frame update (makes the frame visible and resets the frame timer)

This is where sprites get updated and drawn. Try to avoid creating events or mutating object state directly in the main loop functions. Instead, perform those operations in the sprites' and `Event`'s `update` functions.

## Events

### creating events

Custom events should be given an ID. This can be defined in the `Events` object's `__init__`.

All built-in events have a designated ID, but custom events can be given an ID from numbers starting at `pygame.USEREVENT` to avoid colliding with built-in IDs, like so:

```
self.ADDCOLONIST = pygame.USEREVENT + 1
```

The Events `object` is instantiated on the main `game` object, which is passed around via the `settings.config` object, so your ID can be accessed thusly: `config.game.events.ADDCOLONIST`.

Once you have an ID you can build a custom event with `pygame.event.Event`.

In this example the instantiation of an object (The `Product` here will be a log from a tree) is deferred to the main loop (when events are processed) by creating the event from the `Resource` object once the resource harvesting is complete. The parameters for `Product`'s instantiation are passed along as kwargs.

```
my_event = pygame.event.Event(config.game.events.ADDPRODUCT, product=Product, product_type='log', position=(x, y), quantity=3)
# post event to event queue
pygame.event.post(my_event)
```

### processing events

Events are processed as the first operation of the main loop, in `game.run()`.

```
def run(self):
    self.events.update()
	...
```

The `Events.update` function calls `pygame.event.get` to retrieve the list of unprocessed events. This will include built-in events, such as keypresses and mouse clicks. `update` uses a big 'ol `if`/`elif` chain (this could be sped up by using a `dict` of functions and lambdas).

The first param passed to `pygame.event.Event` will be stored in the `Event.type` param, so we can check for our custom event like so (this just checks to see if we have room for more colonists, instantiates a new sprite object if so, and drops it into the relevent sprite collections; it doesn't worry about drawing it to the surface or updating its state--those operations are handled by the main loop):

```
elif event.type == self.ADDCOLONIST:
	if len(config.game.colonists) < config.max_colonists:
		new_colonist = Colonist()
		config.game.colonists.add(new_colonist)
		config.game.all_sprites.add(new_colonist)
```

Other examples of events are things like toggling a UI feature, like the inventory or colonist panels (this sets a `bool` on the `game` object when `F2` is pressed):

```
elif event.type == keys.KEYDOWN and event.key == keys.K_F2:
	config.game.show_grid = not config.game.show_grid
```

This bool could be updated at any time from anywhere, which would work, but by keeping it in the event loop we can utilize the `KEYDOWN` built-in event and avoid having to manage the timing of the press, since if we just detect the key state and update immediately, it would flip-flop the state rapidly with undesired effects.

The main cases where continuous press would immediately update state is with direct-controlled objects such as the player or camera. Otherwise, events should be used for optimal frame management.

# Game logic implementation guide

* Limit the number of operations wherever possible. If a group of sprites needs to be checked and updated with high frequency, add a separate sprite group for those sprites. Feel free to set update delays to skip object updates for some frames (modulus is your friend!) The only required operations every frame are the draw calls.
* `kill()` your sprites when you're done with them! `kill` is a function provided by `pygame.sprite.Sprite`, which all of your sprite objects should be inheriting from (even text!).
* Don't call `update` functions outside the `game.run` function, or do so sparingly.
* Don't create events from inside `game.run` or `game.draw` as these are already really busy; use objects' `update` functions instead.
* Don't `blit` sprites off screen if you can help it; they aren't visible anyway. This isn't too critical with small amounts of sprites, but can save on cycles.
* If you need to search a list multiple times, such as checking collisions of all colonists against all of a particular object type, make a reduced list of the objects in question, rather than looping through all sprites and comparing properties to check. This way the sprite type check only needs to be performed once per frame.
* Avoid high-complexity operations such as nested loops.
* Avoid directly updating state with keyboard or mouse interaction. Create events instead and do higher complexity operations in the event loop.
* Limit sprite recreation. This is unavoidable for operations such as size changes and new object spawns, but position and color can be updated easily on existing sprites.
* Create long-duration sprites at init time and only draw them when needed.
* Use class functions to mutate object state. Common functions are `update`, `activate`, `deactivate`, `kill`, `move`, `check_collision`

# Installation

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements/requirements.txt
# run it
./main.py
```

# Building

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements/requirements-dev.txt
./build.sh
# run it
dist/colony-sim-pygame
```
