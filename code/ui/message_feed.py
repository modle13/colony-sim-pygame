import logging

import pygame

from settings import config

from ui.text_feed_group import TextFeedGroup
from util.events.event_queue import EventQueue
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class MessageFeed:
    def __init__(self):
        # these don't update position relative to camera
        self.ui_sprites = pygame.sprite.Group()

        self.message_feed = TextFeedGroup(
            position=config.control_panel_message_feed_pos,
            parent_sprite=self,
            label='messages',
            offset_mult=(0.025, 1.0),
            reversed_order=True,
            entry_limit=20,
        )
        self.ui_sprites.add(self.message_feed)

        self.subscribe_to_events()

    def update(self,force=False):
        self.ui_sprites.update()

    def draw(self):
        # self.ui_sprites.draw()
        if self.message_feed.show:
            self.message_feed.draw()

    def add_to_ui_sprites(self,sprites):
        self.ui_sprites.add(sprites)

    def clear_ui_sprites(self):
        pass

    def set_rect(self,rect_ref):
        self.rect = rect_ref

    def add_message(self,msg):
        # print(f"adding message {msg}")
        self.message_feed.add(msg)
        # print(f"msgs are now {self.message_feed.contents}")

    def subscribe_to_events(self):
        EventQueue.instance().subscribe("ADD_MESSAGE",self.add_message)
        EventQueue.instance().subscribe("CLEAR_GAMEPLAY_UI_SPRITES",self.clear_ui_sprites)
