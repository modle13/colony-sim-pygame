import logging

import pygame

from settings import config

from util.events.event_queue import EventQueue
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class UISpritesManager:
    def __init__(self):
        # these don't update position relative to camera
        self.ui_sprites = pygame.sprite.Group()
        self.context_pane_sprites = pygame.sprite.Group()
        # to be set by owning screen
        self.display_surface = None

        self.subscribe_to_events()

    def update(self):
        self.ui_sprites.update()

    def draw(self):
        # self.ui_sprites.draw()

        # draw ui sprites
                # self.display_surface.blit(text_surf,text_rect)

        # self.ui_sprites.draw(self.display_surface)
        for ea in self.ui_sprites:
            # print(f"drawing {ea} to {self.display_surface}")
            ea.draw(self.display_surface)

        # self.context_pane_sprites.draw(self.display_surface)
        for ea in self.context_pane_sprites:
            ea.draw(self.display_surface)

    def add_to_ui_sprites(self,sprites):
        self.ui_sprites.add(sprites)

    def clear_ui_sprites(self):
        pass

    def hide_sprite_context_panes(self):
        had_active = False
        for ea in self.context_pane_sprites:
            if ea.show:
                logger.info(f'closing {ea.source_sprite.label} menu')
                ea.hide()
                had_active = True
        self.context_pane_sprites.empty()
        return had_active

    def show_context_pane(self,target):
        print(f"showing context pane {target}")
        self.hide_sprite_context_panes()
        self.context_pane_sprites.add(target.context_pane)
        target.show_context()

    def subscribe_to_events(self):
        EventQueue.instance().subscribe("ADD_GAMEPLAY_UI_SPRITE",self.add_to_ui_sprites)
        EventQueue.instance().subscribe("CLEAR_GAMEPLAY_UI_SPRITES",self.clear_ui_sprites)
        EventQueue.instance().subscribe("SHOW_CONTEXT_PANE",self.show_context_pane)
