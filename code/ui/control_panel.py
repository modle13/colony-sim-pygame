import logging

import pygame

from settings import config
from ui.button import Button
from ui.dynamic_text_group import DynamicTextGroup
from ui.message_feed import MessageFeed
from ui.panel_card import PanelCard
from util import colors
from util.events.event_queue import EventQueue
from world.colony.colony_manager import ColonyManager
from world.world import World


logger = logging.getLogger(__name__)


class ControlPanel(pygame.sprite.Sprite):
    def __init__(self, game_obj):
        self.DEFAULT_HEIGHT = config.QUARTER_HEIGHT
        self.MIN_HEIGHT = config.unit / 2
        self.size = (config.SCREEN_WIDTH, self.DEFAULT_HEIGHT)
        self.position = (0, config.control_panel_vertical_pos)
        self.DEFAULT_POSITION = self.position
        self.MIN_POSITION = (0, config.SCREEN_HEIGHT - config.unit / 8)
        self.DEFAULT_TOGGLE_TEXT = 'v'
        self.toggle_text = self.DEFAULT_TOGGLE_TEXT

        self.rect = pygame.Rect(0,0,0,0)
        # for use in toggling state of common elements via shared button
        self.context_menus = pygame.sprite.Group()
        # for update management control
        self.ui_containers = pygame.sprite.Group()

        MessageFeed.instance().set_rect(self.rect)

        self.last_colonist_counts = {}
        self.colonist_count_recently_updated = True
        self.colonist_count_update_time = pygame.time.get_ticks()
        self.colonist_count_update_delay = 400

        self.inventory_count_recently_updated = False
        self.inventory_count_update_time = pygame.time.get_ticks()
        self.inventory_count_update_delay = 400

        self.draw_panel()
        self.set_up_content()

        self.subscribe_to_events()

        # TODO: send event to main to get inventory (later Inventory class, right now main)
        # self.control_panel.inventory_display.contents = self.inventory.contents

    def update(self):
        self.cooldowns()
        self.update_contents()
        MessageFeed.instance().update()
        self.ui_containers.update()

    def set_up_content(self):
        self.structure_selector = DynamicTextGroup(
            position=config.control_panel_content_pos,
            parent_sprite=self,
            label='structures',
            offset_mult=(0.1, 1.0),
            show=False,
            display_type='card',
            on_card_click=self.click_structure_card,
        )
        self.ui_containers.add(self.structure_selector)
        self.context_menus.add(self.structure_selector)
        self.structure_selector.set_all_text(config.buildable_structures)

        self.inventory_display = DynamicTextGroup(
            position=config.control_panel_content_pos,
            parent_sprite=self,
            offset_mult=(0.1, 1.0),
            label='inventory',
            content_updater=self.update_stores_display_contents,
            display_type='card',
            on_card_click=self.click_inventory_card,
        )
        self.ui_containers.add(self.inventory_display)

        self.colonist_display = DynamicTextGroup(
            position=config.control_panel_content_pos,
            parent_sprite=self,
            offset_mult=(0.1, 1.0),
            label='colonist',
            content_updater=self.update_colonist_display_contents,
            show=False,
            display_type='card',
            on_card_click=self.click_colonist_card,
        )
        self.ui_containers.add(self.colonist_display)

    def draw_panel(self):
        self.surf, self.rect = self.draw_surf(self.size, self.position)
        MessageFeed.instance().set_rect(self.rect)
        self.buttons = self.define_buttons()
        # positions relative to control panel surface origin
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, 0), (self.rect.width, 0), width=5)
        pygame.draw.line(
            self.surf, colors.OFF_WHITE,
            (config.control_panel_content_pos),
            (config.control_panel_content_pos[0], self.rect.height),
            width=5
        )
        pygame.draw.line(
            self.surf, colors.OFF_WHITE,
            config.control_panel_message_feed_pos,
            (config.control_panel_message_feed_pos[0], self.rect.height),
            width=5
        )
        # pygame.draw.line(
        #     self.surf, colors.OFF_WHITE,
        #     config.control_panel_info_popup_pos,
        #     (config.control_panel_info_popup_pos[0], self.rect.height),
        #     width=5
        # )

    def draw_surf(self, size, position):
        surf = pygame.Surface(size, pygame.SRCALPHA)
        rect = surf.get_rect(topleft=(position))
        surf.fill(colors.GRAY)
        surf.set_alpha(80)
        return surf, rect

    def define_buttons(self):
        buttons = {
            'toggle_control_panel': Button(
                self.toggle_text,
                x=config.SCREEN_WIDTH - config.unit,
                y=self.position[1] - config.unit * 0.25,
                button_action=self.toggle_console,
            ),
            'show_structures': Button(
                'Structures',
                x=self.position[0] + config.unit / 4,
                y=self.position[1] + config.unit / 4 + config.unit / 2 * 0,
                button_action=self.show_structures,
            ),
            # need a better name division here
            'show_stores': Button(
                'Stores',
                x=self.position[0] + config.unit / 4,
                y=self.position[1] + config.unit / 4 + config.unit / 2 * 1,
                button_action=self.show_stores,
            ),
            'show_colonists': Button(
                'Colonists',
                x=self.position[0] + config.unit / 4,
                y=self.position[1] + config.unit / 4 + config.unit / 2 * 2,
                button_action=self.show_colonists,
            ),
        }
        return buttons

    def draw(self, screen):
        screen.blit(self.surf, self.rect)
        for key, value in self.buttons.items():
            value.update(screen)
        # Draw the UI containers
        for entity in self.ui_containers:
            if entity.show:
                entity.draw()
        MessageFeed.instance().draw()

    def toggle_console(self):
        if self.size[1] == self.DEFAULT_HEIGHT:
            self.size = (self.size[0], self.MIN_HEIGHT)
            self.position = self.MIN_POSITION
        else:
            self.size = (self.size[0], self.DEFAULT_HEIGHT)
            self.position = self.DEFAULT_POSITION
        self.toggle_text = '^' if self.toggle_text == self.DEFAULT_TOGGLE_TEXT else self.DEFAULT_TOGGLE_TEXT
        self.draw_panel()
        self.regenerate_all_cards()
        self.update_contents()

    def show_console(self):
        if self.size[1] == self.DEFAULT_HEIGHT:
            return
        self.size = (self.size[0], self.DEFAULT_HEIGHT)
        self.position = self.DEFAULT_POSITION
        self.toggle_text = 'v'
        self.draw_panel()
        self.regenerate_all_cards()
        self.update_contents()

    def regenerate_all_cards(self):
        self.colonist_display.regenerate_cards()
        self.inventory_display.regenerate_cards()
        self.structure_selector.regenerate_cards()

    def update_contents(self):
        self.colonist_display.update(force=True)
        self.inventory_display.update(force=True)
        self.structure_selector.update(force=True)
        MessageFeed.instance().update(force=True)
        # self.message_feed.update(force=True)

    def hide_all_control_panel_views(self):
        self.colonist_display.show = False
        self.inventory_display.show = False
        self.structure_selector.show = False

    def show_structures(self):
        self.hide_all_control_panel_views()
        self.structure_selector.show = True
        self.show_console()
        logger.info('structure shown')

    def show_stores(self):
        self.hide_all_control_panel_views()
        self.inventory_display.show = True
        self.show_console()
        logger.info('stores shown')

    def show_colonists(self):
        self.hide_all_control_panel_views()
        self.colonist_display.show = True
        self.show_console()
        logger.info('colonists shown')

    def update_stores_display_contents(self):
        if self.inventory_count_recently_updated:
            return
        for key,value in ColonyManager.instance().inventory.items():
            self.inventory_display.set(key,value)

    def update_colonist_display_contents(self):
        if self.colonist_count_recently_updated:
            return

        logger.debug('UPDATING COLONIST DISPLAY CONTENTS')

        # even though this function will override DynamicTextTroup.content_updater,
            # self will still refer to the game object
        # TODO: have the control panel listen for events from colonist_manager instead
        results = list(map(lambda x: x.role.name, World.instance().get_active_colony_colonists()))
        # results = list(map(lambda x: x.role, World.instance().get_active_colony_colonists()))
        counts = {x: results.count(x) for x in results}
        if counts != self.last_colonist_counts:
            logger.debug('ROLES COUNT CHANGED; updating display')
            all_roles = set().union(self.last_colonist_counts.keys(), counts.keys())
            for key in all_roles:
                value = counts.get(key, 0)
                logger.debug(f'setting {key}: {value}')
                self.colonist_display.set(key, value)
            self.last_colonist_counts = counts

        self.colonist_count_recently_updated = True
        self.colonist_count_update_time = pygame.time.get_ticks()

    def click_colonist_card(self,name):
        logger.info(f"clicked colonist card {name}")
        EventQueue.instance().publish('CLICKED_COLONIST_CARD',[name])

    def click_inventory_card(self,name):
        logger.info(f"clicked inventory card {name}")
        EventQueue.instance().publish('CLICKED_INVENTORY_CARD',[name])

    def click_structure_card(self,name):
        logger.info(f"clicked structure card {name}")
        EventQueue.instance().publish('HIDE_CONTEXT_PANES',[False])
        EventQueue.instance().publish('CLICKED_STRUCTURE_CARD',[name])

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if self.colonist_count_recently_updated:
            if current_time - self.colonist_count_update_time >= self.colonist_count_update_delay:
                self.colonist_count_recently_updated = False

    def subscribe_to_events(self):
        # logger.info("SUBSCRIBING TO CONTROL PANEL EVENTS")
        EventQueue.instance().subscribe('SHOW_STRUCTURE_SELECTION',self.show_structures)
        EventQueue.instance().subscribe('SHOW_STORES_SELECTION',self.show_stores)
        EventQueue.instance().subscribe('SHOW_COLONIST_SELECTION',self.show_colonists)
