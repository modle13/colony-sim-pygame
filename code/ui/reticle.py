import logging
import math

import pygame

from util import camera
from util.events.hotkey_events import HotkeyEvents
from util.input_manager import InputManager
from settings import config


logger = logging.getLogger(__name__)


can_move_up    = lambda value: value > config.camera.state.height
can_move_down  = lambda value: value < config.max_height - config.camera.state.height
can_move_left  = lambda value: value > config.camera.state.width
can_move_right = lambda value: value < config.max_width - config.camera.state.width

directions = {
    'UP':    {'axis': 'y', 'value': -1, 'check': can_move_up   },
    'DOWN':  {'axis': 'y', 'value':  1, 'check': can_move_down },
    'LEFT':  {'axis': 'x', 'value': -1, 'check': can_move_left },
    'RIGHT': {'axis': 'x', 'value':  1, 'check': can_move_right},
}


class Reticle(pygame.sprite.Sprite):
    def __init__(self):
        super(Reticle, self).__init__()

        self.radius = config.grid_square_size // 25
        # center of screen
        self.start = (config.HALF_WIDTH, config.HALF_HEIGHT)

        self.surf = pygame.Surface((self.radius*2, self.radius*2), pygame.SRCALPHA)
        # self.surf.fill(config.colors.bg_color_selected)
        self.rect = self.surf.get_rect(center=self.start)
        self.position_rect = self.rect.copy()
        pygame.draw.circle(self.surf,config.colors.reticle,(self.radius,self.radius),self.radius)

        self.mouse_offset = None
        self.original_pos_on_mouse_down = None

    # Move the position based on user keypresses
    def update(self):
        # pressed_keys = config.game.pressed_keys
        # this is the viewport position
        # don't want to move if edges of screen are fully visible
        # so use math based on camera state (which is just a rect) and max width/height
        # also, not using elif here so that each direction processes each update
        # otherwise higher priority checks would override lower, and diagonal movement would not work

        if HotkeyEvents.instance().right_mouse_drag:
            self.do_mouse_move()
            # if using mouse, stop processing movement
            return
        if InputManager.instance().get_up():
        # if pressed_keys[keys.K_UP]    or pressed_keys[keys.K_w]:
            self.move('UP', self.position_rect.y)
        if InputManager.instance().get_down():
        # if pressed_keys[keys.K_DOWN]  or pressed_keys[keys.K_s]:
            self.move('DOWN', self.position_rect.y)
        if InputManager.instance().get_left():
        # if pressed_keys[keys.K_LEFT]  or pressed_keys[keys.K_a]:
            self.move('LEFT', self.position_rect.x)
        if InputManager.instance().get_right():
        # if pressed_keys[keys.K_RIGHT] or pressed_keys[keys.K_d]:
            self.move('RIGHT', self.position_rect.x)

    def set_mouse_offset(self, pos: tuple):
        self.original_mouse_pos = pos
        curr_x, curr_y, _, _ = self.rect
        self.original_pos_on_mouse_down = (curr_x, curr_y)
        self.mouse_drag = True

    def clear_mouse_offset(self):
        self.mouse_drag = False

    def do_mouse_move(self):
        """
        move reticle toward mouse
        """
        unit = config.grid_square_size
        # get current mouse pos
        mouse_x, mouse_y = pygame.mouse.get_pos()
        # get original mouse pos (at time of MOUSEBUTTONDOWN)
        start_mouse_x, start_mouse_y = self.original_mouse_pos
        # calculate the difference; this is the offset to the original reticle rect pos
        diff_x = mouse_x - start_mouse_x
        diff_y = mouse_y - start_mouse_y
        # get original reticle position (at time of MOUSEBUTTONDOWN)
        orig_x, orig_y = self.original_pos_on_mouse_down
        # apply mouse diff offset to original position to get new pos
        new_x = math.floor((orig_x + diff_x) / unit) * unit
        new_y = math.floor((orig_y + diff_y) / unit) * unit
        # get boundaries and normalize them to grid units
        _, _, cam_width, cam_height = config.camera.state
        top = cam_height
        top = math.floor(top / unit) * unit
        #bottom = config.max_height - cam_height + unit
        bottom = config.max_height - cam_height + unit
        bottom = math.floor(bottom / unit) * unit
        left = cam_width
        left = math.floor(left / unit) * unit
        #right = config.max_width - cam_width + unit
        right = config.max_width - cam_width + unit
        right = math.floor(right / unit) * unit
        # check boundaries
        new_y = new_y if new_y > top else top
        new_y = new_y if new_y < bottom else bottom
        new_x = new_x if new_x > left else left
        new_x = new_x if new_x < right else right

        self.rect.x = new_x
        self.rect.y = new_y

    def move(self, direction, check_param):
        direction_details = directions.get(direction)
        if not direction_details.get('check')(check_param):
            # logger.info(f"can't move in direction {direction}; {check_param}")
            return
        axis = direction_details.get('axis')
        dir_value = direction_details.get('value')
        if axis == 'y':
            self.position_rect.y += dir_value * config.grid_square_size//2
        elif axis == 'x':
            self.position_rect.x += dir_value * config.grid_square_size//2
