import logging
import sys

import pygame

from settings import *

from ui.screens.screen_state import SCREEN_STATE

from ui.screens.base_screen import BaseScreen
from ui.screens.settings_screen import SettingsScreen

from util.debug import debug
from util.events.event_queue import EventQueue
from util.events.flow_events_enum import FLOW_EVENTS
from util.input_manager import InputManager
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class InitScreen(BaseScreen):
    def __init__(self):
        self.screen_id = 'init_screen'
        super().__init__('Pygame ColonySim')

        # main menu
        self.options_entries = [
            {'name':'new game', 'flow_event': FLOW_EVENTS.START_GAMEPLAY,},
            {'name':'load game', 'flow_event': FLOW_EVENTS.SHOW_LOAD_SAVE,},
            {'name':'settings', 'flow_event': FLOW_EVENTS.SHOW_SETTINGS,},
            {'name':'exit', 'flow_event': FLOW_EVENTS.SHOW_EXIT},
        ]

    def run(self):
        if self.state == SCREEN_STATE.ENABLED_VISIBLE:
            super().run()
        else:
            self.cooldowns()

    def input(self):
        if not self.can_move or not self.input_active:
            # logger.info(f'{self.title} can move? {self.can_move}; input active? {self.input_active}')
            return

        # if InputManager.instance().get_menu_interact(key_down=(self.selection_index in self.key_down_indexes)):
        #     EventQueue.instance().publish("FLOW_EVENT",[self.current_element.flow_event])
        #     self.can_move = False
        #     self.selection_time = pygame.time.get_ticks()
        #     # EventQueue.instance().publish('SET_GAME_STATE',[self.current_element.game_state])
        #     # EventQueue.instance().publish('OPEN_SCREEN',[self.current_element.screen_id])
        # else:
        super().input()

    def draw(self):
        self.draw_backing()
        super().draw()
