import logging

from settings import config
from util.events.event_queue import EventQueue
from util.singleton import Singleton

from ui.screens.screen_state import SCREEN_STATE

from ui.screens.exit_screen import ExitScreen
from ui.screens.gameplay_screen import GameplayScreen
from ui.screens.init_screen import InitScreen
from ui.screens.inventory_screen import InventoryScreen
from ui.screens.loading_screen import LoadingScreen
from ui.screens.pause_screen import PauseScreen
from ui.screens.quit_screen import QuitScreen
from ui.screens.settings_screen import SettingsScreen
from ui.screens.upgrade_screen import UpgradeScreen


logger = logging.getLogger(__name__)


@Singleton
class ScreenManager:
    def __init__(self):
        logger.info("ScreenManager initialized")
        self.active_screens = []
        self.current_screen = None
        self.subscribe_to_events()

    def get_screen(self,target_screen):
        match target_screen:
            case 'loading_screen':
                return LoadingScreen.instance()
            case 'init_screen':
                return InitScreen.instance()
            case 'gameplay_screen':
                return GameplayScreen.instance()
            case 'load_screen':
                return InitScreen.instance()
            case 'settings_screen':
                return SettingsScreen.instance()
            case 'exit_screen':
                return ExitScreen.instance()

            case 'settings_screen_gameplay':
                return SettingsScreen.instance()
            case 'settings_screen_controls':
                return SettingsScreen.instance()
            case 'settings_screen_video':
                return SettingsScreen.instance()
            case 'settings_screen_audio':
                return SettingsScreen.instance()

            case 'settings_screen_in_game':
                return PauseScreen.instance()
            case 'quit_screen':
                return QuitScreen.instance()

            case 'pause_screen':
                return PauseScreen.instance()

            # TODO: not wired up yet
            case 'inventory_screen':
                return InventoryScreen.instance()
            case 'upgrade_screen':
                return UpgradeScreen.instance()
            case 'quit_screen':
                return QuitScreen.instance()
            case _:
                return None

    def update(self):
        # print(f"updating screen_manager; current screen is {self.current_screen}")
        if self.current_screen and self.current_screen.state == SCREEN_STATE.ENABLED_VISIBLE:
            self.current_screen.run()

    def clean_up_screens(self):
        for index,each in enumerate(self.active_screens):
            if not each:
                del(self.active_screens,index)
                return

    def assign_current_screen(self,assign_newest=False):
        if self.current_screen and not assign_newest:
            return

        logger.info(f"assigning screen {self.current_screen}; active screens: {self.active_screens}")

        if self.current_screen:
            self.current_screen.enable()

    def go_back_one_screen(self):
        logger.info('going back one screen')
        # disable current screen and remove it from active_screens

        if self.current_screen.screen_id in self.base_screens:
            logger.info(f'on {self.current_screen.screen_id}, not going back')
            return

        if self.active_screens and \
            self.active_screens[-1] == self.current_screen \
            and self.current_screen.screen_id not in self.base_screens:
            # remove it from active list
            self.active_screens.pop()

        logger.info(f'active screens are {self.active_screens}')

        # disable it
        self.disable_current_screen()

        # set current screen
        self.assign_current_screen(assign_newest=True)

    def disable_current_screen(self):
        if self.current_screen.screen_id == 'pause_screen':
            # if we're closing the pause screen here, then it's an unpause
            EventQueue.instance().publish('UNPAUSE')

        self.current_screen.disable()
        self.current_screen = None

    def open_screen(self,target_screen,close_others=False):
        # using string "True" because too lazy to parse split string params from menu entry interactions
        if close_others or close_others == "True":
            logger.info("close others flag sent, closing all screens")
            self.close_all_screens()
        else:
            self.hide_all_screens()

        logger.info('target screen to open is ' + target_screen)
        # log_stack()
        found_screen = self.get_screen(target_screen)
        logger.info(f'found screen {found_screen}')
        if not found_screen:
            return
        if found_screen not in self.active_screens:
            self.active_screens.append(found_screen)
        self.current_screen = found_screen

        self.assign_current_screen(assign_newest=True)

        logger.info(f'active screens are {self.active_screens}')

    def close_all_screens(self):
        for each in self.active_screens:
            each.disable()
        self.active_screens = []
        self.current_screen = None

    def hide_all_screens(self):
        for each in self.active_screens:
            each.set_state(SCREEN_STATE.ENABLED_HIDDEN)

    def start_gameplay_mode(self):
        self.close_all_screens()
        self.open_screen('gameplay_screen')

    def set_screen(self,screen):
        pass

    def hide_sprite_context_panes(self,had_active):
        for sprite in config.game.game_objects:
            if sprite.context_pane.show:
                logger.info(f'closing {sprite.label} menu')
                sprite.context_pane.hide()
                had_active = True
        return had_active

    def handle_escape(self):
        had_active_ui_element = self.hide_sprite_context_panes(False)
        return had_active_ui_element

    def subscribe_to_events(self):
        EventQueue.instance().subscribe("HIDE_CONTEXT_PANES",self.hide_sprite_context_panes)
        EventQueue.instance().subscribe('OPEN_SCREEN',self.open_screen)
        # EventQueue.instance().subscribe('UNPAUSE',self.start_gameplay_mode)
