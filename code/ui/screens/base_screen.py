import logging

import pygame

from settings import config
from ui.menu_element import MenuElement

from ui.screens.screen_state import SCREEN_STATE

# from util.debug import log_stack
from util.debug import debug
from util.events.event_queue import EventQueue
# from util.events.flow_events_enum import FLOW_EVENTS # may not need this here
from util.input_manager import InputManager


logger = logging.getLogger(__name__)

config.MENU_FONT      = pygame.font.Font(config.font, int(110 / 1080 * config.SCREEN_HEIGHT))
config.LARGE_FONT     = pygame.font.Font(config.font, int(40  / 1080 * config.SCREEN_HEIGHT))
config.MEDIUM_FONT    = pygame.font.Font(config.font, int(35  / 1080 * config.SCREEN_HEIGHT))
config.SMALL_FONT     = pygame.font.Font(config.font, int(25  / 1080 * config.SCREEN_HEIGHT))
config.SMALLER_FONT   = pygame.font.Font(config.font, int(20  / 1080 * config.SCREEN_HEIGHT))
config.TINY_FONT      = pygame.font.Font(config.font, int(15  / 1080 * config.SCREEN_HEIGHT))
config.TINIER_FONT    = pygame.font.Font(config.font, int(10  / 1080 * config.SCREEN_HEIGHT))


class BaseScreen:
    def __init__(self,title,active_states=[SCREEN_STATE.ENABLED_VISIBLE]):
        self.state = SCREEN_STATE.DISABLED
        self.title = title

        # general display setup
        self.display_surface = pygame.display.get_surface()
        self.font        = config.MENU_FONT
        self.font_small  = config.SMALL_FONT
        self.font_medium = config.MEDIUM_FONT
        self.font_large  = config.LARGE_FONT

        # general display setup
        self.title_width = self.font_large.size(self.title)[0]

        # selection system
        self.current_element_list = []
        self.current_element = None
        self.selection_index = 0
        self.last_selection_index = 0
        self.allowed_input_states = active_states
        self.draw_title_states = [SCREEN_STATE.ENABLED_VISIBLE]
        # key_held_down_indexes are used for holdfill triggers
        self.key_held_down_indexes = []
        self.input_active = True
        self.reset_since_button_hold = False

        # timers
        self.init_time = pygame.time.get_ticks()
        self.init_cooldown = 1000
        self.init_complete = False
        self.selection_time = 0
        self.selection_cooldown = 150
        self.can_move = True
        self.load_time = pygame.time.get_ticks()
        self.load_cooldown = 150
        self.load_complete = False
        self.current_hold_time = 2000
        self.hold_started = False
        self.hold_length = 900

        # element definitions
        self.element_width = config.SCREEN_UNIT*10
        self.element_height = config.SCREEN_UNIT*1.5

        # backing dimensions
        self.backing_width = config.SCREEN_WIDTH
        self.backing_height = config.SCREEN_UNIT*16.5
        self.backing_left = 0
        self.backing_top = config.SCREEN_UNIT*3.5

        self.main_menu_element_list = []
        # self.create_menu_options()
        self.current_element_list = self.main_menu_element_list

        self.options_entries = []
        self.disable_backspace_entries = []

    def run(self):
        #logger.info('called run in screen parent for',self.title)
        self.cooldowns()
        self.update()
        if self.state == SCREEN_STATE.ENABLED_VISIBLE:
            self.input()
            self.draw()

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.can_move:
            if current_time - self.selection_time >= self.selection_cooldown:
                self.can_move = True
        if not self.load_complete:
            if current_time - self.load_time >= self.load_cooldown:
                self.load_complete = True

    def update(self):
        # logger.info(f'called update in screen parent for {self.title}')
        if self.reset_since_button_hold:
            # logger.info(f'{self.title} self.reset_since_button_hold is True')
            # FIXME doesn't seem to quite be working
            #   want to disable input after screen load until button has been released
            if not InputManager.instance().get_menu_interact(key_down=False):
                # logger.info(f'{self.title} input get_menu_interact after load is False; re-enabling input')
                self.reset_since_button_hold = False

    def input(self):
        if self.state != SCREEN_STATE.ENABLED_VISIBLE or not self.load_complete or self.reset_since_button_hold:
            # logger.info(f"{self.title} state {self.state}; load complete {self.load_complete}; reset since button hold {self.reset_since_button_hold}")
            return

        if self.state not in self.allowed_input_states:
            # logger.info(f"{self.title} invalid state {self.state}")
            return
        if not self.can_move or not self.input_active:
            # logger.info(f'{self.title} can move? {self.can_move}; input active? {self.input_active}')
            return

        inputs = InputManager.instance()
        element_count = len(self.current_element_list)

        key_down = True
        if self.selection_index in self.key_held_down_indexes: # anything with a holdfill defined
            # key_down means only pressed in current frame
            # and want continual checking if it's a hold timeout select
            key_down = False

        # if inputs.get_up_menu() and self.state in self.allowed_input_states and element_count > 0:
        if inputs.get_mouse_move() != pygame.math.Vector2(0,0):
            # print("Mouse moving")
            self.update_current_selection()
            if inputs.get_lmb_down():
                print("LMB down pressed in base screen while mouse moving")
                self.check_menu_clicks()
        elif inputs.get_lmb_down():
            print("LMB down pressed in base screen")
            self.check_menu_clicks()
        elif inputs.get_up_menu_no_alpha_move():
            if not self.current_element_list:
                return
            self.last_selection_index = self.selection_index
            self.selection_index -= 1
            if self.selection_index < 0:
                self.selection_index = len(self.current_element_list) - 1
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
            self.current_element = self.current_element_list[self.selection_index]
        elif inputs.get_down_menu_no_alpha_move():
            if not self.current_element_list:
                return
            self.last_selection_index = self.selection_index
            self.selection_index += 1
            if self.selection_index > len(self.current_element_list) - 1:
                self.selection_index = 0
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
            self.current_element = self.current_element_list[self.selection_index]
        elif inputs.get_up_menu() and element_count > 0:
            if not self.current_element_list:
                return
            self.last_selection_index = self.selection_index
            self.selection_index -= 1
            if self.selection_index < 0:
                self.selection_index = element_count - 1
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
            #logger.info("up pressed in screen")
        # elif inputs.get_down_menu() and self.state in self.allowed_input_states and element_count > 0:
        elif inputs.get_down_menu() and element_count > 0:
            if not self.current_element_list:
                return
            self.last_selection_index = self.selection_index
            self.selection_index += 1
            if self.selection_index > element_count - 1:
                self.selection_index = 0
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
            #logger.info("down pressed in screen")
        elif inputs.get_left_menu():
            #logger.info("left pressed in screen")
            pass
        elif inputs.get_right_menu():
            #logger.info("right pressed in screen")
            pass

        if inputs.get_menu_interact(key_down=(self.selection_index in self.key_held_down_indexes)):
            # print(f"asdf; key down is {key_down}; {self.key_held_down_indexes}; {self.selection_index} {self.current_hold_time} current element {self.current_element}")
            # logger.info("inputs.get_menu_interact trigger in screen.input")
            # might be able to remove the self.load_complete since `def input` now has a return check for it
            # hold timer checks
            if not key_down:
                #logger.info('key down not set, checking hold timers')
                if not self.hold_started:
                    self.current_hold_time = pygame.time.get_ticks()
                    self.hold_started = True
                    #logger.info('hold not started, starting hold, returning')
                    return
                if not self.hold_time_elapsed():
                    #progress = (pygame.time.get_ticks() - self.current_hold_time) / self.hold_length * 100
                    #logger.info('hold time not elapsed, returning',progress)
                    return

            EventQueue.instance().publish("FLOW_EVENT",[self.current_element.flow_event])
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
        elif inputs.get_backspace() and self.current_element.label not in self.disable_backspace_entries:
            self.current_element.handle_backspace()
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
            self.current_element = self.current_element_list[self.selection_index]
        else:
            self.hold_started = False
            self.current_hold_time = pygame.time.get_ticks()

    def draw(self):
        # draw main screen
        debug(f'{self.screen_id} : {pygame.time.get_ticks() - self.load_time}', y=30)

        if self.state in self.draw_title_states:
            if self.title:
                self.draw_title()
                self.draw_sub_title()

        if self.screen_id == "loading_screen":
            debug("."*((pygame.time.get_ticks()//100)%8),y=50,align='center')
        debug('time elapsed: ' + str((pygame.time.get_ticks() - self.init_time)//1000),y=50,x=10,align="topright")
        self.draw_options()

    def set_state(self,new_state):
        if new_state == self.state:
            return
        logger.info(f"{self.screen_id} setting state from {str(self.state)} to {str(new_state)}")
        # self.state = max(0, new_state)
        self.state = new_state

        self.can_move = False
        self.selection_time = pygame.time.get_ticks()
        self.hold_started = False

        match self.state:
            case SCREEN_STATE.DISABLED:
                pass
            case SCREEN_STATE.ENABLED_VISIBLE:
                self.reset_load_timer()
                self.create_menu_options()
                logger.info(f'opened {self.screen_id} screen')
                # to prevent immediate triggers

    def draw_sub_title(self):
        # TODO: move this to base screen?
        # draw studio title
        text_width,text_height = self.font_small.size('modle studios')
        text_surf = self.font_small.render('modle studios',False,config.colors.title)
        text_rect = pygame.Rect(config.SCREEN_WIDTH//2-text_width//2,config.SCREEN_HEIGHT//2-config.SCREEN_UNIT*5,text_width,text_height)
        pygame.draw.rect(self.display_surface,config.colors.ui_bg_color,text_rect.inflate(20,20))
        self.display_surface.blit(text_surf,text_rect)
        pygame.draw.rect(self.display_surface,config.colors.ui_border_color,text_rect.inflate(20,20),3)

    def draw_options(self):
        for ea in self.current_element_list:
            fill_percent = 0
            current = self.selection_index == ea.index
            if current and self.hold_started:
                fill_percent = min(100,(pygame.time.get_ticks() - self.current_hold_time) / self.hold_length)
            ea.display(self.display_surface,self.selection_index,fill_percent=fill_percent,current=current)

    def draw_backing(self):
        pygame.draw.rect(
            self.display_surface,
            config.colors.ui_bg_color,
            pygame.Rect(0,0,config.SCREEN_WIDTH,config.SCREEN_HEIGHT)
        )

    def create_menu_options(self):
        self.main_menu_element_list = self.create_options(self.options_entries)
        if self.current_element_list:
            self.current_element = self.current_element_list[self.selection_index]
            # print(f"CURRENT ELEMENT AT CREATE is {self.current_element}; {self.current_element_list}")

    def hold_time_elapsed(self):
        return self.hold_started and pygame.time.get_ticks() - self.current_hold_time > self.hold_length

    def draw_title(self):
        # draw screen title
        if self.state not in self.draw_title_states:
            return
        text_width,text_height = self.font_large.size(self.title)
        text_surf = self.font_large.render(self.title,False,config.colors.title)
        text_rect = pygame.Rect(config.SCREEN_WIDTH//2-text_width//2,config.SCREEN_UNIT*2,text_width,text_height)
        pygame.draw.rect(self.display_surface,config.colors.ui_bg_color,text_rect.inflate(20,20))
        self.display_surface.blit(text_surf,text_rect)
        pygame.draw.rect(self.display_surface,config.colors.ui_border_color,text_rect.inflate(20,20),3)

    def create_options(self,entries):
        element_list = []
        for index,ea in enumerate(entries):
            # horiz
            left = config.SCREEN_WIDTH//2 - self.element_width // 2
            # vert
            top = config.SCREEN_HEIGHT//2 + (index * config.SCREEN_UNIT*1.5)
            menu_element = MenuElement(
                ea.get('name'),
                left,top,
                self.element_width,self.element_height,
                index,self.font_small,
                state=ea.get('state'),
                holdfill=ea.get('holdfill'),
                held_font=self.font_medium,
            )

            self.set_trigger_properties(menu_element, ea)

            element_list.append(menu_element)
            if ea.get('holdfill'):
                # print(f"{ea.get('name')} that's a holdfill")
                self.key_held_down_indexes.append(index)

        self.current_element_list = element_list
        return element_list

    def set_trigger_properties(self, menu_element, data):
        menu_element.screen_id = data.get('screen_id','')
        menu_element.flow_event = data.get('flow_event','')

        menu_element.trigger_event = data.get('trigger_event','')
        param_string = data.get('trigger_event_params','')
        menu_element.trigger_event_params = param_string.split(',') if param_string else []
        logger.info(f'{self.screen_id} element event params are {menu_element.trigger_event_params}')

    def reset_load_timer(self):
        # logger.info('resetting load timer')
        self.reset_since_button_hold = True
        self.load_complete = False
        self.load_time = pygame.time.get_ticks()

    def enable(self):
        logger.info(f'enabling screen {self.title}')
        self.reset_load_timer()
        self.set_state(SCREEN_STATE.ENABLED_VISIBLE)

    def disable(self):
        # log_stack()
        logger.info(f'disabling screen {self.title}')
        self.set_state(SCREEN_STATE.DISABLED)

    def check_menu_clicks(self):
        if self.current_element.rect.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                print(f"clicked {self.current_element.name}")
                self.can_move = False
                self.selection_time = pygame.time.get_ticks()
                EventQueue.instance().publish("FLOW_EVENT",[self.current_element.flow_event])
            #     self.pressed = True
            #     self.button_color = colors.GREEN
            # else:
            #     if self.pressed:
            #         self.button_action()
            #         self.pressed = False

    def update_current_selection(self):
        if not self.current_element_list:
            return

        for index,ea in enumerate(self.current_element_list):
            if ea.rect.collidepoint(pygame.mouse.get_pos()):
                self.last_selection_index = self.current_element_list.index(self.current_element)
                self.selection_index = index
                self.current_element = ea
                # self.can_move = False
                # self.selection_time = pygame.time.get_ticks()
                break

    def __repr__(self):
        return f"{self.screen_id}: {self.state}"
