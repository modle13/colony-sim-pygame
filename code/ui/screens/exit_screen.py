import logging

from ui.screens.screen_state import SCREEN_STATE

from ui.screens.base_screen import BaseScreen

from util.debug import debug
from util.events.flow_events_enum import FLOW_EVENTS
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class ExitScreen(BaseScreen):

    def __init__(self):
        self.screen_id = 'exit_screen'
        super().__init__("Exit Game?")

        self.options_entries = [
            {'name':'yes','flow_event': FLOW_EVENTS.EXIT_GAME,'holdfill':True},
            {'name':'no','flow_event': FLOW_EVENTS.SHOW_INIT},
        ]

    def run(self):
        if self.state != SCREEN_STATE.ENABLED_VISIBLE:
            return
        super().run()
        self.input()
        self.update()
        self.draw()

    def draw(self):
        self.draw_backing()
        super().draw()
