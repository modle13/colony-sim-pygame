import logging

import pygame

from settings import *

from ui.screens.screen_state import SCREEN_STATE

from ui.screens.base_screen import BaseScreen

from util.input_manager import InputManager
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class UpgradeScreen(BaseScreen):
    def __init__(self):
        self.screen_id = 'upgrade_screen'
        super().__init__('upgrade',active_states=[SCREEN_STATE.ENABLED_VISIBLE])

        # general setup
        self.state = 0
        self.display_surface = pygame.display.get_surface()
        self.player = None
        self.attribute_number = 0
        self.attribute_names = []
        self.font = pygame.font.Font(UI_FONT,UI_FONT_SIZE)
        self.font_medium = pygame.font.Font(UI_FONT,UI_FONT_SIZE_MEDIUM)
        self.font_large = pygame.font.Font(UI_FONT,UI_FONT_SIZE_LARGE)
        self.title = 'Level Up'
        self.title_width = self.font_large.size(self.title)[0]

        # element dimensions
        self.width = 0
        self.height = 0

        # backing dimensions
        self.backing_width = self.display_surface.get_size()[0]
        self.backing_height = self.display_surface.get_size()[1] * 0.85
        self.backing_left = 0
        self.backing_top = self.display_surface.get_size()[1] * 0.175

        # selection system
        self.selection_index = 0
        self.selection_time = 0
        self.selection_cooldown = 150
        self.can_move = True
        logger.info('upgrade screen initialized')

    def run(self):
        self.input()
        #self.update()
        self.draw()
        self.cooldowns()

    def set_state(self,new_state):
        self.state = new_state
        if self.state == 10:
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()

    def set_player(self,player):
        self.player = player
        self.attribute_number = len(player.stats.default_stats)
        self.attribute_names = list(player.stats.default_stats.keys())
        self.width = self.display_surface.get_size()[0] // (len(self.attribute_names) + 1)
        self.height = self.display_surface.get_size()[1] * 0.7
        self.create_elements()

    def input(self):
        if not self.can_move:
            return
        inputs = InputManager.instance()
        if inputs.get_right_menu() and self.selection_index < self.attribute_number - 1:
            self.selection_index += 1
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
        elif inputs.get_left_menu() and self.selection_index > 0:
            self.selection_index -= 1
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
        elif inputs.get_interact():
            self.can_move = False
            self.selection_time = pygame.time.get_ticks()
            self.element_list[self.selection_index].trigger(self.player)

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.can_move:
            if current_time - self.selection_time >= self.selection_cooldown:
                self.can_move = True

    def create_elements(self):
        self.element_list = []

        for element,index in enumerate(range(self.attribute_number)):
            # horiz
            full_width = self.display_surface.get_size()[0]
            increment = full_width // self.attribute_number
            left = (element * increment) + (increment - self.width) // 2

            # vert
            top = self.display_surface.get_size()[1] * 0.3

            # create
            upgrade_element = UpgradeElement(left,top,self.width,self.height,index,self.font)
            self.element_list.append(upgrade_element)

    def draw(self):
        super().draw()
        pygame.draw.rect(self.display_surface,UI_BORDER_COLOR,pygame.Rect(self.backing_left,self.backing_top,self.backing_width,self.backing_height))
        for index,element in enumerate(self.element_list):
            # get attributes
            name = self.attribute_names[index]
            value = self.player.stats.get(name)
            max_value = self.player.stats.get_max(name)
            if value >= max_value:
                cost = 'maxed'
            else:
                cost = int(self.player.stats.upgrade_cost[name])
            element.display(self.display_surface,self.selection_index,name,value,max_value,cost,self.player.stats.exp)

        # draw xp
        target_elem_cost = self.player.stats.upgrade_cost[self.attribute_names[self.selection_index]]
        output_str = f'{int(self.player.stats.exp)} : {int(target_elem_cost)}'
        text_color = TEXT_COLOR if self.player.stats.exp - target_elem_cost >= 0 else TEXT_COLOR_BAD
        text_surf = self.font_medium.render(output_str,False,text_color)
        text_width,text_height = self.font_medium.size(output_str)
        text_rect = pygame.Rect(15,self.backing_top,text_width,text_height)
        pygame.draw.rect(self.display_surface,UI_BG_COLOR,text_rect.inflate(20,20))
        self.display_surface.blit(text_surf,text_rect)
        pygame.draw.rect(self.display_surface,UI_BORDER_COLOR,text_rect.inflate(20,20),3)


class UpgradeElement:
    def __init__(self,l,t,w,h,index,font):
        self.rect = pygame.Rect(l,t,w,h)
        self.index = index
        self.font = font

    def display_names(self,surface,name,cost,selected,exp):
        color = TEXT_COLOR_SELECTED if selected else TEXT_COLOR
        color = color if cost=='maxed' or exp-cost >= 0 else TEXT_COLOR_BAD
        color = TEXT_COLOR_MAX if cost=='maxed' else color

        # title
        title_surf = self.font.render(name,False,color)
        title_rect = title_surf.get_rect(midtop = self.rect.midtop + pygame.math.Vector2(0,20))

        # cost
        cost_surf = self.font.render(str(cost),False,color)
        cost_rect = cost_surf.get_rect(midbottom = self.rect.midbottom - pygame.math.Vector2(0,20))

        # draw
        surface.blit(title_surf,title_rect)
        surface.blit(cost_surf,cost_rect)

    def display_bar(self,surface,value,max_value,selected):
        # drawing setup
        top = self.rect.midtop + pygame.math.Vector2(0,60)
        bottom = self.rect.midbottom + pygame.math.Vector2(0,-50)
        color = BAR_COLOR_SELECTED if selected else BAR_COLOR

        # bar setup
        full_height = bottom[1] - top[1]
        relative_number = (value / max_value) * full_height
        value_rect = pygame.Rect(top[0]-15,bottom[1]-relative_number,30,relative_number)

        # draw elements
        pygame.draw.line(surface,color,top,bottom,5)
        pygame.draw.rect(surface,color,value_rect)

    def trigger(self,player):
        stat = list(player.stats.default_stats.keys())[self.index]
        player.stats.attempt_upgrade(stat)
        Save.instance().player_stats = player.stats

    def display(self,surface,selection_number,name,value,max_value,cost,exp):
        if self.index == selection_number:
            pygame.draw.rect(surface,BG_COLOR_SELECTED,self.rect)
        else:
            pygame.draw.rect(surface,UI_BG_COLOR,self.rect)

        pygame.draw.rect(surface,UI_BORDER_COLOR,self.rect,4)

        self.display_names(surface,name,cost,self.index==selection_number,exp)
        self.display_bar(surface,value,max_value,self.index==selection_number)
