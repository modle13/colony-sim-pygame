from ui.screens.base_screen import BaseScreen

from util.singleton import Singleton


@Singleton
class TemplateScreen(BaseScreen):
    def __init__(self):
        super().__init__("Rename Me")

    def run(self):
        super().run()
        self.input()
        self.update()
        self.draw()

    def input(self):
        super().input()

    def update(self):
        if self.state == 0:
            pass
        elif self.state == 10:
            pass
