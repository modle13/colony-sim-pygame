import logging

import pygame

from ui.control_panel import ControlPanel

from ui.screens.screen_state import SCREEN_STATE

from ui.screens.base_screen import BaseScreen

from ui.ui_sprites_manager import UISpritesManager

from util.debug import debug
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class GameplayScreen(BaseScreen):
    def __init__(self):
        self.screen_id = 'gameplay_screen'
        super().__init__("")
        self.control_panel = ControlPanel(self)
        UISpritesManager.instance().display_surface = self.display_surface

    def run(self):
        # logger.info("running gameplay screen")
        super().run()
        self.input()
        self.update()
        self.draw()

    def input(self):
        pass
        # super().input()

    def update(self):
        # logger.info("updating gameplay screen")
        match self.state:
            case SCREEN_STATE.DISABLED:
                pass
            case SCREEN_STATE.ENABLED_VISIBLE:
                UISpritesManager.instance().update()
                self.control_panel.update()
            case SCREEN_STATE.ENABLED_HIDDEN:
                pass

    def draw(self):
        super().draw()
        UISpritesManager.instance().draw()
        self.control_panel.draw(self.display_surface)

    def hide_menus(self, had_active):
        for sprite in config.game.context_menus:
            if sprite.show:
                logger.info(f'closing {sprite.label} menu')
                had_active = True
                sprite.toggle()
        return had_active
