from ui.screens.base_screen import BaseScreen

from ui.screens.screen_state import SCREEN_STATE
from util.events.flow_events_enum import FLOW_EVENTS

from util.singleton import Singleton


@Singleton
class PauseScreen(BaseScreen):
    def __init__(self):
        self.screen_id = 'pause_screen'
        super().__init__("Pause")

        # main menu
        self.options_entries = [
            {'name':'unpause', 'flow_event': FLOW_EVENTS.START_GAMEPLAY,},
            {'name':'save game', 'flow_event': FLOW_EVENTS.SAVE_GAME,},
            {'name':'settings', 'flow_event': FLOW_EVENTS.SHOW_IN_GAME_SETTINGS,},
            {'name':'quit', 'flow_event': FLOW_EVENTS.SHOW_QUIT,},
        ]

    def run(self):
        if self.state == SCREEN_STATE.ENABLED_VISIBLE:
            super().run()
        else:
            self.cooldowns()

    def draw(self):
        self.draw_backing()
        super().draw()
