from ui.screens.base_screen import BaseScreen

from ui.screens.screen_state import SCREEN_STATE
from util.events.flow_events_enum import FLOW_EVENTS

from util.singleton import Singleton

@Singleton
class SettingsScreen(BaseScreen):
    def __init__(self):
        self.screen_id = 'settings_screen'
        super().__init__("Settings")

        # main menu
        self.options_entries = [
            {'name':'gameplay', 'flow_event': FLOW_EVENTS.SHOW_GAMEPLAY_SETTINGS,},
            {'name':'controls', 'flow_event': FLOW_EVENTS.SHOW_CONTROLS_SETTINGS,},
            {'name':'video', 'flow_event': FLOW_EVENTS.SHOW_VIDEO_SETTINGS,},
            {'name':'audio', 'flow_event': FLOW_EVENTS.SHOW_AUDIO_SETTINGS,},
            {'name':'back', 'flow_event': FLOW_EVENTS.SHOW_INIT,},
        ]

    def run(self):
        if self.state == SCREEN_STATE.ENABLED_VISIBLE:
            super().run()
        else:
            self.cooldowns()

    def draw(self):
        self.draw_backing()
        super().draw()
