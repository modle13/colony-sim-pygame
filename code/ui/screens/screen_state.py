from enum import Enum


class SCREEN_STATE(Enum):
    DISABLED = 0
    ENABLED_VISIBLE = 1
    ENABLED_HIDDEN = 2
    SET_SELECTION = 3
