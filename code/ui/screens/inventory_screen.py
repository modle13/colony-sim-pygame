import logging

import pygame

from settings import *

from ui.screens.screen_state import SCREEN_STATE

from ui.screens.base_screen import BaseScreen

from util.input_manager import InputManager
from util.singleton import Singleton
from util.support import render_text_list, wrap_text


logger = logging.getLogger(__name__)


@Singleton
class InventoryScreen(BaseScreen):
    def __init__(self):
        self.screen_id = 'inventory_screen'
        super().__init__('inventory',active_states=[SCREEN_STATE.ENABLED_VISIBLE])

    def run(self):
        super().run()
        self.input()
        self.update()
        self.draw()

    def input(self,override=None):
        super().input()
        inputs = InputManager.instance()

    def update(self):
        super().update()

    def draw(self):
        super().draw()
