import logging

import pygame

from settings import config
from util import colors
from util.input_manager import InputManager


logger = logging.getLogger(__name__)


class PanelCard(pygame.sprite.Sprite):
    def __init__(self, x=0, y=0, content='placeholder', amount=0, card_count=0, on_click=None):
        pygame.sprite.Sprite.__init__(self)

        BORDER = 5
        WIDTH = 90
        HEIGHT = 50
        HALF_WIDTH = WIDTH / 2
        HALF_HEIGHT = HEIGHT / 2

        self.x_offset = card_count % config.control_panel_cards_per_row
        # rounds down
        self.y_offset = int(card_count / config.control_panel_cards_per_row)

        self.x = x + self.x_offset * (WIDTH + BORDER)
        self.y = y + self.y_offset * (HEIGHT + BORDER)

        self.content = content or 'default'
        # for structure selector use
        self.label = self.content
        self.amount = amount

        self.font = config.TINY_FONT
        self.show = True

        self.click_time = 0
        self.click_cooldown = 500
        self.can_be_clicked = True

        content_size = self.font.size(content)

        # color in rect with a border
        self.surf = pygame.Surface((WIDTH, HEIGHT))
        self.rect = self.surf.get_rect(topleft=(self.x, self.y))
        self.surf.fill(colors.GRAY)

        # draw outline; inflate offsets too annoying to deal with for later positioning
        # unless we find an easier way to get an outline of a rect
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, 0), (WIDTH, 0), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (WIDTH, 0), (WIDTH, HEIGHT), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (WIDTH, HEIGHT), (0, HEIGHT), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, HEIGHT), (0, 0), width=3)

        self.labelSurf = self.font.render(content, True, colors.OFF_WHITE)
        self.surf.blit(self.labelSurf, (BORDER, BORDER))
        self.countSurf = self.font.render(str(self.amount), True, colors.OFF_WHITE)
        self.surf.blit(self.countSurf, (BORDER, HALF_HEIGHT + BORDER))

        # bottom section horizontal
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, HALF_HEIGHT), (WIDTH, HALF_HEIGHT), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (HALF_WIDTH, HALF_HEIGHT), (HALF_WIDTH, HEIGHT), width=3)

        self.on_click = on_click if on_click else self.on_click_default

    def update(self, show):
        self.cooldowns()
        if show and self.can_be_clicked:
            self.check_click()

    def set_text(self, text):
        return
        self.contents = text
        if self.prefix:
            contents = f'{self.prefix}{text}'

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()

        if self.rect.collidepoint(mouse_pos) and InputManager.instance().get_lmb_down():
            self.on_click(self.content)
            self.click_time = pygame.time.get_ticks()
            self.can_be_clicked = False
            logger.info(f"clicked {self.content}")

    def on_click_default(self):
        logger.info(f"{name} has no click definition")

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.can_be_clicked:
            if current_time - self.click_time >= self.click_cooldown:
                self.can_be_clicked = True
