import pygame

from settings import config


class MenuElement:
    def __init__(self,name,l,t,w,h,index,font,state=0,holdfill=False,held_font=None,current=False):
        self.name = name
        self.label = name
        self.rect = pygame.Rect(l,t,w,h)
        self.index = index
        self.font = font
        self.state = state
        self.holdfill = holdfill
        self.held_font = held_font
        self.selection_color = config.colors.text_selected
        self.default_color = config.colors.text_default

    def display_names(self,surface,name,selected,held=False,current=False):
        color = self.selection_color if selected else self.default_color

        #if held:
        if current:
            text_height = self.held_font.size(self.name)[1]
            name_surf = self.held_font.render(name,False,color)
        else:
            text_height = self.font.size(self.name)[1]
            name_surf = self.font.render(name,False,color)

        name_rect = name_surf.get_rect(center = self.rect.center)
        surface.blit(name_surf,name_rect)

    def display(self,surface,selection_number,fill_percent=0,current=False):
        if self.index == selection_number:
            pygame.draw.rect(surface,config.colors.bg_color_selected,self.rect)
            if self.holdfill:
                fill_rect = self.rect.copy()
                fill_rect.width = self.rect.width * fill_percent
                pygame.draw.rect(surface,config.colors.bg_color_fill,fill_rect)
        else:
            pygame.draw.rect(surface,config.colors.ui_bg_color,self.rect)

        pygame.draw.rect(surface,config.colors.ui_border_color,self.rect,4)

        self.display_names(surface,self.name,self.index==selection_number,held=fill_percent>0.05 and self.holdfill,current=current)

    def handle_backspace(self):
        return

    def __repr__(self):
        return f"{self.name}:{self.index}; {self.rect}; state:{self.state}; holdfill:{self.holdfill}"
