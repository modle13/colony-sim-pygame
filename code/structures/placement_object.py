import logging

import pygame

from settings import config

from structures.structure import Structure
from util.events.event_queue import EventQueue
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class PlacementObject:
    def __init__(self):
        self.subscribe_to_events()

    def set_icon(self,name):
        # convert this later so whatever is updating config.game.placement_object is updating this instead
        # idk what is updating this, looks like all the code used to be in Events
        # and is now in GameStateManager but doing nothing
        config.game.placement_object = Structure(pygame.mouse.get_pos(), name, 'cursor', cursor=True)
        config.game.placement_mode_on = True
        config.game.ui_sprites.add(config.game.placement_object)

    def subscribe_to_events(self):
        pass
        # EventQueue.instance().subscribe("CLICKED_STRUCTURE_CARD",self.set_icon)
