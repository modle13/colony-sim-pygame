import pygame

from settings import config
from util.base_sprite import BaseSprite
from util import colors


class Influence(BaseSprite):
    def __init__(self, position, influence_range, show=True):
        super().__init__()
        self.label = "influence"
        self.position = position
        self.range = influence_range
        self.show = show
        if not self.range % 2: # then it's even
            self.range += 1 # but it needs to be odd for symmetry
        self.surf, self.rect = self.generate_sprite()

    def generate_sprite(self):
        unit = config.grid_square_size * self.range
        size = (unit, unit)
        # build transparent surface
        surf = pygame.Surface(size, pygame.SRCALPHA)
        rect = surf.get_rect(center=(self.position))
        # add alpha
        alpha = 12
        border_color = colors.GRAY
        fill_color = (*colors.LIGHT_GREEN, alpha)
        self.color = fill_color
        surf.fill(border_color)
        surf.fill(fill_color, surf.get_rect().inflate(-10, -10))
        return surf, rect

    def set_pos(self, pos):
        unit = config.grid_square_size
        self.rect.center = (pos[0] + unit / 2, pos[1] + unit / 2)
