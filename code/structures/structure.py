from collections import Counter
import logging
import random
import time

import pygame

from data.structure.definitions import get_pieces, get_overlay
from settings import config
from structures.disabled import Disabled
from structures.influence import Influence
from structures.product import Product
from structures.progress import Progress
from structures.structure_state import STRUCTURE_STATE
from ui.context_pane import ContextPane
from ui.text import Text
from util.base_sprite import BaseSprite
from util import colors
from util.debug import log_stack
from util.events.event_queue import EventQueue
from util.inventory import Inventory
from util.support import snap_to_grid


logger = logging.getLogger(__name__)


class Structure(BaseSprite):
    def __init__(self, position, structure_type, colony, cursor=False):
        super(Structure, self).__init__()
        # TODO: good lord

        # TIMERS

        self.STATE_ACITON_COOLDOWN_TIME_MAX = 600
        self.BUILD_ACTION_COOLDOWN_QUICKBUILD = 45

        self.unassign_allowed = False
        self.unassign_last_time = 0
        self.unassign_cooldown = 1000

        self.requested_operator = False
        self.requested_operator_time = 0
        self.requested_operator_cooldown = 1100

        self.can_check_assignment_eligibility = False
        self.check_assignment_eligibility_time = 0
        self.check_assignment_eligibility_cooldown = 750

        self.autobuild = False

        # TODO: This needs to be reorganized; some params belong in non-cursor logic
        # order should be: critical vars on top, like active, name
        # then alphabetical
        self.active = False
        self.colony = colony
        self.position = position
        self.structure_type = structure_type
        self.label = structure_type
        self.color = colors.OFF_WHITE
        self.name = structure_type
        self.sprite_type = 'structure'
        self.cursor = cursor
        self.show = True
        self.surf,self.rect,self.color = self.generate_surface()
        # self.image = self.surf
        self.pieces = get_pieces(config.unit)+get_overlay(self.structure_type,config.unit)
        self.build_stage = 0
        self.build_started = False
        structure_def = config.definitions.structures.get(self.structure_type)
        self.impervious = structure_def.get('impervious') or False
        self.operator = None
        self.context_pane = None
        # helpers; increase production rate on a curve like 1.25, 1.375, 1.45, etc.; increase smaller with each new colonist
        self.helpers = pygame.sprite.Group()
        self.debug_repr_sprite = Text(config.TINY_FONT, colors.WHITE, colors.TRANSPARENT, 0, -5)
        self.debug_repr_sprite.update(structure_type)
        self.influence = Influence(position, structure_def.get('influence_range') or 1, show=structure_def.get('show_influence'))
        self.influence.set_pos(self.rect.topleft)
        self.targets_in_range = 0
        self.is_eligible_to_request_operator = False
        # logger.info(f"cursor? {self.cursor}; influence of structure is {self.influence}")
        self.progress = 0
        self.disabled_display = Disabled(self)
        self.progress_display = Progress(self)

        self.state = None
        self.last_state = STRUCTURE_STATE.NEW

        if not self.cursor: # then it's a real structure so handle assignments and inventory
            self.active = True
            # TODO: maybe just combine all these structure defs into a struct/class
            self.helper_max = structure_def.get('max_helpers') or 0
            self.operable = structure_def.get('operable') or False
            self.target_type = structure_def.get('target_type') or ''
            self.crafting_station = structure_def.get('crafting_station') or False
            self.cultivate_data = structure_def.get('cultivate_data') or {}
            self.target_size, _ = structure_def.get('size') or (1, 1)
            self.action_archetype = structure_def.get('action_archetype') or ''
            self.workable = False
            self.autobuild = structure_def.get('autobuild',False)
            self.output_available = False
            self.claimed = False
            self.build_types = structure_def.get('build_types') or []
            self.input_types_default = structure_def.get('input_types') or []
            self.input_types = self.build_types
            self.storage_types = structure_def.get('storage_types') or []
            self.dedicated_storage = structure_def.get('dedicated_storage') or False
            # TODO: add allowed storage toggling; all by default, add to list to disallow
            self.disallowed_storage = []
            self.output_inventory = Inventory(limit=structure_def.get('capacity') or 0, kinds=self.storage_types)
            self.input_inventory = Inventory(limit=structure_def.get('capacity') or 0, purpose='input', kinds=self.input_types)
            self.input_ready = False
            self.input_full = False
            self.output_full = False
            self.full = False
            self.production_map = structure_def.get('production_map') or {}
            # TODO: think about design here; if product can't be changed this is unnecessary
            self.product_selected = None if not self.production_map else list(self.production_map.keys()).pop()
            self.build_mats = structure_def.get('build_mats')
            self.consumes_mats = {} if not self.product_selected else self.production_map.get(self.product_selected).get('consumes')
            self.produces = {} if not self.product_selected else self.production_map.get(self.product_selected).get('produces')
            self.consumes = {}
            self.set_reprs()
            self.complete_threshold = 100
            self.context_pane = ContextPane(self, self.sprite_type)
            initial_inventory = config.initial_inventory.get(self.label)
            if initial_inventory:
                self.store(initial_inventory)

        self.state_action_time = 0
        self.state_action_cooldown_time = 0
        self.state_cooldown = False

        # draw outline
        pygame.draw.rect(self.surf, self.color, pygame.Rect(0,0,config.unit,config.unit))
        # fill in outline with black
        pygame.draw.rect(self.surf, config.colors.fill, pygame.Rect(1,1,config.unit-2,config.unit-2))

        if self.cursor:
            self.set_state(STRUCTURE_STATE.CURSOR)
        else:
            self.set_state(STRUCTURE_STATE.NEW)

    def update(self):
        self.cooldowns()
        # logger.info(f"state is {self.state}")

        pos = self.rect.topleft
        text = self.structure_type
        match self.state:
            case STRUCTURE_STATE.NEW:
                self.set_state(STRUCTURE_STATE.BUILD)
                self.context_pane.update()
            case STRUCTURE_STATE.BUILD:
                if self.autobuild:
                    if not self.state_cooldown:
                        result = self.build()
                        if result:
                            self.set_state(STRUCTURE_STATE.READY)
                    return

                if not self.operator:
                    self.request_assign_operator()
                # if not self.state_cooldown:
                #     self.build()
                self.check_inventory()
                self.context_pane.update()
                self.progress_display.update(self.progress)
            case STRUCTURE_STATE.READY:
                if self.action_archetype == 'harvester':
                    self.check_nearby_resources()
                # logger.info(f"targets in range {self.targets_in_range}")
                if not self.operator and self.operable and not self.requested_operator:
                    self.check_assignment_eligibility()
                    if self.is_eligible_to_request_operator:
                        self.request_assign_operator()
                self.check_inventory()
                self.context_pane.update()
                self.progress_display.update(self.progress)
            case STRUCTURE_STATE.PAUSED:
                self.context_pane.update()
            case STRUCTURE_STATE.DISMANTLE:
                self.context_pane.update()
            case STRUCTURE_STATE.CURSOR:
                mouse_pos = config.camera.apply_pos_vector(pygame.mouse.get_pos())
                pos = snap_to_grid(mouse_pos)
                self.rect.topleft = pos
                self.position = pos
                # logger.info(f"pos is {self.rect.topleft}")

        # TODO: replace this with display update function call
        repr_pos = (pos[0] + self.rect.width / 2, pos[1] - self.debug_repr_sprite.font.get_linesize())
        self.debug_repr_sprite.update(text)
        self.debug_repr_sprite.set_pos(repr_pos)
        if self.cursor:
            self.influence.set_pos(pos)

    def set_state(self, new_state):
        if self.state == new_state:
            return

        self.last_state = self.state
        self.state = new_state

        match self.state:
            case STRUCTURE_STATE.NEW:
                self.set_state(STRUCTURE_STATE.BUILD)
            case STRUCTURE_STATE.BUILD:
                # logger.info(f"consumes is {self.consumes}")
                if not self.autobuild:
                    self.consumes = self.build_mats
                    self.request_assign_operator()
            case STRUCTURE_STATE.READY:
                self.progress = 0
                self.consumes = self.consumes_mats
                self.input_types = self.input_types_default
                self.input_inventory.initialize_contents(self.input_types)
            case STRUCTURE_STATE.PAUSED:
                self.unassign_all_colonists()
            case STRUCTURE_STATE.DISMANTLE:
                pass
            case STRUCTURE_STATE.CURSOR:
                # logger.info("setting state to CURSOR, triggering quickbuild")
                self.quickbuild()

    def check_assignment_eligibility(self):
        if not self.can_check_assignment_eligibility:
            return

        # logger.info("Checking assignment eligibility")

        self.can_check_assignment_eligibility = False
        self.check_assignment_eligibility_time = pygame.time.get_ticks()

        match self.action_archetype:
            case 'processor':
                self.is_eligible_to_request_operator = self.check_craft_materials_availability()
                # need to check if materials exist in outputs
            case 'cultivator':
                # need to check if plantable or materials exist in outputs
                self.is_eligible_to_request_operator = self.check_cultivate_item_availability()
                if not self.is_eligible_to_request_operator:
                    self.is_eligible_to_request_operator = self.check_craft_materials_availability()
            case 'harvester':
                self.is_eligible_to_request_operator = self.targets_in_range > 0

        # logger.info(f"checked assignment eligibility: {self.action_archetype} : {self.is_eligible_to_request_operator}")

    def check_craft_materials_availability(self):
        needed = self.check_needed_input()
        if not needed:
            # good to go
            return True

        # logger.info(f"{self} NEEDED ITEMS ARE {needed}")
        all_available_in_storage = True
        for key,value in needed.items():
            # find in storages
            eligible_targets = list(filter(
                lambda x: x.output_inventory.has_amount_of_kind(key,value) and x.is_ready(),
                self.colony.get_storage_structures()
            ))

            if not eligible_targets:
                all_available_in_storage = False
                break

        return all_available_in_storage

    def check_cultivate_item_availability(self):
        # return False (for testing) to avoid retrieving plantable item from output inventory
        kind = self.cultivate_data.get('name')
        # logger.info(f"searching for plantable item {kind}")
        filtered_storages = list(filter(lambda x: x.check_inventory_for(kind), self.colony.get_storage_structures()))
        return len(filtered_storages) > 0

    def quickbuild(self):
        # TODO: maybe slow this down for autobuild
        # logger.info("TRIGGERING QUICKBUILD")
        for i in range(len(self.pieces)):
            self.build()

    def draw_extra(self, screen):
        # OVERRIDE
        camera = config.camera
        # double-self intentional here because the structure object directly contains its surf/rect
        # logger.info("drawing progress")
        self.progress_display.draw()
        # self.blit(self.progress_display, screen, camera=camera)
        # self.blit(self, screen, camera=camera)
        # logger.info("drawing influence")
        self.influence.draw()
        # self.blit(self.influence, screen, camera=camera)
        # logger.info("drawing debug_repr_sprite")
        # self.debug_repr_sprite.draw()
        # self.blit(self.debug_repr_sprite, screen, camera=camera)
        if self.operator and self.context_pane.show:
            # logger.info('drawing debug repr sprite')
            pygame.draw.line(
                screen,
                colors.OFF_WHITE,
                config.camera.apply(self).center,
                config.camera.apply(self.operator).center,
                width=1
            )
        # self.blit(self.disabled_display, screen, camera=camera)

    def blit(self, obj, screen, camera=None):
        if not obj.show:
            return
        screen.blit(obj.surf, obj.rect if not camera else camera.apply(obj))

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if self.state_cooldown:
            if current_time - self.state_action_time >= self.state_action_cooldown_time:
                self.state_cooldown = False
        if not self.unassign_allowed:
            if current_time - self.unassign_last_time >= self.unassign_cooldown:
                self.unassign_allowed = True
        if self.requested_operator:
            if current_time - self.requested_operator_time >= self.requested_operator_cooldown:
                self.requested_operator = False
        if not self.can_check_assignment_eligibility:
            if current_time - self.check_assignment_eligibility_time >= self.check_assignment_eligibility_cooldown:
                self.can_check_assignment_eligibility = True

    def build(self):
        self.state_cooldown = True
        self.state_action_time = pygame.time.get_ticks()
        if self.autobuild:
            self.state_action_cooldown_time = self.BUILD_ACTION_COOLDOWN_QUICKBUILD
        else:
            if not self.build_started:
                self.build_started = True
                if hasattr(self, 'input_inventory'):
                    self.input_inventory.contents = {}
                    self.consumes = {}
            self.state_action_cooldown_time = self.STATE_ACITON_COOLDOWN_TIME_MAX

        # apply the new rect to the surface
        pygame.draw.rect(self.surf, self.color, self.pieces[self.build_stage])
        self.build_stage += 1
        # logger.info(f"PROGRESS {self.build_stage} {len(self.pieces)}")
        self.progress = int(self.build_stage / len(self.pieces) * 100)

        if self.build_stage >= len(self.pieces):
            return True
        return False

    ### WORK

    def work(self):
        if self.crafting_station and self.workable:
            self.progress += 0.5
            completed = self.progress >= self.complete_threshold
            if completed:
                self.progress -= self.complete_threshold
                self.process_work_complete()
            return completed
        else:
            return True

    def process_work_complete(self):
        self.workable = False
        for key_consumed, value_consumed in self.consumes.items():
            if key_consumed == 'ANY':
                removed = self.input_inventory.remove_first(1)
                if not removed:
                    return
            else:
                self.input_inventory.remove(key_consumed, value_consumed)
        if type(self.produces) == dict:
            for key_produced, value_produced in self.produces.items():
                self.output_inventory.add(key_produced, value_produced)
        elif type(self.produces) == int:
            self.output_inventory.add(self.product_selected, self.produces)
        else:
            logger.error(f'invalid data type detected in structure produces field; expected int or dict, got {type(self.produces)}')

    def needs_builder(self):
        return self.state == STRUCTURE_STATE.BUILD and not self.operator

    def check_nearby_resources(self):
        targets = self.colony.get_active_targets(self.name)
        # logger.info(f'active targets for {self.name} are {targets}')

        filtered_targets = list(filter(
            lambda x: not x.claimed and not x.expired and self.in_influence_range(x),
            targets
        ))
        self.targets_in_range = len(filtered_targets)

    def in_influence_range(self,target_obj):
        in_range = False
        if self.influence.range > 1:
            in_range = self.influence.rect.colliderect(target_obj.rect)
        return in_range

    ### INVENTORY

    def store(self, items: dict, inventory_type=None):
        types = self.storage_types
        inventory = self.output_inventory
        if self.crafting_station or inventory_type == "input":
            types = self.input_types
            inventory = self.input_inventory

        # logger.info(f'ADDING {items} to (type:{inventory_type}) inventory {inventory}')
        # logger.info(f'INVENTORY is {inventory}; types are {types}')

        for key, value in items.items():
            if (key in types or self.dedicated_storage) and value:
                if inventory_type != "input" or key in inventory.contents.keys() or self.dedicated_storage:
                    added = inventory.add(key, value, allow_new_key=self.dedicated_storage)
                    logger.info(f'added {added} to {inventory}')
                    items[key] = value - added
                    logger.info(f'remaining: {items}')

    def add_to_input_inventory(self, kind, amount):
        if hasattr(self, 'input_inventory'):
            self.input_inventory.add(kind, amount)
            return True
        else:
            logger.error(f'attempt was made to add to non-existent input inventory: {kind}, {amount}, target: {self.label}')
            return False

    def retrieve_specific(self, item, needed_quantity, source_claim = None) -> int:
        return_amount = self.output_inventory.remove(item, needed_quantity, source_claim)
        # logger.info(f'checking {self.name} {self.output_inventory} for {needed_quantity} of {item}')
        # log_stack()
        return {item: return_amount}

    def retrieve_any(self, needed_quantity) -> int:
        # logger.info(f'checking {self.name} {self.output_inventory} for {needed_quantity} of any item')
        returned_items = self.output_inventory.remove_any(needed_quantity)
        # logger.info(f'retrieved {returned_items} from {self.name}')
        # log_stack()
        return returned_items

    def check_inventory(self):
        if self.cursor:
            return

        self.workable = False
        if self.crafting_station or self.state == STRUCTURE_STATE.BUILD:
            self.input_full = self.input_inventory.is_full()
            self.input_ready = True
            for key, value in self.consumes.items():
                if key == 'ANY':
                    self.input_ready = self.input_inventory.has_total_of_at_least(value)
                else:
                    self.input_ready = self.input_ready and self.input_inventory.has_amount_of_kind(key, value)

            if (self.input_ready or not self.consumes) and not self.output_full:
                self.workable = True

        self.check_output_inventory()
        self.output_available = not self.output_inventory.is_empty()
        self.full = self.output_full and self.input_full

    def check_inventory_for(self, kind):
        has_kind = self.output_inventory.contains(kind)
        return has_kind

    def check_needed_input(self,item_name=None):
        # logger.info(f'{self.name} consumes {self.consumes}; current input: {self.input_inventory.contents}')

        remaining = {}

        if item_name:
            # logger.info(f"consumes {self.consumes}")
            # logger.info(f"available {item_name}: {self.input_inventory.get_available_count(kind=item_name)}")
            diff = self.consumes.get(item_name) - self.input_inventory.get_available_count(kind=item_name)
            if diff > 0:
                # logger.info(f"available {item_name} count diff is {diff}")
                remaining[item_name] = diff
        else:
            for key,needed in self.consumes.items():
                # `current` will always be a number; 0 if key does not exist
                current = self.input_inventory.get_available_count(kind=key)
                diff = needed - current
                # logger.info(f'needed {needed}, current {current}, diff {diff}')
                if diff > 0:
                    remaining[key] = diff
                    break

        # logger.info(f'{self.name} still needs {remaining}')
        return remaining

    def check_output_inventory(self) -> None:
        if not self.product_selected:
            # then this is not a producing structure
            self.output_full = False
            return

        self.output_full = False
        product_details = self.production_map.get(self.product_selected)
        produces = product_details.get('produces')
        if type(produces) == dict:
            # structure has byproducts in addition to primary product
            # so need to loop and check to see if any are full
            # and set output_full for any
            for key, value in produces.items():
                self.output_full = self.output_inventory.is_full(kind=key)
                if self.output_full:
                    # stop checking
                    return
        elif type(produces) == int:
            # then just check product selected because there's no byproduct
            self.output_full = self.output_inventory.is_full(kind=self.product_selected)

    def output_has_finished_product(self):
        if self.action_archetype == 'cultivator':
            # then the production map is items that need to stay with the structure
            # such as wheat seed, sapling, etc.
            # i.e. not a finished product to be used in other structures, but to be used
            # by current structure to plant a resource
            keys_to_ignore = self.production_map.keys()
            for k,v in self.output_inventory.contents.items():
                if k not in keys_to_ignore:
                    return True
            return False
        else:
            return True

    def drop_all_inventory(self):
        if self.cursor:
            return
        input_inventory = {} if not hasattr(self, 'input_inventory') else self.input_inventory.contents
        all_inventory = dict(Counter(self.output_inventory.contents) + Counter(input_inventory))
        for key, value in all_inventory.items():
            self.colony.attempt_product_add(key,value,self.position)

    def count_all_inventory(self):
        # cache all structure inventory on inventory change
        if self.cursor:
            return
        input_inventory = {} if not hasattr(self, 'input_inventory') else self.input_inventory.contents
        all_inventory = dict(Counter(self.output_inventory.contents) + Counter(input_inventory))
        return all_inventory

    ### STATE

    def unassign(self):
        if self.unassign_allowed:
            self.unassign_all_colonists()
            self.unassign_last_time = pygame.time.get_ticks()
            self.unassign_allowed = False

    def toggle(self):
        self.active = not self.active
        if self.active:
            self.set_state(self.last_state)
        else:
            self.set_state(STRUCTURE_STATE.PAUSED)
        self.disabled_display.show = not self.active
        logger.info(f'{self.structure_type} active? {self.active}; operator: {self.operator}; helpers: {self.helpers}')
        logger.info(f'show disabled? {self.disabled_display.show}')

    def activate(self):
        self.set_state(self.last_state)
        self.active = True

    def deactivate(self):
        if self.impervious:
            return
        self.set_state(STRUCTURE_STATE.PAUSED)
        self.active = False

    def destroy(self):
        if self.impervious and not self.cursor:
            return
        self.unassign_all_colonists()
        self.influence_surf = None
        self.drop_all_inventory()
        self.kill()

    def is_ready(self):
        return self.state == STRUCTURE_STATE.READY

    def can_build(self):
        return self.state == STRUCTURE_STATE.BUILD and not self.state_cooldown

    def finish_build(self):
        self.unassign_all_colonists()
        self.set_state(STRUCTURE_STATE.READY)

    def is_cursor(self):
        return self.state == STRUCTURE_STATE.CURSOR

    ### HELPERS

    def request_assign_operator(self):
        if self.requested_operator:
            # logger.info("already requested operator")
            return
        EventQueue.instance().publish("ASSIGN_OPERATOR",[self])
        self.requested_operator = True
        self.requested_operator_time = pygame.time.get_ticks()

    def assign_operator(self,operator):
        self.operator = operator
        self.active = True
        self.requested_operator = False

    def unassign_all_colonists(self):
        if self.operator:
            # relieve operator from duty
            self.operator.unassign_role()
            # disassociate operator colonist from structure
            self.operator = None
        for helper in self.helpers:
            if helper:
                helper.unassign_role()
                self.helpers.remove(helper)

    def request_assign_helper(self):
        EventQueue.instance().publish("ASSIGN_HELPER",[self])

    def assign_helper(self,helper):
        if helper:
            self.helpers.add(helper)

    ### GRAPHICS

    def generate_surface(self):
        details = config.definitions.structures.get(self.structure_type)

        size = details.get('size') or (1, 1)
        self.default_size = tuple(config.grid_square_size * x for x in size)
        surf = pygame.Surface(self.default_size, pygame.SRCALPHA)

        rect = surf.get_rect(topleft=self.position)

        # want to return rather than setting via self dot-notation so we can clearly see the params in init
        return surf, rect, details.get('color')

    def generate_surface_polygon(self):
        details = config.definitions.structures.get(self.structure_type)
        size = details.get('size') or (1, 1)
        self.default_size = tuple(config.grid_square_size * x for x in size)
        polygon_points = details.get('polygon')
        # make transparent surface to draw polygon onto
        # SRCALPHA makes it transparent; this is the surface the polygon will get drawn on
        surf = pygame.Surface(self.default_size, pygame.SRCALPHA)
        # scale the polygon_points
        points = list(map(lambda x: (x[0] * self.default_size[0], x[1] * self.default_size[1]), polygon_points))
        pygame.draw.polygon(surf, details.get('color'), points)
        rect = surf.get_rect(topleft=self.position)
        # want to return rather than setting via self dot-notation so we can clearly see the params in init
        return surf, rect

    ### REPRS

    def show_context(self):
        self.context_pane.display()

    def inventory_repr(self):
        return self.output_inventory.get_contents_repr()

    def input_inventory_repr(self):
        return self.input_inventory.get_contents_repr()

    def label_repr(self):
        return self.label

    def state_repr(self):
        text = f'{self.state}'
        if self.state == STRUCTURE_STATE.BUILD:
            text = f'{text}: {self.build_stage}/{len(self.pieces)}'
        return text

    def position_repr(self):
        return self.rect.center

    def workable_repr(self):
        workable_msg = 'NO'
        if self.workable:
            workable_msg = 'YES'
        elif self.output_full:
            workable_msg = f'{workable_msg} (OUTPUT FULL)'
        else:
            workable_msg = f'{workable_msg} (MISSING INPUTS)'
        return workable_msg

    def progress_repr(self):
        return f'{self.progress:.2f}%'

    def targets_repr(self):
        return f'{self.targets_in_range}'

    def set_reprs(self):
        self.representation = {
            'output': self.inventory_repr,
            'label': self.label_repr,
            'state': self.state_repr,
            'position': self.position_repr,
        }
        # if self.crafting_station:
        self.representation['input'] = self.input_inventory_repr
        self.representation['ready for work'] = self.workable_repr
        self.representation['progress'] = self.progress_repr
        self.representation['targets'] = self.targets_repr

    def __repr__(self):
        return self.name
