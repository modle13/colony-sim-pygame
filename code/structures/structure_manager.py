import logging

import pygame

from entities.colonists.colonist import Colonist
from settings import config
from state.game_state_manager import GameStateManager
from structures.structure import Structure
from util import keys
from util.debug import log_stack
from util.events.event_queue import EventQueue
from util.input_manager import InputManager


logger = logging.getLogger(__name__)


class StructureManager:
    def __init__(self,colony):
        self.colony = colony

        self.repair_can_tick = False
        self.repair_tick_delay = 2000
        self.repair_last_tick = 0

        self.structures = pygame.sprite.Group()
        self.subscribe_to_events()

        logger.info(f"StructureManager for {colony} initialized")

    def update(self):
        self.cooldowns()
        self.manage_structures()

    def manage_structures(self):
        if not self.repair_can_tick:
            return
        self.check_structure_repair()

    def check_structure_repair(self):
        pass

    def make_structure(self, kind=None, position=None):
        # log_stack()
        # logger.info(f"MAKING STRUCTURE {kind}")
        if not kind or not position:
            logger.error(f'not sure what this structure is or where to put it: kind={kind}, position={position}')

        new_structure = Structure(position, kind, self.colony)
        self.structures.add(new_structure)
        self.colony.add_structure(new_structure)

        # hook for particles etc
        EventQueue.instance().publish("STRUCTURE_SPAWNED",[new_structure])

    def check_structure_right_click(self,pos):
        for entry in self.structures:
            if config.camera.apply(entry).collidepoint(pos):
                if InputManager.instance().get_delete():
                    entry.destroy()
                else:
                    entry.toggle()
                break

    def set_mouse_structure(self,name):
        new_placement_object = Structure(pygame.mouse.get_pos(), name, 'cursor', cursor=True)
        logger.info(f"created placement object {name}")
        config.game.placement_object = new_placement_object
        self.colony.add_structure(new_placement_object)
        GameStateManager.instance().placement_mode_on = True
        EventQueue.instance().publish('ADD_PLACEMENT_OBJECT',[new_placement_object])

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.repair_can_tick:
            if current_time - self.repair_last_tick >= self.repair_tick_delay:
                self.repair_can_tick = True

    def subscribe_to_events(self):
        EventQueue.instance().subscribe('CLICKED_STRUCTURE_CARD',self.set_mouse_structure)
        EventQueue.instance().subscribe('RIGHT_CLICK',self.check_structure_right_click)
