from enum import Enum


class STRUCTURE_STATE(Enum):
    NEW = 0
    BUILD = 1
    READY = 2
    PAUSED = 3
    DISMANTLE = 4
    CURSOR = 5
