import math

import pygame

from data.structure import definitions
from entities.colonists.target_enum import TARGET_TYPE
from ui.menu_config import get_menu_defs
from util import colors


pygame.init()


class Config(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)

class Colors(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)

config = Config({})
# this will need to be more specific if other definitions get added
config.definitions = definitions
config.debug = False

config.FPS = 60

#################################
#####        COLORS         #####
#################################

color_map = {
    'bg_color_fill': '#4f7ff0',
    'bg_color_selected': colors.YELLOW,
    'grid': colors.LIGHT_BLUE,
    'product': colors.MAGENTA,
    'resource': colors.LIGHT_BLUE,
    'storage': colors.YELLOW,
    'title': colors.WHITE,
    'ui_bg_color': colors.BLACK,
    'ui_border_color': colors.GRAY,
    'text_selected': '#111111',
    'text_default': colors.WHITE,
    'reticle': colors.WHITE,
    'fill': colors.BLACK,
}
config.colors = Colors(color_map)

#################################
##### SIZES AND DIMENSIONS  #####
#################################

# Define constants for the screen width and height
config.SCREEN_WIDTH = 1200
config.SCREEN_HEIGHT = 900
#config.SCREEN_WIDTH = 800
#config.SCREEN_HEIGHT = 600
config.SCREEN_UNIT = config.SCREEN_WIDTH//40


# useful precalculations
config.THREE_QUARTER_WIDTH  = config.SCREEN_WIDTH  / 4 * 3
config.THREE_QUARTER_HEIGHT = config.SCREEN_HEIGHT / 4 * 3
config.HALF_WIDTH           = config.SCREEN_WIDTH  / 2
config.HALF_HEIGHT          = config.SCREEN_HEIGHT / 2
config.THIRD_WIDTH          = config.SCREEN_WIDTH  / 3
config.THIRD_HEIGHT         = config.SCREEN_HEIGHT / 3
config.QUARTER_WIDTH        = config.SCREEN_WIDTH  / 4
config.QUARTER_HEIGHT       = config.SCREEN_HEIGHT / 4
config.SCREEN_CENTER        = (config.HALF_WIDTH, config.HALF_HEIGHT)
#screen_divisor = 35
screen_divisor = 30
unit = int(config.SCREEN_WIDTH / screen_divisor)
config.unit = unit
config.grid_square_size = unit
config.tile_size = unit
#config.world_size = 1.5
config.world_size = 1.5
config.max_width  = int(config.SCREEN_WIDTH  * config.world_size)
config.max_height = int(config.SCREEN_HEIGHT * config.world_size)
# columns are related to x coords / width; offset by 1 to avoid black edges on right and bottom
config.num_grid_columns = int(config.max_width / unit) + 1
#config.num_grid_columns = int(config.max_width / unit) + 2
# rows are related to y coords / height
config.num_grid_rows = int(config.max_height / unit) + 1
#config.num_grid_rows = int(config.max_height / unit) + 2
config.grid_snap_offset = 5

config.control_panel_vertical_pos = config.THREE_QUARTER_HEIGHT
config.control_panel_content_pos = (config.unit * 2.5, 0)
# config.control_panel_message_feed_pos = (config.SCREEN_WIDTH - config.unit * 5, 5)
config.control_panel_message_feed_pos = (config.SCREEN_WIDTH - config.unit * 8, 5)
# info popup no longer used, popping out a separate box instead of putting it in control panel
# config.control_panel_info_popup_pos = (config.SCREEN_WIDTH - config.unit * 8, 0)
config.control_panel_cards_per_row = 6


config.movement_speed = 4

#################################
##### COLONIST SETTINGS     #####
#################################

# config.max_colonists = 15000
config.max_colonists = 15
# config.max_colonists = 3
config.colonist_def = {
    'default_role': 'hauler',
    'default_action_archetype': 'porter',
    'hunger_threshold': 400,
    'inventory_size': 2,
    'speed': config.movement_speed,
    'start_food': 400,
    #'surf_size': (unit // 2, unit // 2),
    'surf_size': (unit // 4, unit // 4),
    'tick_time': 200,
}

#################################
##### RESOURCE SETTINGS     #####
#################################

config.max_creatures = 5
config.max_trees = 25
config.max_shrooms = 10
config.max_wheat = 10

config.growth_sizes = [
    int(unit / 5),
    int(unit / 5) * 2,
    int(unit / 5) * 3,
    int(unit / 5) * 4,
    unit,
]

config.type_sizes = {
    'creature': math.floor(len(config.growth_sizes) / 2),
    'shroom'  : math.floor(len(config.growth_sizes) / 2),
    'wheat'   : math.floor(len(config.growth_sizes) / 2),
    'dummy'   : len(config.growth_sizes)                ,
    'tree'    : len(config.growth_sizes)                ,
}

# seconds
config.growth_rate = {
    'min': 0.1,
    'max': 0.3,
}

config.resource_product_types = {
    'creature': {'meat'     : 2, 'hide'      : 1, 'bone' : 3, 'fat'    : 2,},
    'shroom':   {'shroombit': 3, 'compost'   : 1                          ,},
    'tree':     {'log'      : 3, 'compost'   : 2                          ,},
    'wheat':    {'straw'    : 1, 'wheat_germ': 2, 'grain': 2, 'compost': 2,},
}

config.resource_events = {
    'ADDCREATURE': {
        'max': config.max_creatures,
        'name': 'creature',
        'role': 'hunter',
    },
    'ADDTREE': {
        'max': config.max_trees,
        'name': 'tree',
        'role': 'woodcutter',
    },
    'ADDSHROOM': {
        'max': config.max_shrooms,
        'name': 'shroom',
        'role': 'forager',
    },
    'ADDWHEAT': {
        'max': config.max_wheat,
        'name': 'wheat',
        'role': 'thresher',
    },
}

config.food_values = {
    'soup': 600,
    'shroombit': 200,
}

#################################
##### FONT SETTINGS         #####
#################################

config.font = 'freesansbold.ttf'
config.font_2 = '../graphics/font/joystix.ttf'
config.MENU_FONT      = pygame.font.Font(config.font, int(110 / 1080 * config.SCREEN_HEIGHT))
config.LARGE_FONT     = pygame.font.Font(config.font, int(40  / 1080 * config.SCREEN_HEIGHT))
config.MEDIUM_FONT    = pygame.font.Font(config.font, int(35  / 1080 * config.SCREEN_HEIGHT))
config.SMALL_FONT     = pygame.font.Font(config.font, int(25  / 1080 * config.SCREEN_HEIGHT))
config.SMALLER_FONT   = pygame.font.Font(config.font, int(20  / 1080 * config.SCREEN_HEIGHT))
config.TINY_FONT      = pygame.font.Font(config.font, int(15  / 1080 * config.SCREEN_HEIGHT))
config.TINIER_FONT    = pygame.font.Font(config.font, int(10  / 1080 * config.SCREEN_HEIGHT))


#################################
##### STRUCTURE DEFINITIONS #####
#################################

# assume buildable if buildable flag not present
config.buildable_structures = list(
    dict(filter(
        lambda elem: True if 'buildable' not in elem[1] else elem[1]['buildable'],
        definitions.structures.items()
    )).keys()
)

config.starting_structures = [
    {
        'kind': 'town_center',
        # floor to snap it to the grid (should make that a common function)
        'position': (
            math.floor(config.THIRD_WIDTH  / unit) * unit - config.grid_snap_offset,
            math.floor(config.THIRD_HEIGHT / unit) * unit - config.grid_snap_offset,
        ),
    },
]

config.storage_structures = list(
    dict(
        filter(
            lambda x: x[1].get('target_type') == TARGET_TYPE.STORAGE, definitions.structures.items()
        )
    ).keys()
)

config.storage_items = definitions.storage_items

config.initial_inventory = {
    'town_center': {
        'fertilizer': 2,
        'log': 2,
        'shroombit': 2,
        'coins': 2,
        'compost': 2,
        'grain': 2,
        'wheat_germ': 2,
        'wheat_seed': 1,
        'soup': 2,
        'straw': 2,
        'plank': 10,
        'stone': 10,
    }
}


#################################
##### MENU AND UI SETTINGS  #####
#################################

config.menu_defs = get_menu_defs(config)
config.sprite_context_defs = {
    'colonist': {
        'fields': ['role', 'position', 'target', 'state', 'status', 'food', 'inventory',],
        'font': config.SMALL_FONT,
    },
    'resource': {
        'fields': ['label', 'position', 'progress', 'claimed',],
        'font': config.SMALL_FONT,
    },
    'product': {
        'fields': ['label', 'position', 'claimed',],
        'font': config.SMALL_FONT,
    },
    'structure': {
        'fields': ['label', 'state', 'position', 'ready for work', 'progress', 'targets', 'input', 'output',],
        'font': config.TINY_FONT,
    },
}


#################################
#####   RESOURCE SETTINGS   #####
#################################

creature_polygon = [
    #top left, moving clockwise around contour
    # head
    (0    , 0    ),
    (2 / 6, 0    ),
    # body
    (2 / 6, 2 / 6),
    (1    , 2 / 6),
    # back leg
    (1    , 1    ),
    (5 / 6, 1    ),
    # belly
    (5 / 6, 4 / 6),
    # front leg
    (3 / 6, 4 / 6),
    (3 / 6, 1    ),
    (2 / 6, 1    ),
    # chest
    (2 / 6, 2 / 6),
    # head
    (0    , 2 / 6),
]

tree_polygon = [
    (1 / 4, 1    ),
    (1 / 2, 1 / 4),
    (3 / 4, 1    )
]

wheat_polygon = [
    (0    , 1    ), #start, at left
    (1 / 6, 1 / 3), # first peak
    (2 / 6, 1    ), # first valley
    (3 / 6, 1 / 3), # second peak
    (4 / 6, 1    ), # second valley
    (5 / 6, 1 / 3), # third peak
    (1    , 1    ), # finish
]

config.resources = {
    'creature': {'points': creature_polygon, 'color': colors.TAN         ,},
    'shroom'  : {'points': None,             'color': colors.LIGHT_BROWN ,},
    'tree'    : {'points': tree_polygon ,    'color': colors.LIGHT_GREEN ,},
    'wheat'   : {'points': wheat_polygon,    'color': colors.LIGHT_YELLOW,},
}
