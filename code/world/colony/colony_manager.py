from collections import Counter
import logging

import pygame

from util.events.event_queue import EventQueue
from util.singleton import Singleton
from world.colony.colony import Colony


logger = logging.getLogger(__name__)


@Singleton
class ColonyManager:
    def __init__(self):
        self.colonies = {}
        self.active_colony_index = 0
        self.active_colony = None

        self.structures = []

        self.inventory = {}

        # TIMERS
        self.inventory_update_time = 0
        self.inventory_update_cooldown = 500
        self.inventory_updated = False

        self.subscribe_to_events()

        logger.info("ColonyManager initialized")

    def initialize(self,regions):
        self.create_colony("new colony",regions)

    def update(self):
        self.cooldowns()
        # logger.info("update called in colony_manager")
        for k,v in self.colonies.items():
            v.update()
        self.update_inventory(self.active_colony)

    def draw(self):
        if self.active_colony:
            self.active_colony.draw()

    def create_colony(self,name,regions):
        colony = Colony(name)
        self.colonies[name] = colony
        self.active_colony_name = name
        self.active_colony = self.colonies[self.active_colony_name]
        colony.add_region(regions)

    def get_active_colonists(self):
        if self.active_colony:
            return self.active_colony.colonist_manager.colonists
        return []

    def get_active_structures(self):
        if self.active_colony:
            return self.active_colony.structure_manager.structures
        return []

    def update_inventory(self,colony):
        if self.inventory_updated:
            return

        self.inventory = {}

        # TODO: only trigger this on event signal, sent when targets' inventories change
            # or when active_colony changes
        eligible_targets = colony.get_storage_structures()
        for ea in eligible_targets:
            self.inventory = dict(Counter(self.inventory) + Counter(ea.count_all_inventory()))
        self.inventory_updated = True
        self.inventory_update_time = pygame.time.get_ticks()
        # logger.info(f"active inventory is {self.inventory}")

    def assign_colonist(self,structure):
        # logger.info(f"attempting to assign a colonist to {structure.name} in colony {structure.colony}")
        colony = structure.colony
        if not colony:
            logger.error(f'unable to find colony with name {structure.colony_name}')
            return

        colony.assign_colonist(structure)

    def check_for_sprite_click(self,pos):
        clicked = self.active_colony.check_for_sprite_click(pos)
        if clicked:
            EventQueue.instance().publish("SHOW_CONTEXT_PANE",[clicked])

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if self.inventory_updated:
            if current_time - self.inventory_update_time >= self.inventory_update_cooldown:
                self.inventory_updated = False

    def subscribe_to_events(self):
        EventQueue.instance().subscribe("ASSIGN_HELPER",self.assign_colonist)
        EventQueue.instance().subscribe("ASSIGN_OPERATOR",self.assign_colonist)
        EventQueue.instance().subscribe("CHECK_FOR_SPRITE_CLICK",self.check_for_sprite_click)
        EventQueue.instance().subscribe("GAME_INITIALIZED",self.initialize)
