import pygame

import logging

from entities.colonists.colonist_manager import ColonistManager
from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from structures.structure_manager import StructureManager
from util.events.event_queue import EventQueue
from util.input_manager import InputManager
from util.inventory import Inventory
from util.support import check_for_world_object_click


logger = logging.getLogger(__name__)


# TODO: colony sprite groups should go HERE, not in main.py
class Colony:
    def __init__(self,name):
        self.name = name
        self.inventory = Inventory(limit_type='unlimited', purpose='colony', kinds=config.storage_items)
        self.colonist_manager = ColonistManager(self)
        self.structure_manager = StructureManager(self)

        self.regions = []

        self.subscribe_to_events()

        logger.info(f"Colony '{name}' initialized")

    def update(self):
        # logger.info("updating colony")
        self.colonist_manager.update()
        self.structure_manager.update()
        self.cooldowns()

    def draw(self):
        self.colonist_manager.draw()
        for ea in self.regions:
            ea.draw()

    def cooldowns(self):
        current_time = pygame.time.get_ticks()

    def process_placement_click(self,target_pos):
        target_square = self.get_collide_grid_square(target_pos)
        if not target_square:
            logger.error(f'location {target_pos} is occupied; not placing {config.game.placement_object.structure_type}')
            return
        # TODO: reverse the order of these params to match Structure object
        # logger.info(f'making a {config.game.placement_object} from {config.game.placement_object.structure_type} at {target_square.rect.topleft}')
        self.structure_manager.make_structure(
            config.game.placement_object.structure_type,
            # config.camera.apply_pos_vector(target_square.rect.topleft)
            target_square.rect.topleft
        )
        # logger.info(f'making a {config.game.placement_object.structure_type} at {target_square.rect.topleft}')
        # enables multi-press mode
        if not InputManager.instance().get_shift():
            logger.info('resetting placement mode')
            EventQueue.instance().publish("RESET_PLACEMENT_MODE")
            # self.reset_placement_mode()

    def attempt_resource_add(self,resource,automatic,pos):
        self.regions[0].attempt_resource_add(resource,automatic,pos)

    def attempt_product_add(self,name,amt,pos):
        # TODO get region by position; or assign source a region ref instead of just colony ref
            # would have to update colonist region periodically
        self.regions[0].attempt_product_add(name,amt,pos)

    def get_collide_grid_square(self,position):
        unoccupied_squares = self.get_unpopulated_tiles()
        # logger.info(f"unoccupied squares are {unoccupied_squares}")
        for grid_square in unoccupied_squares:
            if config.camera.apply(grid_square).collidepoint(position):
                logger.info(f"found a colliding point sprite {grid_square}; click pos:{position}")
                return grid_square
        # logger.info(f"did not find a colliding point sprite {grid_square}; click pos:{position}")
        return None

    def set_regions(self,regions):
        for ea in regions:
            ea.assign_region(self.name)
        self.regions = regions

    def add_region(self,region):
        region.assign_region(self.name)
        self.regions.append(region)
        logger.info(f"Colony '{self.name}' added region '{region}'")

    def get_active_targets(self,target_type):
        return self.regions[0].active_targets.get(target_type) or []

    def get_unpopulated_tiles(self):
        return self.regions[0].get_unpopulated_tiles()

    def get_products(self):
        # TODO update list of products from all regions on a timer and cache it here
        return self.regions[0].products

    def get_resources(self):
        # TODO update list of resources from all regions on a timer and cache it here
        return self.regions[0].resources

    def get_storage_structures(self):
        # TODO update list of structures from all regions on a timer and cache it here
        return self.regions[0].storage_structures

    def get_structures(self):
        # TODO update list of structures from all regions on a timer and cache it here
        return self.regions[0].structures

    def get_targets(self):
        # TODO update list of targets from all regions on a timer and cache it here
        return self.regions[0].structures

    def add_structure(self,new_structure):
        self.regions[0].add_structure(new_structure)

    def add_blocking_sprites(self,new_structure):
        self.regions[0].blocking_sprites.add(new_structure)

    def add_storage_structure(self,new_structure):
        self.regions[0].storage_structures.add(new_structure)

    def assign_colonist(self,structure):
        # get next unassigned colonist (non-default)
        # for ea in self.colonist_manager.colonists:
        #     logger.info(f"colonists {self.colonist_manager.colonists}; {ea}: {ea.role}; default role {config.colonist_def.get('default_role')}")
        colonist = next((
            x for x in self.colonist_manager.colonists \
            if x.assignment_change_allowed and \
            x.target and x.target.target_type == TARGET_TYPE.DUMMY
        ) , None)
        # logger.info(f'target colonist for assignment is {colonist}')
        if colonist:
            target_role = None
            if structure.needs_builder():
                colonist.set_role("builder")
            else:
                colonist.set_role(structure.structure_type)
            colonist.action_archetype = structure.action_archetype
            colonist.assignment = structure
            colonist.target = structure
            structure.assign_operator(colonist)

    def check_for_sprite_click(self, position):
        scaled_position = (position[0]*config.game.scale,position[1]*config.game.scale)
        found = check_for_world_object_click(self.colonist_manager.colonists,scaled_position)
        if not found:
            for ea in self.regions:
                found = check_for_world_object_click(ea.game_objects,scaled_position)
                # found = ea.check_for_sprite_click(position)
                if found:
                    break

        return found

    def check_right_click(self,pos):
        for entry in self.get_structures():
            if config.camera.apply(entry).collidepoint(pos):
                if InputManager.instance().get_delete():
                    entry.destroy()
                else:
                    entry.toggle()
                break

    def subscribe_to_events(self):
        EventQueue.instance().subscribe('PROCESS_PLACEMENT_CLICK',self.process_placement_click)
        EventQueue.instance().subscribe('ADD_RESOURCE',self.attempt_resource_add)
        EventQueue.instance().subscribe('RIGHT_CLICK',self.check_right_click)

    def __repr__(self):
        return self.name
