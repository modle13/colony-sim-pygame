import logging

from util.singleton import Singleton

from world.colony.colony_manager import ColonyManager
from world.region.region_manager import RegionManager
from settings import config


logger = logging.getLogger(__name__)


@Singleton
class World:
    def __init__(self):
        logger.info("World initialized")
        RegionManager.instance().initialize()
        ColonyManager.instance().initialize(RegionManager.instance().get_unassigned_region())

    def update(self):
        # logger.info("updating world")
        ColonyManager.instance().update()
        RegionManager.instance().update()
        config.game.structures = ColonyManager.instance().get_active_structures()

    def get_active_colony_colonists(self):
        return ColonyManager.instance().get_active_colonists()

    def draw(self):
        ColonyManager.instance().draw()
        # RegionManager.instance().draw()
