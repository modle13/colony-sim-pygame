import logging
import random
import time

import pygame

from entities.colonists import movement
from entities.colonists.target import Target
from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from structures.product import Product
from structures.progress import Progress
from ui.context_pane import ContextPane
from util.events.event_queue import EventQueue
from util import colors
from util import polygons


logger = logging.getLogger(__name__)


class Resource(pygame.sprite.Sprite):
    def __init__(self, region, position, resource_type):
        super(Resource, self).__init__()
        self.region = region
        self.position = position
        self.resource_type = resource_type
        self.label = resource_type
        self.name = resource_type
        self.color = config.resources.get(resource_type).get('color')
        self.size = 0
        self.target_size = 5 if self.resource_type == 'creature' else 1
        self.max_size = config.type_sizes.get(self.resource_type)
        self.surf, self.rect = self.generate_surface()
        self.sprite_type = 'resource'
        self.target_type = TARGET_TYPE.RESOURCE
        self.active = False
        self.claimed = False
        self.expired = False
        self.start_time = time.time()
        self.progress = 0
        self.progress_display = Progress(self)
        self.complete_threshold = 100
        self.set_grow_time()
        self.show = True
        self.set_reprs()
        self.context_pane = ContextPane(self, self.sprite_type)
        # logger.info(f'spawned resource of type <{resource_type}>')

    def generate_surface(self):
        side = config.growth_sizes[self.size]
        # SRCALPHA makes it transparent; this is the surface the polygon will get drawn on
        surf = pygame.Surface((side, side), pygame.SRCALPHA)

        polygons.draw_shape(surf, self)
        position = self.position
        if hasattr(self, 'rect'):
            position = self.rect.center
        rect = surf.get_rect(center=position)
        # want to return rather than setting via self dot-notation so we can clearly see the params in init
        return surf, rect

    def work(self):
        self.progress += 2
        completed = self.progress >= self.complete_threshold
        if completed:
            self.progress -= self.complete_threshold
            self.process_work_complete()
        return completed

    def update(self):
        self.context_pane.update()
        self.progress_display.update(self.progress)
        if self.resource_type == 'creature':
            self.manage_target()
            movement.move_toward_target(self, self.target.rect)
        if self.active:
            return

        if self.size < self.max_size:
            if time.time() - self.start_time > self.grow_time:
                self.grow()
        else:
            self.activate()

    def manage_target(self):
        """
        for living resources
        """
        if not hasattr(self, 'target') or self.check_reached_target():
            self.target = Target(colors.get_random_color())

    def check_reached_target(self):
        """
        for living resources
        """
        collided = pygame.sprite.collide_rect(self, self.target)
        return collided

    def draw(self, screen, camera=None):
        screen.blit(self.progress_display.surf, self.progress_display.rect if not camera else camera.apply(self.progress_display))

    def grow(self):
        self.set_grow_time()
        self.surf, self.rect = self.generate_surface()
        self.size += 1
        self.set_grow_time()
        self.start_time = time.time()

    def set_grow_time(self):
        # random float within range
        self.grow_time = random.uniform(config.growth_rate.get('min'), config.growth_rate.get('max'))

    def activate(self):
        self.active = True

    def destroy(self):
        self.active = False
        self.claimed = False
        self.expired = True
        self.kill()

    def process_work_complete(self):
        # 3 params needed: object to create, how many to create, where to put it
        # quantity would vary with type of resource/building being worked
        products = config.resource_product_types.get(self.resource_type)
        for name, amt in products.items():
            self.region.attempt_product_add(name,amt,self.rect.center)

    def show_context(self):
        self.context_pane.display()

    def label_repr(self):
        return self.label

    def position_repr(self):
        return self.rect.center

    def progress_repr(self):
        return f'{self.progress}%'

    def claimed_repr(self):
        return "TRUE" if self.claimed else "FALSE"

    def set_reprs(self):
        self.representation = {
            'label': self.label_repr,
            'position': self.position_repr,
            'progress': self.progress_repr,
            'claimed': self.claimed_repr,
        }
