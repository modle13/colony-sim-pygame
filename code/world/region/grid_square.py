import math
import random

import pygame

from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from structures.product import Product
from structures.progress import Progress
from util import colors
from util import keys


# much of this logic will get lifted to a 'workable' object, building, etc.; see issue 9
# these should not have workable properties, but should be static entities
# workable objects would get drawn on top? would replace?
# not every grid square will be a workable object, so danger of god object
# this should be bare-bones with position/color properties only
class GridSquare(pygame.sprite.Sprite):
    def __init__(self, x, y, color):
        super(GridSquare, self).__init__()
        self.border_width = 1
        self.start = (x, y)
        self.am_clicked = False
        self.surf = pygame.Surface((config.grid_square_size, config.grid_square_size))
        self.rect = self.surf.get_rect(topleft=self.start)
        self.border_color = color
        self.color = color
        self.set_default_border()
        self.active = False
        self.claimed = False
        self.target_type = TARGET_TYPE.GRID
        self.sprite_type = 'grid'
        self.label = 'grid'
        self.name = 'grid'
        self.progress = 0
        self.progress_display = Progress(self)
        self.complete_threshold = 100
        self.show = True
        self.target_size = 1

    def activate(self):
        self.surf.fill(colors.GRAY)
        self.active = True
        self.claimed = False

    def deactivate(self):
        self.set_default_border()
        self.active = False
        self.claimed = False

    def set_default_border(self):
        # to make border, first fill whole rect with single color
        self.surf.fill(self.border_color)
        # then fill all but 2 pixels on edge with black
        self.surf.fill(colors.BLACK, self.surf.get_rect().inflate(-self.border_width * 2, -self.border_width * 2))

    def work(self):
        self.progress += 3
        completed = self.progress >= self.complete_threshold
        if completed:
            # why not just zero it out, not sure this makes sense
            #   would end up with a value of 2 every time
            self.progress -= self.complete_threshold
        self.progress_display.update(self.progress)
        return completed

    def process_work_complete(self):
        # TODO this never gets called
        logger.info("PROCESSED WORK COMPLETE ON GRID SQUARE")
        # attrs can be custom
        # 3 params needed: object to create, where to put it, how many to create
        # quantity would vary with type of resource/building being worked
        # position = (self.rect.center[0], self.rect.center[1])
        # create the event with ADDPRODUCT ID
        # self.colony.attempt_product_add(key,value,self.position)
        # make_thing = pygame.event.Event(config.game.events.ADDPRODUCT, product=Product, product_type='dummy', position=position, quantity=3)
        # # post event to event queue
        # pygame.event.post(make_thing)

    def on_click(self, mouse_pos):
        if config.camera.apply(self).collidepoint(mouse_pos):
            if self.active:
                self.deactivate()
            else:
                self.activate()

    def __repr__(self):
        return f'{self.name}:{self.start}'
