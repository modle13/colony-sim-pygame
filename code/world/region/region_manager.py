import logging

from world.region.region import Region
from util.events.event_queue import EventQueue
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class RegionManager:
    def __init__(self):
        self.regions = []
        self.subscribe_to_events()
        logger.info("RegionManager initialized")

    def initialize(self):
        self.regions = [Region(1),Region(2)]
        logger.info("RegionManager regions created")

    def update(self):
        # logger.info("update called in region_manager")
        for ea in self.regions:
            ea.update()

    def draw(self):
        pass

    def get_unassigned_region(self):
        for ea in self.regions:
            if not ea.assigned:
                return ea
        return None

    def subscribe_to_events(self):
        pass
        # EventQueue.instance().subscribe('SOME_EVENT',self.some_func)
