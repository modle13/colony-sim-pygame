import random

import pygame

from settings import config
from util import colors
from world.region.grid_square import GridSquare


class Grid:
    def __init__(self):
        self.grid_sprites = pygame.sprite.Group()
        new_grid_sprites = self.generate_squares()
        self.grid_sprites.add(new_grid_sprites)

    def update(self):
        self.grid_sprites.update()

    def get_random_grid_square(self,squares=None):
        if not squares:
            squares = self.get_unpopulated_tiles().sprites()
        try:
            random_sprite = random.choice(squares)
        except IndexError:
            random_sprite = None
        return random_sprite

    def get_occupied_tiles(self,sprite_group,blocking_sprites):
        occupied_tiles = pygame.sprite.groupcollide(sprite_group, blocking_sprites, False, False)
        return occupied_tiles

    def get_unpopulated_tiles(self,blocking_sprites):
        # FIXME: will not scale well
        #   will need to be chunked within each region to avoid having too large
        #   of a copy each time an unoccupied tile is needed
        unoccupied_squares = self.grid_sprites.copy()
        occupied_squares = self.get_occupied_tiles(unoccupied_squares,blocking_sprites)
        # this is a dict
        unoccupied_squares.remove(occupied_squares)
        return unoccupied_squares

    def generate_squares(self):
        sprites = []
        block_size = config.grid_square_size
        for x in range(config.num_grid_columns):
            for y in range(config.num_grid_rows):
                # TODO: uncertain why need to subtract 5 here, but otherwise, need to add 5 to snap
                # and alignment of grid squares is off
                # could possibly be caused by border thickness
                x_pos = x * block_size - 5
                y_pos = y * block_size - 5
                color = colors.GRAY
                square = GridSquare(x_pos, y_pos, color)
                sprites.append(square)
        return sprites
