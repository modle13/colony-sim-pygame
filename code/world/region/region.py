import logging
from random import randrange

import pygame

from settings import config
from structures.product import Product
from util.events.event_queue import EventQueue
from util.support import blit
from util.y_sort_camera_group import YSortCameraGroup
from world.region.grid import Grid
from world.resource import Resource


logger = logging.getLogger(__name__)


class Region:
    def __init__(self,id):
        self.id = id
        self.name = "some region"
        self.pos = pygame.math.Vector2(0,0)
        logger.info(f"Region {self.id}:'{self.name}' initialized")

        self.can_tick_shroom = False
        self.tick_delay_shroom_min = 1500
        self.tick_delay_shroom_max = 3500
        self.tick_delay_shroom = randrange(self.tick_delay_shroom_min, self.tick_delay_shroom_max)
        self.last_tick_shroom = 0

        self.can_tick_trees = False
        self.tick_delay_trees_min = 1500
        self.tick_delay_trees_max = 3500
        self.tick_delay_trees = randrange(self.tick_delay_trees_min, self.tick_delay_trees_max)
        self.last_tick_trees = 0

        self.can_tick_wheat = False
        self.tick_delay_wheat_min = 1500
        self.tick_delay_wheat_max = 3500
        self.tick_delay_wheat = randrange(self.tick_delay_wheat_min, self.tick_delay_wheat_max)
        self.last_tick_wheat = 0

        self.update_targets_delay = 400
        self.update_targets_time = 0
        self.update_targets_cooldown = True

        self.active = True
        self.assigned = False
        self.assigned_colony = None

        self.grid = Grid()

        # SPRITES
        self.all_sprites = pygame.sprite.Group()
        self.game_objects = pygame.sprite.Group()

        # resources and products
        self.products = pygame.sprite.Group()
        self.creatures = pygame.sprite.Group()
        self.shrooms = pygame.sprite.Group()
        self.trees = pygame.sprite.Group()
        self.wheat = pygame.sprite.Group()

        # role targets
        self.forager_targets = pygame.sprite.Group()
        self.farmer_targets = pygame.sprite.Group()
        self.hauler_targets = pygame.sprite.Group()
        self.woodcutter_targets = pygame.sprite.Group()

        # structures
        self.structures = pygame.sprite.Group()
        self.storage_structures = pygame.sprite.Group()

        # other sprites
        self.blocking_sprites = pygame.sprite.Group()

        self.targets, self.resources = self.set_sprite_group_collections()

        self.active_targets = self.targets.copy()

        self.subscribe_to_events()

    def update(self):
        # logger.info(f"update called in region {self.name}")
        self.cooldowns()
        self.check_shrooms()
        self.check_trees()
        self.check_wheat()
        self.grid.update()

        self.all_sprites.update()

        if self.update_targets_cooldown:
            return

        self.update_targets_time = pygame.time.get_ticks()
        self.update_targets_cooldown = True

        self.update_active_targets()

    def draw(self):
        # logger.info("drawing region")
        # self.all_sprites.draw(config.game.game_surf)
        for ea in self.all_sprites:
            # logger.info(f"drawing {ea}")
            blit(ea)

    def update_active_targets(self):
        all_capacity = {}
        for structure in self.storage_structures:
            for item in config.storage_items:
                capacity = structure.output_inventory.get_capacity(item)
                current = all_capacity.get(item) or 0
                all_capacity[item] = current + capacity

        # filter for available targets
        for role_type in self.targets:
            self.active_targets[role_type] = list(
                filter(lambda x:
                    x.active
                    and not x.claimed,
                    self.targets.get(role_type)
                )
            )
            # filter again for available storage for haulers
            if role_type == 'hauler':
                self.active_targets[role_type] = list(
                    filter(lambda x:
                        (all_capacity.get(x.label) or 0) > 0,
                        self.active_targets.get(role_type)
                    )
                )

    def check_shrooms(self):
        # this doesn't even need an event if Region holds the resource info

        if not self.can_tick_shroom:
            return

        self.attempt_resource_add('ADDSHROOM',True,None)

        self.last_tick_shroom = pygame.time.get_ticks()
        self.can_tick_shroom = False

    def check_trees(self):
        # this doesn't even need an event if Region holds the resource info

        if not self.can_tick_trees:
            return

        self.attempt_resource_add('ADDTREE',True,None)

        self.last_tick_trees = pygame.time.get_ticks()
        self.can_tick_trees = False

    def check_wheat(self):
        if not self.can_tick_wheat:
            return

        self.attempt_resource_add('ADDWHEAT',True,None)

        self.last_tick_wheat = pygame.time.get_ticks()
        self.can_tick_wheat = False

    def attempt_resource_add(self,resource,automatic,pos):
        # logger.info(f"attempt_resource_add called with {resource},{automatic},{pos}")
        # TODO: much of this strcture exists due to how pygame events were used previously
        #   with resource logic contained within events.py
        #   a refactor is likely in order
        event_config = config.resource_events.get(resource)
        name = event_config.get('name')
        resource_group = self.resources.get(name)

        if resource_group == None:
            logger.error(f"no resource group found for event '{resource}', item name: '{name}' in {self.resources}; found {resource_group}")
            return

        max_amount = event_config.get('max',0)
        if not automatic or len(resource_group) < max_amount:
            self.add_new_resource(name, resource_group, event_config, position=(pos or (0,0)))
        else:
            # logger.info(f'maximum {name} have been added: {max_amount}')
            pass

    def add_new_resource(self, item, group, props, position=(0,0)):
        # logger.info(f"add_new_resource called with: type={item}, group={group}, props={props}, position={position}")

        if position == (0,0):
            # logger.info(f"no position set for {item}, getting one")
            random_sprite = self.grid.get_random_grid_square()
            if not random_sprite:
                # no space available
                return
            position = random_sprite.rect.center
            # logger.info(f"target position for {item} is {position}")
        if not position:
            return

        new_resource = Resource(self, config.camera.apply_pos_vector(position), item)
        # TODO: change this to region ID
        group.add(new_resource)
        self.blocking_sprites.add(new_resource)
        role = props.get('role')
        # TODO: maybe? pass groups into sprite init rather than add directly? (does it really matter?)
        self.targets.get(role).add(new_resource)
        self.all_sprites.add(new_resource)
        self.game_objects.add(new_resource)

    def attempt_product_add(self,name,amt,pos):
        sprites_to_add = []
        logger.info(f"product add {name}:{amt} at {pos}")
        for i in range(amt):
            x = randrange(int(pos[0]) - 10, int(pos[0]) + 10)
            y = randrange(int(pos[1]) - 10, int(pos[1]) + 10)
            sprite = Product((x, y), name)
            sprites_to_add.append(sprite)
        self.products.add(sprites_to_add)
        self.targets.get('hauler').add(sprites_to_add)

        self.all_sprites.add(sprites_to_add)
        self.game_objects.add(sprites_to_add)

    def assign_region(self,colony_name):
        self.assigned_colony = colony_name
        self.assigned = True

    def get_unpopulated_tiles(self):
        return self.grid.get_unpopulated_tiles(self.blocking_sprites)

    def add_structure(self,new_structure):
        if not new_structure.is_cursor():
            if new_structure.structure_type in config.storage_structures:
                self.storage_structures.add(new_structure)
            self.blocking_sprites.add(new_structure)
            self.storage_structures.add(new_structure)
            self.structures.add(new_structure)
            self.game_objects.add(new_structure)

        self.all_sprites.add(new_structure)

    def set_sprite_group_collections(self):
        targets = {
            'woodcutter': self.woodcutter_targets,
            'thresher':   self.farmer_targets,
            'forager':    self.forager_targets,
            'hauler':     self.hauler_targets,
        }
        resources = {
            'creature':   self.creatures,
            'shroom':     self.shrooms,
            'tree':       self.trees,
            'wheat':      self.wheat,
        }
        # add dummy groups to avoid NoneType on target filters
        for structure_type in config.definitions.structures:
            if structure_type not in targets:
                targets[structure_type] = pygame.sprite.Group()
        return targets, resources

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.can_tick_shroom:
            if current_time - self.last_tick_shroom >= self.tick_delay_shroom:
                # self.can_tick_shroom = True
                self.tick_delay_shroom = randrange(self.tick_delay_shroom_min, self.tick_delay_shroom_max)
        if not self.can_tick_trees:
            if current_time - self.last_tick_trees >= self.tick_delay_trees:
                # self.can_tick_trees = True
                self.tick_delay_trees = randrange(self.tick_delay_trees_min, self.tick_delay_trees_max)
        if not self.can_tick_wheat:
            if current_time - self.last_tick_wheat >= self.tick_delay_wheat:
                # self.can_tick_wheat = True
                self.tick_delay_wheat = randrange(self.tick_delay_wheat_min, self.tick_delay_wheat_max)
        if self.update_targets_cooldown:
            if current_time - self.update_targets_time >= self.update_targets_delay:
                self.update_targets_cooldown = False

    def subscribe_to_events(self):
        pass

    def __repr__(self):
        return f'{self.id}:{self.name}'
