import logging

import pygame

from settings import config
from util.support import blit


logger = logging.getLogger(__name__)


class BaseSprite(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.default_size = (config.unit//5,config.unit//5)
        self.show = True
        self.label = "base sprite"
        # logger.info(f"sprite default size is {self.default_size}")

    def draw(self,camera=None):
        if hasattr(self, 'debug_repr_sprite') and self.context_pane:
            blit(self.debug_repr_sprite)
        if hasattr(self, 'context_pane') and self.context_pane:
            blit(self.context_pane, follow_camera=False)
        # Show the status messages on colonists
        if hasattr(self, 'status_text_object') and self.status_text_object:
            blit(self.status_text_object)

        # logger.info(f"Blitting self {self}")
        blit(self)
