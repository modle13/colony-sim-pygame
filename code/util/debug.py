import traceback

import pygame

from settings import config


pygame.init()

font = pygame.font.Font(None,25)


def debug(info,y=None,x=None,align='topleft',char_center=True):
    x,y = get_coords(x,y,info,align,char_center=char_center)

    display_surface=pygame.display.get_surface()
    debug_surf=font.render(str(info),True,'White')
    debug_rect=debug_surf.get_rect(topleft = (x,y))
    try:
        pygame.draw.rect(display_surface,'Black',debug_rect)
        display_surface.blit(debug_surf,debug_rect)
    except TypeError:
        pass


def log_stack():
    print()
    for each in traceback.format_stack():
        print(each.strip())
    print()


def get_coords(x,y,info,align,char_center=True):
    if align.endswith('left'):
        x = x if x else 10
    elif align.endswith('right'):
        x = config.SCREEN_WIDTH - font.size(info)[0] - (x if x else 0)
    elif align.endswith('mid'):
        if char_center:
            x = config.SCREEN_WIDTH // 2 - font.size(info)[0] // 2 - (x if x else 0)
        else:
            x = config.SCREEN_WIDTH // 2 - (x if x else 0)
    else:
        x = 10

    if align.startswith('top'):
        y = y if y else 10
    elif align.startswith('mid'):
        y = config.SCREEN_HEIGHT // 2 - font.size(info)[1] // 2 - (y if y else 0)
    elif align.startswith('bottom'):
        y = config.SCREEN_HEIGHT - font.size(info)[1] - (y if y else 0)

    if align == 'center':
        x = (config.SCREEN_WIDTH // 2) - (font.size(info)[0] // 2) + (x if x else 0)
        y = (config.SCREEN_HEIGHT // 2) + (y if y else 0)

    x = x or 10
    y = y or 10

    return x,y


def debug_write(content):
    with open('debug_write.txt', 'w') as output_file:
        output_file.write(content)
