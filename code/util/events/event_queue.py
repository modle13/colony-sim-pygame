import argparse
from collections import defaultdict
import functools
import logging

from util.singleton import Singleton


logger = logging.getLogger(__name__)


"""
usage

# publish an event
EventQueue.instance().publish("SOME_EVENT",[event_param])

# listen for an event
EventQueue.instance().subscribe('SOME_EVENT',self.some_func)
"""


# pylint: disable=too-few-public-methods
class Event:
    """Event class"""

    def __init__(self, name, args, kwargs):
        self.name = name
        self.args = args if args is not None else []
        self.kwargs = kwargs if kwargs is not None else {}


@Singleton
class EventQueue:
    """Event queue class that contains events. Events can be subscribed to call a callback"""
    def __init__(self):
        self.events = []
        self.subscribers = defaultdict(list)

    def subscribe(self, event_name: str, function: callable, obj=None):
        """Subscribes the passed function to the event. Whenever the named event
        occurs, the passed function will be executed.
        """
        logger.info(f'subscribed to {event_name}')
        self.subscribers[event_name].append({'function':function,'obj':obj})

    def publish(self, event_name: str, args=None, kwargs=None):
        # call this for non-argparse events
        # the args are identical to pump, but publish is clearer
        logger.debug(f'published to {event_name} with args {args}')
        self.pump(event_name, args=args)

    def pump(self, event_name: str, args=None, kwargs=None):
        """Adds an event to the end of the events list"""
        logger.debug(f'added event to events list {event_name} with args {args} and kwargs {kwargs}')
        event = Event(event_name, args, kwargs)
        self.events.append(event)

    def update(self):
        """Calls all subscriber functions for all pumped events, then removes all pumped events."""
        # logger.debug("updating EventQueue")
        for event in self.events:
            logger.debug(f'processing event {event.name}')
            #for function in self.subscribers[event.name]:
            #    function(*{event.args, **event.kwargs)
            for ea in self.subscribers[event.name]:
                if ea.get('obj'):
                    # handles 'self' object function execution
                    #ea.get('function')(ea.get('obj'),*event.args,**event.kwargs)
                    ea.get('function')(None,*event.args,**event.kwargs)
                else:
                    ea.get('function')(*event.args,**event.kwargs)
        self.events = []


class ParseEvent(argparse.Action):
    """Parse Event class, to be used as an action for the argparse.ArgumentParser"""
    def __init__(self, event_name, *args, **kwargs):
        self.event_queue = EventQueue.instance()
        self.event_name = event_name
        super().__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        self.event_queue.pump(self.event_name, args=[values])


def get_action(event_name):
    """Utility function that returns a pre-filled function (not-called yet)
    that returns a ParseEvent.
    """
    logger.info(f'returning argparse event {event_name}')
    return functools.partial(ParseEvent, event_name)
