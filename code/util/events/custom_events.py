import pygame

# Create a custom event for adding a new objects
# this is just an int, essentially a custom id
#self.CUSTOM_EVENT = pygame.USEREVENT + 1
# pygame.time.set_timer will cause the event to repeat at the given interval
#pygame.time.set_timer(self.CUSTOM_EVENT, 500)

UPDATE_PLAYER_MAP = pygame.USEREVENT+1
PLAYER_SEND_DATA = pygame.USEREVENT+2
QUIT_GAME = pygame.USEREVENT+3
# SET_SCREEN_STATE = pygame.USEREVENT+4
KILL_LEVEL = pygame.USEREVENT+5
DISCONNECT_PLAYER = pygame.USEREVENT+6
PAUSE_GAME = pygame.USEREVENT+7
UNPAUSE_GAME = pygame.USEREVENT+8
LOAD_LEVEL = pygame.USEREVENT+9

custom_event_map = {
    QUIT_GAME:'QUIT_GAME',
    # SET_SCREEN_STATE:'SET_SCREEN_STATE',
    PAUSE_GAME:'PAUSE_GAME',
    UNPAUSE_GAME:'UNPAUSE_GAME',
}

def map_event(event_id):
    return custom_event_map.get(event_id)

def post(event_type,data={}):
    event = None
    if event_type == 'quit_game':
        #event = pygame.event.Event(QUIT_GAME)
        quit_game()
    # elif event_type == 'set_state':
    #     event = pygame.event.Event(SET_SCREEN_STATE,data)
    elif event_type == 'pause_game':
        event = pygame.event.Event(PAUSE_GAME)
    elif event_type == 'unpause_game':
        event = pygame.event.Event(UNPAUSE_GAME)

    if event:
        # post event to event queue
        pygame.event.post(event)

def quit_game():
    #logger.info('quitting game')
    event = pygame.event.Event(QUIT_GAME)
    pygame.event.post(event)
