import logging
import sys

import pygame

from settings import *
# import util.events.custom_events as custom_events
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class EventManager():
    def __init__(self):
        self.buttons_down = {}
        self.keys_down = {}
        self.keys_up = {}
        self.joyhats_down = {}
        self.tracked_down_keys = [
                pygame.K_LCTRL,
                pygame.K_LSHIFT,
                pygame.K_TAB,
                pygame.K_ESCAPE,
                pygame.K_SPACE,
                pygame.K_RETURN,
                pygame.K_UP,
                pygame.K_DOWN,
                pygame.K_LEFT,
                pygame.K_RIGHT,
                pygame.K_BACKSPACE,
                pygame.K_DELETE,
                pygame.K_F1,
                pygame.K_F2,
                pygame.K_F3,
                pygame.K_F4,
                pygame.K_F5,
                pygame.K_F6,
                pygame.K_F7,
                pygame.K_F8,
                pygame.K_w,
                pygame.K_s,
                pygame.K_a,
                pygame.K_b,
                pygame.K_c,
                pygame.K_d,
                pygame.K_q,
                pygame.K_e,
                pygame.K_i,
                pygame.K_l,
                pygame.K_m,
                pygame.K_o,
                pygame.K_r,
                pygame.K_u,
                pygame.K_1,
                pygame.K_2,
                pygame.K_3,
                pygame.K_4,
                ]

        self.tracked_down_buttons = [0,1,2,3,4,5,6,7,8,9,10]

        self.tracked_down_hats = {'up':(1,-1),'down':(1,1),'left':(0,-1),'right':(0,1)}
        # self.input_exclude_events = [pygame.TEXTINPUT] + list(custom_events.custom_event_map.keys())
        self.input_exclude_events = [pygame.TEXTINPUT]
        self.mouse_key_map = {
            1:'LMB',
            2:'MMB',
            3:'RMB',
            4:'MB4',
            5:'MB5'
        }

    def update(self):
        self.buttons_down = {}
        self.joyhats_down = {}
        self.keys_down = {}
        self.keys_up = {}
        # Look at every event in the queue
        #logger.info('exclude events are',self.input_exclude_events)
        # FIXME: This will interfere with custom events in util.events.events such as ADDTREE
        #   pygame.event.get flushes the queue; those events should move to pub/sub events with event_queue 
        for event in pygame.event.get(exclude=self.input_exclude_events):
            if event.type == pygame.QUIT:
                pygame.quit()
                # why do we need sys.exit()? pygame.quit should handle this
                sys.exit()
            elif event.type == pygame.JOYBUTTONDOWN:
                #logger.info("joy button down")
                self.get_joybuttons_down(event.button)
                if self.buttons_down:
                    pass
                    #logger.info('buttons: ',self.buttons_down)
            elif event.type == pygame.JOYHATMOTION:
                self.get_joyhats_down(event.value)
                if self.joyhats_down:
                    pass
                    #logger.info('hats: ',self.joyhats_down)
            elif event.type == pygame.KEYDOWN:
                #logger.info(pygame.key.name(event.key))
                #logger.info(event.unicode)
                self.get_keys_down(event.key)
                if self.keys_down:
                    pass
                    #logger.info('keys: ',self.keys_down)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # logger.info(f'MOUSEBUTTONDOWN {event.button}')
                self.get_mouse_button_down(event.button)
            elif event.type == pygame.MOUSEBUTTONUP:
                # logger.info(f"MOUSEBUTTONUP {event.button}")
                self.get_mouse_button_up(event.button)

    def get_joybuttons_down(self,button):
        for entry in self.tracked_down_buttons:
            self.buttons_down[entry] = self.buttons_down.get(entry,button==entry)

    def get_joyhats_down(self,hat):
        self.joyhats_down['left'] = hat[0] == -1
        self.joyhats_down['right'] = hat[0] == 1
        self.joyhats_down['up'] = hat[1] == 1
        self.joyhats_down['down'] = hat[1] == -1

    def get_keys_down(self,key):
        for entry in self.tracked_down_keys:
            self.keys_down[entry] = self.keys_down.get(entry,key==entry)

    def get_mouse_button_down(self,button):
        matched = self.mouse_key_map.get(button,None)
        if matched:
            self.keys_down[matched] = True

    def get_mouse_button_up(self,button):
        matched = self.mouse_key_map.get(button,None)
        if matched:
            self.keys_up[matched] = True
