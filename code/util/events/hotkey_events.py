import logging
import sys

import pygame

from settings import *

from state.game_state_enum import GAME_STATE

# import util.events.custom_events as custom_events
from util.events.event_queue import EventQueue
from util.input_manager import InputManager
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class HotkeyEvents():
    def __init__(self):
        self.subscribe_to_events()
        self.right_mouse_drag = False

    def update(self):
        inputs = InputManager.instance()

        mmb_active = self.check_mmb(inputs)
        if mmb_active:
            return

        if inputs.get_rmb_down():
            EventQueue.instance().publish("RIGHT_CLICK",[pygame.mouse.get_pos()])
        elif inputs.get_debug_toggle():
            config.debug = not config.debug
            logging.getLogger().setLevel(logging.DEBUG if config.debug else logging.INFO)
            logger.info(f'\n\nlogging debug {"ENABLED" if config.debug else "DISABLED"}\n')
        elif inputs.get_grid_toggle():
            # toggle grid?
            logger.info(f'\n\ntoggling grid\n')
            config.game.show_grid = not config.game.show_grid
        elif inputs.get_structures_toggle():
            # toggle structure list?
            logger.info(f'\n\ntoggling structures\n')
            EventQueue.instance().publish('SHOW_STRUCTURE_SELECTION')
        elif inputs.get_inventory_toggle():
            # toggle inventory counter?
            logger.info(f'\n\ntoggling inventory\n')
            EventQueue.instance().publish('SHOW_STORES_SELECTION')
        elif inputs.get_colonists_toggle():
            # toggle colonist counter?
            logger.info(f'\n\ntoggling colonists\n')
            EventQueue.instance().publish('SHOW_COLONIST_SELECTION')
        elif inputs.get_message_feed_toggle():
            # toggle message feed?
            # TODO: don't really need this; just always visible? maybe expand view?
            logger.info(f'\n\ntoggling message feeds\n')
            config.game.message_feed.toggle()
        elif inputs.get_spawn_tree():
            logger.info(f'\n\ndebug spawning tree\n')
            EventQueue.instance().publish("ADD_RESOURCE",["ADDTREE",False,pygame.mouse.get_pos()])
        elif inputs.get_spawn_shroom():
            logger.info(f'\n\ndebug spawning shroom\n')
            EventQueue.instance().publish("ADD_RESOURCE",["ADDSHROOM",False,pygame.mouse.get_pos()])
        elif inputs.get_spawn_wheat():
            logger.info(f'\n\ndebug spawning wheat\n')
            EventQueue.instance().publish("ADD_RESOURCE",["ADDWHEAT",False,pygame.mouse.get_pos()])

        if inputs.get_mb_wheel_up():
            # logger.info("mouse wheel up pressed")
            # zoom in (smaller zoom multiplier)
            EventQueue.instance().publish("ZOOM_CAMERA",[False])
        elif inputs.get_mb_wheel_down():
            # logger.info("mouse wheel down pressed")
            # zoom out (larger zoom multiplier)
            EventQueue.instance().publish("ZOOM_CAMERA",[True])

    def check_mmb(self,inputs):
        if self.right_mouse_drag:
            if inputs.get_mmb_up():
                print("released mmb")
                # release input lock from this application
                pygame.event.set_grab(False)
                config.game.reticle.clear_mouse_offset()
                self.right_mouse_drag = False
                return True
        else:
            if inputs.get_mmb_down():
                print("pressed mmb")
                # lock input to this application to prevent mouse from going outside screen border
                pygame.event.set_grab(True)
                config.game.reticle.set_mouse_offset(pygame.mouse.get_pos())
                self.right_mouse_drag = True
                self.down_mouse_pos = pygame.mouse.get_pos()
                return True
        return False

    def subscribe_to_events(self):
        pass
        # EventQueue.instance().subscribe("SOME_EVENT",self.some_func)
