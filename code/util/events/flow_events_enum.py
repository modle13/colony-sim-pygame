from enum import Enum


class FLOW_EVENTS(Enum):
    # init screen
    SHOW_INIT=0
    START_GAMEPLAY=1
    SHOW_LOAD_SAVE=2
    SHOW_SETTINGS=3
    SHOW_EXIT=4

    # settings screen
    SHOW_GAMEPLAY_SETTINGS=5
    SHOW_CONTROLS_SETTINGS=6
    SHOW_VIDEO_SETTINGS=7
    SHOW_AUDIO_SETTINGS=8

    # pause screen
    SAVE_GAME = 9
    SHOW_IN_GAME_SETTINGS = 10
    SHOW_QUIT = 11

    # general
    EXIT_GAME = 12
    SHOW_PAUSE = 13
