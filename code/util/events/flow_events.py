import logging
import sys

import pygame

from settings import *

from state.game_state_enum import GAME_STATE

# import util.events.custom_events as custom_events
from util.events.event_queue import EventQueue
from util.events.flow_events_enum import FLOW_EVENTS
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FlowEvents():
    def __init__(self):
        self.subscribe_to_events()

    def initialize(self):
        logger.info("INITIALIZING FlowEvents")
        self.subscribe_to_events()

    def change_modes(self,target_mode):
        logger.info(f"CHANGE MODES {target_mode}")
        match target_mode:
            case FLOW_EVENTS.SHOW_INIT:
                self.trigger_screen_flow(GAME_STATE.MENUS, "init_screen")

            ## init
            case FLOW_EVENTS.START_GAMEPLAY:
                self.trigger_screen_flow(GAME_STATE.GAMEPLAY, "gameplay_screen")
            case FLOW_EVENTS.SHOW_LOAD_SAVE:
                self.trigger_screen_flow(GAME_STATE.MENUS, "load_screen")
            case FLOW_EVENTS.SHOW_SETTINGS:
                self.trigger_screen_flow(GAME_STATE.MENUS, "settings_screen")
            case FLOW_EVENTS.SHOW_EXIT:
                self.trigger_screen_flow(GAME_STATE.MENUS, "exit_screen")

            ## settings
            case FLOW_EVENTS.SHOW_GAMEPLAY_SETTINGS:
                self.trigger_screen_flow(GAME_STATE.MENUS, "settings_screen_gameplay")
            case FLOW_EVENTS.SHOW_CONTROLS_SETTINGS:
                self.trigger_screen_flow(GAME_STATE.MENUS, "settings_screen_controls")
            case FLOW_EVENTS.SHOW_VIDEO_SETTINGS:
                self.trigger_screen_flow(GAME_STATE.MENUS, "settings_screen_video")
            case FLOW_EVENTS.SHOW_AUDIO_SETTINGS:
                self.trigger_screen_flow(GAME_STATE.MENUS, "settings_screen_audio")

            ## pause
            case FLOW_EVENTS.SAVE_GAME:
                EventQueue.instance().publish('SAVE_GAME')
            case FLOW_EVENTS.SHOW_IN_GAME_SETTINGS:
                self.trigger_screen_flow(GAME_STATE.MENUS, "settings_screen_in_game")
            case FLOW_EVENTS.SHOW_QUIT:
                self.trigger_screen_flow(GAME_STATE.MENUS, "quit_screen")
            # case FLOW_EVENTS.ASDF:
            #     pass

            case FLOW_EVENTS.EXIT_GAME:
                EventQueue.instance().publish('EXIT_GAME_TO_DESKTOP')
            case FLOW_EVENTS.SHOW_PAUSE:
                self.trigger_screen_flow(GAME_STATE.MENUS, "pause_screen")

    def trigger_screen_flow(self,target_state,target_screen):
        EventQueue.instance().publish('SET_GAME_STATE',[target_state])
        EventQueue.instance().publish('OPEN_SCREEN',[target_screen])

    def subscribe_to_events(self):
        logger.info("SUBSCRIBING TO FLOW EVENTS")
        EventQueue.instance().subscribe("FLOW_EVENT",self.change_modes)
