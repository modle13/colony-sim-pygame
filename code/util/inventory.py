import logging

from util.debug import log_stack


logger = logging.getLogger(__name__)


class Inventory(object):
    def __init__(self, limit_type='individual', purpose='output', limit=10, kinds=[]):
        self.purpose = purpose
        self.limit_type = limit_type
        self.limit = limit
        self.initialize_contents(kinds)
        self.claimed = {}
        self.show = True
        # logger.info(f"Inventory initialized; purpose: {purpose}; limit_type: {limit_type}; limit: {limit}; kinds: {kinds}")

    def initialize_contents(self,kinds):
        self.kinds = kinds
        self.contents = {kind: 0 for kind in kinds}

    def add(self, kind, amount, allow_new_key=False) -> int:
        remaining_capacity = 0

        if allow_new_key:
            remaining_capacity = self.has_room()
        else:
            remaining_capacity = self.has_room(kind)

        to_add = remaining_capacity if remaining_capacity < amount else amount
        # logger.info(f"NEED TO ADD THIS MANY {to_add}")
        try:
            self.contents[kind] += to_add
            # logger.info(f'increased count of {kind} in inventory by {to_add}')
            # log_stack()
        except KeyError:
            self.contents[kind] = to_add
            # logger.info(f'added {to_add} of item {kind} to inventory')
            # log_stack()

        self.remove_claim(kind,to_add)

        return to_add

    def add_all(self, data, allow_new_key=False):
        remaining = {}
        for k,v in data.items():
            amount_not_added = self.add(k,v,allow_new_key)
            if amount_not_added > 0:
                remaining[k] = amount_not_added
        return remaining

    def remove(self, kind, amount, source_claim=None) -> int:
        # logger.info(f"REMOVING {kind}:{amount} with source {source_claim}")
        if not kind:
            return 0
        current = self.contents.get(kind,0)
        to_remove = 0
        if current:
            to_remove = current if current < amount else amount
            new_amount = current - to_remove
            self.set(kind, new_amount)

        if source_claim:
            # logger.info(f"REMOVING CLAIM on {source_claim}")
            for key,value in source_claim.items():
                self.remove_claim(key,value)
        # logger.info(f'inventory is now {self.contents}; claim is {self.claimed}')

        return to_remove

    def remove_first(self, amount):
        to_remove = {}
        remaining = amount
        for key,value in self.contents.items():
            if value >= remaining:
                to_remove[key] = remaining
                remaining = 0
                break
            else:
                if value > 0:
                    to_remove[key] = value
                    remaining -= value

        if remaining == 0:
            for key,value in to_remove.items():
                self.remove(key, value)
            return True
        else:
            logger.info("not enough to remove")
            return False

    def remove_any(self, amount):
        to_remove = {}
        remaining = amount
        for key,value in self.contents.items():
            if value >= remaining:
                to_remove[key] = remaining
                remaining = 0
                break
            else:
                if value > 0:
                    to_remove[key] = value
                    remaining -= value

        if remaining == 0:
            for key,value in to_remove.items():
                self.remove(key, value)
        else:
            logger.info("not enough to remove")

        return to_remove

    def set(self, kind, amount):
        self.contents[kind] = amount

    def is_full(self, kind=None) -> int:
        return self.has_room(kind=kind) == 0

    def is_empty(self, kind=None) -> int:
        return self.has_room(kind=kind) == self.limit

    def get_capacity(self, kind=None) -> int:
        return self.has_room(kind=kind)

    def has_room(self, kind=None) -> int:
        # will return `0` if kind is not `None` but does not exist in the `contents` keys
        if self.limit_type == 'unlimited':
            return float('inf')
        if kind and kind not in self.contents:
            return 0
        count = self.get_available_count(kind=kind)
        remaining_capacity = 0 if self.limit_type == 'individual' else self.limit
        if count or self.limit_type == 'individual':
            remaining_capacity = self.limit - count
        return remaining_capacity

    def get_available_count(self, kind=None) -> int:
        count = 0
        if self.limit_type == 'total' or not kind:
            count = sum(self.contents.values())
        else:
            # log_stack()
            # logger.info(f"get_available_count claimed is {self.claimed}")
            if self.purpose == 'input':
                count = self.contents.get(kind,0) + self.claimed.get(kind,0)
            else:
                count = self.contents.get(kind,0) - self.claimed.get(kind,0)
        return count

    def get_needed_count(self, kind=None) -> int:
        # logger.info(f"get_available_count claimed is {self.claimed}")
        count = self.contents.get(kind,0) + self.claimed.get(kind,0)
        return count

    def claim_any_inventory(self,amount):
        to_claim_name = ''
        to_claim_amt = 0
        for key,value in self.contents.items():
            if value >= amount:
                # claim all needed amount
                to_claim_name = key
                to_claim_amt = amount
                break
            else:
                # only claim what's there
                if value > 0:
                    to_claim_name = key
                    to_claim_amt = value

            if value > 0:
                # found at least one, only want one kind, so we're done
                break

        self.claim_inventory(to_claim_name,to_claim_amt)
        # could also check to be sure claim_inventory claimed the right amount, but
            # shouldn't really be necessary since nothing else will call claim between
            # execution and return
        return to_claim_name,to_claim_amt

    def claim_inventory(self,kind,amt):
        avail = self.get_available_count(kind)
        to_claim = min(avail,amt)
        try:
            self.claimed[kind] += to_claim
            # logger.info(f'increased count of {kind} in inventory by {to_add}')
            # log_stack()
        except KeyError:
            self.claimed[kind] = to_claim
            # logger.info(f'added {to_add} of item {kind} to inventory')
            # log_stack()
        return to_claim

    def set_claimed(self,kind,amt):
        # logger.info(f"setting input claim to {kind}:{amt}")
        # used to reserve target inventory capacity to put items into
        # self.claimed = {kind: amt}
        current = self.claimed.get(kind,0)
        self.claimed[kind] = current+amt

    def remove_claim(self,kind,amt):
        found = self.claimed.get(kind,0)
        if found:
            self.claimed[kind] = max(found - amt, 0) # keep it 0 or positive

    def contains(self, kind) -> bool:
        return bool(self.contents.get(kind))

    def get_kinds(self) -> list:
        return list(self.contents.keys())

    def has_amount_of_kind(self, kind, amount) -> bool:
        # logger.info(f'looking for at least {amount} of {kind} in {self.contents}')
        found = self.contents.get(kind)
        return found != None and bool(found >= amount)

    def has_total_of_at_least(self, amount) -> bool:
        found = 0
        for key,value in self.contents.items():
            found += value
            if found >= amount:
                return True
        return False

    def has_any_of(self, coll) -> bool:
        # logger.info(f"checking {self.contents} for any of {coll}")
        for k,v in coll.items():
            if self.contents.get(k, 0) > 0:
                return True

        return False

    def reset(self):
        self.contents = {}

    def toggle(self):
        self.show = not self.show

    def get_contents_repr(self) -> list:
        all_keys = list(self.contents.keys())
        all_keys.extend(x for x in list(self.claimed.keys()) if x not in all_keys)
        contents_repr = []
        for ea in all_keys:
            amt = self.contents.get(ea,0)
            claimed_amt = self.claimed.get(ea,0)
            if amt or claimed_amt:
                contents_repr.append(f'{ea}: {amt} {self.get_claimed_repr(ea)}')
        return contents_repr

    def get_claimed_repr(self,item):
        item_claimed = self.claimed.get(item,0)
        out = ''
        if item_claimed:
            out = f'({item_claimed})'
        return out

    def __repr__(self):
        text = []
        for key, value in self.contents.items():
            item_text = f'{key} {value}'
            if self.limit_type != 'unlimited':
                text.append(f'{key} {value}/{self.limit}')
            key_claimed = self.claimed.get(key,0)
            if key_claimed > 0:
                item_text = f'{item_text} ({key_claimed})'
        return ', '.join(text)
