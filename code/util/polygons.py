import math

import pygame

from util import colors
from settings import config


def get_resource_polygon(name, size):
    polygon_points = config.resources.get(name).get('points')
    points = list(map(lambda x: (x[0] * size, x[1] * size), polygon_points))
    return points

def draw_shape(surf, obj):
    """Entry point."""
    side = config.growth_sizes[obj.size]
    if obj.resource_type in ['tree', 'wheat', 'creature']:
        # polygon
        draw_polygon(surf, obj.resource_type, side)
    elif obj.resource_type == 'shroom':
        # circle
        position = (side / 2, side / 2)
        radius = side / 2
        pygame.draw.circle(surf, colors.LIGHT_BROWN, position, radius)
    elif obj.resource_type == 'disabled':
        position = (obj.unit / 2, obj.unit / 2)
        radius_outer = obj.unit / 2
        radius_inner = 3 * obj.unit / 8
        pygame.draw.circle(surf, colors.RED, position, radius_outer)
        pygame.draw.circle(surf, colors.BLACK, position, radius_inner)
        line_start = get_pos_on_circle(position, radius_inner, 315)
        line_end = get_pos_on_circle(position, radius_inner, 135)
        pygame.draw.line(surf, colors.RED, line_start, line_end, width=5)
    else:
        # square
        surf = pygame.Surface((side, side))
        surf.fill(colors.GRAY)


def get_pos_on_circle(position, radius, angle):
    angle_rad = math.radians(angle)
    pos_x = position[0] + radius * math.sin(angle_rad)
    pos_y = position[1] - radius * math.cos(angle_rad)
    return (pos_x, pos_y)


def draw_polygon(surf, kind, side):
    polygon = get_resource_polygon(kind, side)
    pygame.draw.polygon(surf, config.resources.get(kind).get('color'), polygon)
