# http://stackoverflow.com/questions/14354171/add-scrolling-to-a-platformer-in-pygame/14357169#14357169

import logging

import pygame

from settings import config
from util.events.event_queue import EventQueue


logger = logging.getLogger(__name__)


class Camera(object):
    def __init__(self, width, height):
        self.default_width = width*10
        self.default_height = height*10
        self.state = pygame.Rect(0,0,width,height)
        self.position = pygame.Rect(0,0,width,height)
        self.zoom = 1.0
        self.zoom_max = 3.0
        self.zoom_min = 0.2
        self.subscribe_to_events()

    def update(self, target_rect):
        self.state, self.position = self.process_camera(self.state,target_rect)

    def process_camera(self, camera, target_rect):
        l, t, _, _ = target_rect # l = left,  t = top
        _, _, w, h = camera      # w = width, h = height

        x_offset = -l
        y_offset = -t
        return pygame.Rect(x_offset,y_offset,w,h), pygame.Rect(l,t,w,h)

    def apply(self, target):
        return target.rect.move(self.state.bottomright)

    def apply_pos(self, target):
        return target.rect.move(self.state.bottomright)

    def apply_pos_vector(self, target):
        return (target[0] - self.state.bottomright[0], target[1] - self.state.bottomright[1])

    def zoom_camera(self,inward):
        if inward:
            self.zoom += 0.05
            self.zoom = min(self.zoom,self.zoom_max)
        else:
            self.zoom -= 0.05
            self.zoom = max(self.zoom,self.zoom_min)
        # logger.info(f"camera zoomed to {self.zoom}")
        EventQueue.instance().publish("SCALE_WITH_ZOOM",[self.zoom])

    def subscribe_to_events(self):
        EventQueue.instance().subscribe('ZOOM_CAMERA',self.zoom_camera)
