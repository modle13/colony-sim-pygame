from csv import reader
import logging
import math
from os import walk

import pygame

from settings import config

from util.debug import log_stack


logger = logging.getLogger(__name__)


def import_csv_layout(path):
    terrain_map = []
    with open(path) as level_map:
        layout = reader(level_map,delimiter = ',')
        for row in layout:
            terrain_map.append(list(row))
        return terrain_map


def import_folder(path,scale=None):
    surface_list = []

    for _,__,img_files in walk(path):
        for image in sorted(img_files):
            full_path = path + '/' + image
            image_surf = pygame.image.load(full_path).convert_alpha()
            if scale and isinstance(scale,tuple):
                image_surf = pygame.transform.scale(image_surf,scale)
            # elif scale and isinstance(scale,float):
            #     logger.info(f'scaling image by {scale}')
            #     image_surf = scale_by_ratio(image_surf,scale=scale)
            else:
                image_surf = scale_by_ratio(image_surf)
            surface_list.append(image_surf)

    return surface_list

def scale_by_ratio(image,scale=config.unit):
    width,height=image.get_size()
    w_ratio = width/height
    h_ratio = height/width
    new_width=w_ratio*scale//3
    new_height=h_ratio*scale//3
    image = pygame.transform.scale(image,(new_width,new_height))
    return image

def wrap_text(text,font,width):
    # Code from SotK on reddit "Wrap Text in Pygame?" answer
    """Wrap text to fit inside a given width when rendered.
    :param text: The text to be wrapped.
    :param font: The font the text will be rendered in.
    :param width: The width to wrap to.
    """
    text_lines = text.replace('\t', '    ').split('\n')
    if width is None or width == 0:
        return text_lines

    wrapped_lines = []
    for line in text_lines:
        line = line.rstrip() + ' '
        if line == ' ':
            wrapped_lines.append(line)
            continue

        # Get the leftmost space ignoring leading whitespace
        start = len(line) - len(line.lstrip())
        start = line.index(' ', start)
        while start + 1 < len(line):
            # Get the next potential splitting point
            next = line.index(' ', start + 1)
            if font.size(line[:next])[0] <= width:
                start = next
            else:
                wrapped_lines.append(line[:start])
                line = line[start+1:]
                start = line.index(' ')
        line = line[:-1]
        if line:
            wrapped_lines.append(line)
    return wrapped_lines

def render_text_list(lines,font,color=(255, 255, 255)):
    # Code from SotK on reddit "Wrap Text in Pygame?" answer
    """Draw multiline text to a single surface with a transparent background.
    Draw multiple lines of text in the given font onto a single surface
    with no background color, and return the result.
    :param lines: The lines of text to render.
    :param font: The font to render in.
    :param color: The color to render the font in, default is white.
    """
    rendered = [font.render(line, True, color).convert_alpha()
                for line in lines]

    line_height = font.get_linesize()
    width = max(line.get_width() for line in rendered)
    tops = [int(round(i * line_height)) for i in range(len(rendered))]
    height = tops[-1] + font.get_height()

    surface = pygame.Surface((width, height)).convert_alpha()
    surface.fill((0, 0, 0, 0))
    for y, line in zip(tops, rendered):
        surface.blit(line, (0, y))

    return surface

def snap_to_grid(position):
    unit = config.grid_square_size
    snapped_position = (math.floor(position[0] // unit) * unit, math.floor(position[1] // unit) * unit)
    return snapped_position

def blit(sprite,follow_camera=True):
    # logger.info(f"blitting {sprite.label}")
    # log_stack()
    if not sprite.show:
        # logger.info(f"sprite not set to show {sprite}")
        return
    if sprite.rect.colliderect(config.game.visible_area):
        # draw can include other things, such as indicator symbols, debug strings
        #   so draw it along with default blit
        if hasattr(sprite, 'draw_extra'):
            sprite.draw_extra(config.game.game_surf)

        # could just move default blit into target sprite draw
        if follow_camera:
            config.game.game_surf.blit(sprite.surf, config.camera.apply(sprite))
        else:
            # logger.info(f"drawing {sprite} to game_surf")
            config.game.game_surf.blit(sprite.surf, sprite.rect)
    else:
        # logger.info(f"{sprite.rect} did not collide with visible game area {config.game.visible_area}")
        pass

    draw_context_panes(sprite)

def draw_context_panes(sprite):
    # Draw sprite context panes and reprs
    # TODO: move this stuff to appropriate sprite draw
    if hasattr(sprite, 'debug_repr_sprite') and sprite.debug_repr_sprite:
        # logger.info("drawing debug_repr_sprite")
        blit(sprite.debug_repr_sprite)
    if hasattr(sprite, 'context_pane') and sprite.context_pane:
        # logger.info("drawing context_pane")
        blit(sprite.context_pane, follow_camera=False)
    # Show the status messages on colonists
    if hasattr(sprite, 'status_text_object') and sprite.status_text_object:
        # logger.info("drawing status_text_object")
        blit(sprite.status_text_object)

def check_for_world_object_click(group, position):
    for entry in group:
        if entry.show and entry.rect.collidepoint(position):
            logger.info(f'player clicked on: {entry.label}')
            return entry
    return None
