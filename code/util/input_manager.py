import logging

import pygame

from util.events.event_manager import EventManager
from util.singleton import Singleton

logger = logging.getLogger(__name__)


@Singleton
class InputManager:
    """
    # gamepad buttons
    0: A
    1: B
    2: X
    3: Y
    4: L
    5: R
    6: select
    7: start
    8: home
    9: L3
    10: R3

    gamepad axes
    axis0: left horizontal
    axis1: left vertical
    axis2: L2
    axis3: right horizontal
    axis4: right vertical
    axis5: R2

    gamepad hats
    dpad is a tuple, (L/R,U/D) == (-1/1,-1/1)
    """
    def __init__(self):
        logger.info('input manager created')
        self.joystick = None
        self.keys = None
        self.get_joystick()

    def update(self):
        self.get_joystick()
        self.keys = pygame.key.get_pressed()
        self.buttons_down = EventManager.instance().buttons_down
        # NOTE each new key needs to be added to EventManager.tracked_down_keys to be picked up here

        # NOTE: `self.keys_down`, `self.buttons_down`, and `self.joyhats_down` means only the
        #       frame they were pressed; held keys should be queried from `self.keys`
        self.keys_down = EventManager.instance().keys_down
        self.keys_up = EventManager.instance().keys_up
        self.joyhats_down = EventManager.instance().joyhats_down

    def get_joystick(self):
        count = pygame.joystick.get_count()
        if self.joystick and count <= 0:
            self.joystick = None
        elif self.joystick:
            return
        elif count > 0:
            self.joystick = pygame.joystick.Joystick(0)

    def get_joyaxis(self, axis, direction):
        if not self.joystick:
            return False
        if direction > 0 and self.joystick.get_axis(axis) > 0.5:
            return True
        if direction < 0 and self.joystick.get_axis(axis) < -0.5:
            return True

    def get_joyhat(self, direction):
        if not self.joystick:
            return False

        if direction == 'up':
            return self.joystick.get_hat(0)[1] == 1
        elif direction == 'down':
            return self.joystick.get_hat(0)[1] == -1
        elif direction == 'left':
            return self.joystick.get_hat(0)[0] == -1
        elif direction == 'right':
            return self.joystick.get_hat(0)[0] == 1

    def get_joybutton(self, button):
        return self.joystick and self.joystick.get_button(button)

    def get_switch_off_weapon(self):
        return self.keys_down.get(pygame.K_1) or self.joyhats_down.get('left')

    def get_switch_magic(self):
        return self.keys_down.get(pygame.K_2) or self.joyhats_down.get('up')

    def get_switch_item(self):
        return self.keys_down.get(pygame.K_3) or self.joyhats_down.get('down')

    def get_switch_weapon(self):
        return self.keys_down.get(pygame.K_4) or self.joyhats_down.get('right')

    def get_up(self,key_down=False):
        if key_down:
            return self.keys_down.get(pygame.K_UP) or self.keys_down.get(pygame.K_w) or self.get_joyaxis(1,-1)
        return self.keys[pygame.K_UP] or self.keys[pygame.K_w] or self.get_joyaxis(1,-1)

    def get_down(self,key_down=False):
        if key_down:
            return self.keys_down.get(pygame.K_DOWN) or self.keys_down.get(pygame.K_s) or self.get_joyaxis(1,1)
        return self.keys[pygame.K_DOWN] or self.keys[pygame.K_s] or self.get_joyaxis(1,1)

    def get_left(self,key_down=False):
        if key_down:
            return self.keys_down.get(pygame.K_LEFT) or self.keys_down.get(pygame.K_a) or self.get_joyaxis(0,-1)
        return self.keys[pygame.K_LEFT] or self.keys[pygame.K_a] or self.get_joyaxis(0,-1)

    def get_right(self,key_down=False):
        if key_down:
            return self.keys_down.get(pygame.K_RIGHT) or self.keys_down.get(pygame.K_d) or self.get_joyaxis(0,1)
        return self.keys[pygame.K_RIGHT] or self.keys[pygame.K_d] or self.get_joyaxis(0,1)

    def get_up_menu(self):
        return self.get_up(key_down=True) or self.joyhats_down.get('up')

    def get_down_menu(self):
        return self.get_down(key_down=True) or self.joyhats_down.get('down')

    def get_left_menu(self):
        return self.get_left(key_down=True) or self.joyhats_down.get('left')

    def get_right_menu(self):
        return self.get_right(key_down=True) or self.joyhats_down.get('right')

    def get_up_menu_no_alpha_move(self):
        return self.keys_down.get(pygame.K_UP) or self.joyhats_down.get('up')

    def get_down_menu_no_alpha_move(self):
        return self.keys_down.get(pygame.K_DOWN) or self.joyhats_down.get('down')

    def get_left_menu_no_alpha_move(self):
        return self.keys_down.get(pygame.K_LEFT) or self.joyhats_down.get('left')

    def get_right_menu_no_alpha_move(self):
        return self.keys_down.get(pygame.K_RIGHT) or self.joyhats_down.get('right')

    def get_up_joy(self):
        return self.get_joyaxis(1,-1)

    def get_down_joy(self):
        return self.get_joyaxis(1,1)

    def get_left_joy(self):
        return self.get_joyaxis(0,-1)

    def get_right_joy(self):
        return self.get_joyaxis(0,1)

    def get_up_joy_2(self):
        return self.get_joyaxis(4,-1)

    def get_down_joy_2(self):
        return self.get_joyaxis(4,1)

    def get_left_joy_2(self):
        return self.get_joyaxis(3,-1)

    def get_right_joy_2(self):
        return self.get_joyaxis(3,1)

    def get_upleft_joy_2(self):
        return self.get_up_joy_2() and self.get_left_joy_2()

    def get_upright_joy_2(self):
        return self.get_up_joy_2() and self.get_right_joy_2()

    def get_downright_joy_2(self):
        return self.get_down_joy_2() and self.get_right_joy_2()

    def get_downleft_joy_2(self):
        return self.get_down_joy_2() and self.get_left_joy_2()

    def get_attack_action(self):
        return self.keys_down.get(pygame.K_SPACE) or self.buttons_down.get(5)

    def get_off_attack_action(self):
        return self.keys_down.get(pygame.K_LCTRL) or self.buttons_down.get(4)

    def get_magic_action(self):
        return self.keys_down.get(pygame.K_r) or self.buttons_down.get(4)

    def get_interact(self,key_down=False):
        if key_down:
            return self.keys_down.get(pygame.K_e) or self.buttons_down.get(0) # 0 is A
        return self.keys[pygame.K_e] or self.get_joybutton(0)

    def get_alt_interact(self):
        return self.keys_down.get(pygame.K_u) or self.buttons_down.get(3) # 3 is Y

    def get_dodge(self):
        return self.keys_down.get(pygame.K_LSHIFT) or self.buttons_down.get(1) # 1 is B

    def get_use_item(self):
        return self.keys_down.get(pygame.K_q) or self.buttons_down.get(2) # 2 is X

    def get_cancel(self):
        return self.keys_down.get(pygame.K_ESCAPE) or self.buttons_down.get(1) # 1 is B

    def get_structures_toggle(self):
        return self.keys_down.get(pygame.K_b)

    def get_colonists_toggle(self):
        return self.keys_down.get(pygame.K_c)

    def get_inventory_toggle(self):
        return self.keys_down.get(pygame.K_i)

    def get_message_feed_toggle(self):
        return self.keys_down.get(pygame.K_l)

    def get_menu_switch(self):
        return self.keys_down.get(pygame.K_TAB) or self.buttons_down.get(5)

    def get_target_cycle(self):
        return self.keys_down.get(pygame.K_TAB)

    def get_pause_menu_open(self):
        return self.keys_down.get(pygame.K_RETURN) or self.buttons_down.get(7)

    def get_menu_interact(self,key_down=False):
        if key_down:
            # print(f"key_down in get_menu_interact is {key_down}; keys down is {self.keys_down}")
            return self.keys[pygame.K_e] or self.get_joybutton(0) or self.keys[pygame.K_RETURN]
        return self.buttons_down.get(0) or self.keys_down.get(pygame.K_e) or self.keys_down.get(pygame.K_RETURN)

    def get_enter(self):
        return self.buttons_down.get(0) or self.keys_down.get(pygame.K_RETURN)

    def get_backspace(self):
        return self.keys[pygame.K_BACKSPACE]

    def get_backspace_down(self):
        return self.keys_down.get(pygame.K_BACKSPACE)

    def get_delete(self):
        return self.keys[pygame.K_DELETE]

    def get_delete_down(self):
        return self.keys_down.get(pygame.K_DELETE)

    def get_space(self):
        return self.keys[pygame.K_SPACE]

    def get_space_down(self):
        return self.keys_down.get(pygame.K_SPACE)

    def get_inspector_toggle(self):
        return self.keys_down.get(pygame.K_F1)

    def get_pause_no_screen(self):
        return self.keys_down.get(pygame.K_F2)

    def get_debug_toggle(self):
        return self.keys_down.get(pygame.K_F3)

    def get_grid_toggle(self):
        return self.keys_down.get(pygame.K_F4)

    def get_console_toggle(self):
        return self.keys_down.get(pygame.K_F5)

    def get_spawn_tree(self):
        return self.keys_down.get(pygame.K_F6)

    def get_spawn_shroom(self):
        return self.keys_down.get(pygame.K_F7)

    def get_spawn_wheat(self):
        return self.keys_down.get(pygame.K_F8)

    def get_lmb_down(self):
        return self.keys_down.get('LMB')

    def get_lmb_up(self):
        return self.keys_up.get('LMB')

    def get_mmb_down(self):
        return self.keys_down.get('MMB')

    def get_mmb_up(self):
        return self.keys_up.get('MMB')

    def get_rmb_down(self):
        return self.keys_down.get('RMB')

    def get_rmb_up(self):
        return self.keys_up.get('RMB')

    def get_mb_wheel_up(self):
        return self.keys_down.get('MB4')

    def get_mb_wheel_down(self):
        return self.keys_down.get('MB5')

    def get_mouse_move(self):
        return pygame.mouse.get_rel()

    def get_shift(self):
        return self.keys[pygame.K_LSHIFT] or self.keys[pygame.K_RSHIFT]

    def get_escape(self):
        return self.keys_down.get(pygame.K_ESCAPE) # what button is start? # or self.buttons_down.get(1) # 1 is B
