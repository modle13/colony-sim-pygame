import random

import pygame

from util import colors
from entities.colonists.target_enum import TARGET_TYPE


# TODO: need more comprehensive item definitions


# polygon sizes should always be calculated as fractions;
#   scaling will occur based on unit size and structure size during instantiation
#   this avoids having to care about grid size and scaling at this definition time

# TODO: should these follow clockwise? topleft > topright > bottomright > bottomleft
default_polygon = [
    # bottom (left to right)
    (0, 1),
    (1 / 5, 1),
    (2 / 5, 3 / 4),
    (3 / 5, 3 / 4),
    (4 / 5, 1),
    (1, 1),
    # right (bottom to top)
    (3 / 4, 1 / 4),
    # top (right to left)
    (1 / 4, 1 / 4),
    # left (top to bottom) automatically drawn with polygon completion
]

octagon_polygon = [
    # top (left to right)
    (1 / 3, 0),
    (2 / 3, 0),
    # right (top to bottom)
    (1, 1 / 3),
    (1, 2 / 3),
    # bottom (right to left)
    (2 / 3, 1),
    (1 / 3, 1),
    # left (bottom to top)
    (0, 2 / 3),
    (0, 1 / 3),
]

# default sizes will be (1, 1); set `'size': sizes.get('size_you_want'),` key on a structure type to set a custom size
sizes = {
    'std': (2, 2),
    'wide': (3, 2),
    'narrow': (1, 2),
    'narrow_rotated': (2, 1),
    'std_3': (3, 3),
}

def get_pieces(unit):
    thickness = 5
    return [
        pygame.Rect(0,0,unit,thickness), # top
        pygame.Rect(unit-thickness,thickness,unit,unit), # right
        pygame.Rect(0,unit-thickness,unit,unit), # bottom
        pygame.Rect(0,0,thickness,unit), # left
    ]

def gen_rects(seed,unit):
    random.seed(seed)

    rects = []

    for ea in range(random.randint(10,15)):
        w = random.uniform(unit/12,unit/4)
        h = random.uniform(unit/12,unit/4)
        x = random.uniform(0,unit-w)
        y = random.uniform(0,unit-h)
        rects.append(pygame.Rect(x,y,w,h))
    return rects

def get_overlay(name,unit):
    subunit = unit/24
    thickness = unit/8
    return gen_rects(name,unit)
    match name:
        case "composter":
            return [
                pygame.Rect(5*subunit,subunit,8*subunit,subunit), # C-top
                pygame.Rect(3*subunit,subunit,2*subunit,3*subunit), # C-side
                pygame.Rect(4*subunit,3*subunit,3*subunit,3*subunit), # C-bottom
            ]
        case "wheat_farmer":
            return [
                pygame.Rect(subunit,subunit,3*subunit,subunit), # W-left
                pygame.Rect(subunit,subunit,subunit,3*subunit), # W-mid
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # W-right
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # bottom
            ]
        case "forester":
            return [
                pygame.Rect(subunit,subunit,subunit,subunit), # F-top
                pygame.Rect(subunit,subunit,subunit,3*subunit), # F-top
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # F-mid
            ]
        case "sawmill":
            return [
                pygame.Rect(subunit,subunit,subunit,subunit), # S-top
                pygame.Rect(subunit,subunit,subunit,3*subunit), # S-left
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # S-mid
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # S-right
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # S-bottom
            ]
        case "kitchen":
            return [
                pygame.Rect(subunit,subunit,3*subunit,subunit), # K-left
                pygame.Rect(subunit,subunit,subunit,3*subunit), # K-mid
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # K-right-top
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # K-right-bottom
            ]
        case "hunter":
            return [
                pygame.Rect(subunit,subunit,3*subunit,subunit), # H-left
                pygame.Rect(subunit,subunit,subunit,3*subunit), # H-mid
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # H-right
            ]
        case "thresher":
            return [
                pygame.Rect(subunit,subunit,subunit,subunit), # T-top
                pygame.Rect(5*subunit,subunit,subunit,5*subunit), # T-mid
            ]
        case "woodcutter":
            return [
                pygame.Rect(subunit,subunit,3*subunit,subunit), # W-left
                pygame.Rect(subunit,subunit,subunit,3*subunit), # W-mid
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # W-right
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # bottom
            ]
        case "forager":
            return [
                pygame.Rect(subunit,subunit,subunit,subunit), # F-top
                pygame.Rect(subunit,subunit,subunit,3*subunit), # F-top
                pygame.Rect(subunit,3*subunit,3*subunit,3*subunit), # F-mid
            ]
        case "quarry":
            return [
                pygame.Rect(5*subunit,2*subunit,8*unit/2,thickness), # Q-top
                pygame.Rect(2.5*subunit,unit/3,thickness,5*subunit), # Q-left
                pygame.Rect(3*subunit,9*subunit,unit/2,thickness), # Q-bottom
                pygame.Rect(8*subunit,unit/3,thickness,5*subunit), # Q-right
                # pygame.Rect(unit/4,3*unit/4,unit/2,unit/6), # Q-bottom
                # pygame.Rect(unit/3,5*unit/12,unit/12,unit/6), # Q-tick
            ]
        case "town_center":
            return [
                pygame.Rect(subunit,subunit,subunit,subunit), # T-top
                pygame.Rect(5*subunit,subunit,subunit,5*subunit), # T-mid
            ]
        case "trading_post":
            return [
                pygame.Rect(subunit,subunit,subunit,subunit), # T-top
                pygame.Rect(5*subunit,subunit,subunit,5*subunit), # T-mid
            ]

"""
possible fields; schema-validator would be useful here

# type
target_type       : string, currently everything is type 'storage';
                            this should be defaulted True instead of a string
action_archetype  : string, colonist action archetype to be assigned

# flags
buildable         : bool,   structure can be built via structure selection menu
impervious        : bool,   structure will ignore delete/destroy commands
operable          : bool,   a colonist can be assigned
show_influence    : bool,   toggles influence range visibility
crafting_station  : bool,   is a target that can be directly worked
                                assigned colonist will work to produce items

# products
capacity          : int,    max number for each type of inventory; TODO: separate input from output
cultivate_data    : dict,   data used for cultivation, include item and event name
                                example: {'name': 'wheat_seed', 'event_id': 'ADDWHEAT'},
input_types       : list,   items to be processed
production_map    : dict,   defines production details for target items
                                example: {'fertilizer': {'consumes': {'compost': 1}, 'produces': 1}},
                                this consumes 1 compost and produces one fertilizer
                                add more keys to consumes dictionary to change requirements
storage_types     : list,   items eligible to be stored in the output

# workers
helper_max        : int,    max number of additional helpers that can be assigned
influence_range   : int,    number of tiles from center to edge of
                                influence square around structure

# draw
color             : const,  a 3-element tuple imported from the colors module
polygon           : list,   tuples, points of a polygon
size              : string, key reference to sizes dict
"""
structures = {
    'composter': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'crafting_station': True,
        'capacity': 10,
        'input_types': ['compost'],
        'build_types': ['plank','stone'],
        'production_map': {'fertilizer': {'consumes': {'compost': 2}, 'produces': 1}},
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['fertilizer'],
        'helper_max': 5,
        'influence_range': 1,
        'color': colors.ORANGE,
        'polygon': default_polygon,
    },
    'wheat_farmer': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'cultivator',
        'show_influence': True,
        'operable': True,
        'crafting_station': True,
        'capacity': 10,
        'cultivate_data': {'name': 'wheat_seed', 'event_id': 'ADDWHEAT'},
        'input_types': ['fertilizer', 'wheat_germ'],
        'build_types': ['plank','stone'],
        'production_map': {'wheat_seed': {'consumes': {'wheat_germ': 1, 'fertilizer': 1}, 'produces': 1}},
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['wheat_seed'],
        'helper_max': 5,
        'influence_range': 7,
        'color': colors.YELLOW,
        'polygon': default_polygon,
    },
    'forester': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'cultivator',
        'show_influence': True,
        'operable': True,
        'crafting_station': True,
        'capacity': 10,
        'cultivate_data': {'name': 'sapling', 'event_id': 'ADDTREE'},
        'input_types': ['fertilizer'],
        'build_types': ['plank','stone'],
        'production_map': {'sapling': {'consumes': {'fertilizer': 1}, 'produces': 1}},
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['sapling'],
        'helper_max': 5,
        'influence_range': 7,
        'color': colors.LIGHT_GREEN,
        'polygon': default_polygon,
    },
    'sawmill': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'crafting_station': True,
        'capacity': 10,
        'input_types': ['log'],
        'build_types': ['plank','stone'],
        'production_map': {'plank': {'consumes': {'log': 1}, 'produces': {'plank': 2, 'sawdust': 1}}},
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['plank', 'sawdust'],
        'helper_max': 5,
        'influence_range': 1,
        'color': colors.YELLOW,
        'polygon': default_polygon,
    },
    'kitchen': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'crafting_station': True,
        'capacity': 10,
        'input_types': ['shroombit', 'grain'],
        'build_types': ['plank','stone'],
        'production_map': {'soup': {'consumes': {'shroombit': 1, 'grain': 1}, 'produces': 1}},
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['soup'],
        'helper_max': 5,
        'influence_range': 1,
        'color': colors.BROWN,
        'polygon': default_polygon,
    },
    'hunter': {
        'target_type': TARGET_TYPE.HARVEST,
        'action_archetype': 'harvester',
        'operable': True,
        'helper_max': 5,
        'capacity': 10,
        'build_types': ['plank','stone'],
        'build_mats': {'plank': 2, 'stone': 2},
        'color': colors.TAN,
        'polygon': default_polygon,
    },
    'thresher': {
        'target_type': TARGET_TYPE.HARVEST,
        'action_archetype': 'harvester',
        'operable': True,
        'show_influence': True,
        'helper_max': 5,
        'influence_range': 8,
        'capacity': 10,
        'build_types': ['plank','stone'],
        'build_mats': {'plank': 2, 'stone': 2},
        'color': colors.LIGHT_YELLOW,
        'polygon': default_polygon,
    },
    'woodcutter': {
        'target_type': TARGET_TYPE.HARVEST,
        'action_archetype': 'harvester',
        'operable': True,
        'show_influence': True,
        'capacity': 10,
        'build_types': ['plank','stone'],
        'build_mats': {'plank': 2, 'stone': 2},
        'helper_max': 5,
        'influence_range': 8,
        'color': colors.GRAY,
        'polygon': default_polygon,
        'autobuild': True,
    },
    'forager': {
        'target_type': TARGET_TYPE.HARVEST,
        'action_archetype': 'harvester',
        'harvester': True,
        'operable': True,
        'show_influence': True,
        'helper_max': 5,
        'influence_range': 8,
        'capacity': 10,
        'build_types': ['plank','stone'],
        'build_mats': {'plank': 2, 'stone': 2},
        'color': colors.BROWN,
        'polygon': default_polygon,
    },
    'quarry': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'processor',
        'harvester': False,
        'operable': True,
        'crafting_station': True,
        'show_influence': False,
        'helper_max': 5,
        'influence_range': 8,
        'capacity': 10,
        'build_types': ['plank','stone'],
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['copper', 'coal', 'iron', 'silver', 'gold', 'stone'],
        'production_map': {'stone': {'consumes': {}, 'produces': 1}},
        'color': colors.GRAY,
        'polygon': default_polygon,
    },
    'town_center': {
        'target_type': TARGET_TYPE.STORAGE,
        'dedicated_storage': True,
        'buildable': True,
        'impervious': True,
        'operable': False,
        'capacity': 10000,
        # storage_types are set dynamically below
        'build_types': ['plank','stone'],
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': [],
        'helper_max': 0,
        'color': colors.PINK,
        'polygon': octagon_polygon,
        #'size': sizes.get('std_3'),
        'autobuild': True,
    },
    'trading_post': {
        'target_type': TARGET_TYPE.STORAGE,
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'crafting_station': True,
        'capacity': 9999999,
        # input types are set dynamically below
        'input_types': [],
        'build_types': ['plank','stone'],
        # storage_types are set dynamically below
        'production_map': {'coins': {'consumes': {'ANY': 1}, 'produces': 1}},
        'build_mats': {'plank': 2, 'stone': 2},
        'storage_types': ['coins'],
        'helper_max': 0,
        'influence_range': 1,
        'color': colors.PINK,
        'polygon': default_polygon,
    },
}

# aggregate storage types
storage_items = list(map(
        lambda elem: elem[1].get('storage_types',[]),
        structures.items()
    ))
# flatten storage_types list of lists
storage_items = [item for sublist in storage_items for item in sublist if item]
# get unique list of storage types
storage_items = list(set(storage_items))
# set town_center storage_types
structures.get('town_center')['storage_types'] = storage_items

tradeable_items = [item for item in storage_items if item and item != 'coins']
structures.get('trading_post')['input_types'] = tradeable_items
