import logging
import random

import pygame

from settings import config
from ui.context_pane import ContextPane
from util.base_sprite import BaseSprite
from util import colors


logger = logging.getLogger(__name__)


# Define the colonist object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'colonist'
class Entity(BaseSprite):
    def __init__(self,spawn_pos=(0,0)):
        super().__init__()
        # for images later
        # self.surf = pygame.image.load(full_path).convert_alpha()

        if not hasattr(self, 'surf'):
            self.surf = pygame.Surface(config.colonist_def.get('surf_size'))
            random_color = (random.randrange(120, 220), random.randrange(120, 220), random.randrange(120, 220))
            self.surf.fill(random_color)

        self.rect = self.surf.get_rect(center=spawn_pos)
        self.position = pygame.math.Vector2(self.rect.center)
        self.distance_to_target = 10000
        self.direction = pygame.math.Vector2()
        self.hitbox = self.rect.inflate(0,-10)
        self.speed = 3
        self.set_representations()
        self.context_pane = ContextPane(self, self.sprite_type)

        # animations
        self.frame_index = 0
        self.animation_speed = 0.08

    def update(self):
        self.context_pane.update()
        if not self.target:
            return
        self.update_distance_vars()

    def move(self):
        x_diff = self.direction.x*self.speed
        y_diff = self.direction.y*self.speed
        # logger.info(f'x_diff {x_diff}, y_diff {y_diff}')
        self.position += self.direction*self.speed
        self.hitbox.center = self.position

        # sync hitbox with rect, which is what gets drawn
        self.rect.center = self.hitbox.center

    def update_distance_vars(self):
        if not self.target:
            return
        self.distance_to_target,self.direction = self.get_bearing(self.target)

    def get_bearing(self,target):
        self_vec = pygame.math.Vector2(self.rect.center)
        target_vec = pygame.math.Vector2(target.rect.center)
        # vector length
        distance = (target_vec - self_vec).magnitude()

        if distance > 0:
            direction = (target_vec - self_vec).normalize()
        else:
            direction = pygame.math.Vector2()

        return distance, direction

    def set_representations(self):
        self.representation = {
            'position': self.position_representation,
            'target': self.target_representation,
            'state': self.state_representation,
            'role': self.role_representation,
            'inventory': self.inventory_representation,
        }

    def position_representation(self):
        return self.rect.center

    def target_representation(self):
        if not hasattr(self, 'target') or not self.target:
            return ''
        return self.target.label

    def state_representation(self):
        return self.state

    def role_representation(self):
        if self.role:
            return f'{self.role.name}: {self.role.state}'
        return ''

    def inventory_representation(self):
        if not hasattr(self, 'inventory'):
            return ''
        return self.inventory.get_contents_repr()

    def show_context(self):
        self.context_pane.display()
