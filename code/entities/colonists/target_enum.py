from enum import Enum


class TARGET_TYPE(Enum):
    DUMMY = 0
    GRID = 1
    PRODUCT = 2
    RESOURCE = 3
    STORAGE = 4
    BUILD = 5
    HARVEST = 6
