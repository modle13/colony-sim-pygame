import logging

import pygame

from entities.colonists.colonist import Colonist
from settings import config
from util.events.event_queue import EventQueue
from util.support import blit


logger = logging.getLogger(__name__)


# TODO: keep confusing ColonistManager with ColonyManager
#   maybe rename to PopulationManager
class ColonistManager:
    def __init__(self,colony):
        self.colony = colony
        self.colonist_limit = 2
        self.next_colonist_id = 1

        self.colonist_spawn_can_tick = False
        self.colonist_spawn_tick_delay = 2000
        self.colonist_spawn_last_tick = 0

        self.colonists = pygame.sprite.Group()
        self.subscribe_to_events()

        logger.info(f"ColonistManager for {colony} initialized")
        # self.spawn_initial_colonists()

    def update(self):
        # logger.info("updating colonist manager")
        self.colonists.update()
        self.cooldowns()
        self.manage_spawns()

    def draw(self):
        for ea in self.colonists:
            # ea.draw_debug()
            blit(ea)

    def manage_spawns(self):
        if not self.colonist_spawn_can_tick or len(self.colonists) == self.colonist_limit:
            return
        self.spawn_colonist()

    def spawn_initial_colonists(self):
        while len(self.colonists) < self.colonist_limit:
            self.spawn_colonist()
        self.colonist_spawn_can_tick = False
        self.colonist_spawn_last_tick = pygame.time.get_ticks()

    def spawn_colonist(self):
        new_colonist = Colonist(self.colony,self.next_colonist_id)
        self.next_colonist_id += 1
        # EventQueue.instance().publish("COLONIST_SPAWNED",[new_colonist])
        self.colonists.add(new_colonist)
        # config.game.game_objects.add(new_colonist)
        # config.game.all_sprites.add(new_colonist)
        # self.colonist_spawn_can_tick = False
        # self.colonist_spawn_last_tick = pygame.time.get_ticks()

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.colonist_spawn_can_tick:
            if current_time - self.colonist_spawn_last_tick >= self.colonist_spawn_tick_delay:
                self.colonist_spawn_can_tick = True

    def subscribe_to_events(self):
        pass
        # EventQueue.instance().subscribe('SOME_EVENT',self.some_func)
