from enum import Enum


class COLONIST_STATE(Enum):
    IDLE = 0
    TARGET_MODE = 1
    HUNGRY = 2
    WORKING = 3
