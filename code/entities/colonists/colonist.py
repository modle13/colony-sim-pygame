import logging
import random

import pygame
from entities.colonists.colonist_state import COLONIST_STATE
from entities.colonists.roles.assignments.hauler import Hauler
from entities.colonists.roles.role_manager import RoleManager
from entities.colonists.target_enum import TARGET_TYPE
from entities.entity import Entity
from settings import config
from ui.text import Text
from util import colors
from util.debug import log_stack
from util.inventory import Inventory
from util.support import import_folder


logger = logging.getLogger(__name__)


class Colonist(Entity):
    def __init__(self,colony,id):
        self.sprite_type = 'colonist'
        self.label = 'colonist'
        self.name = 'colonist'
        self.state = COLONIST_STATE.IDLE
        self.role = None
        self.colony = colony
        self.id = id

        # animations
        self.animations = {}
        self.import_player_assets()
        self.status = 'down'
        self.facing = 'down'
        self.default_size = (config.unit//5,config.unit//5)
        self.surf = pygame.image.load('../graphics/npcs/colonist1/idle/npc1.png').convert_alpha()
        # FIXME: doesn't seem to scale properly
        self.surf = pygame.transform.scale(self.surf,self.default_size)
        # sprite base class draw requires image and rect
        self.image = self.surf

        super().__init__((config.SCREEN_WIDTH//2-config.unit//2.5,config.SCREEN_HEIGHT//2-config.unit//2.5))

        self.inventory = Inventory(purpose='colonist', limit_type='total', limit=config.colonist_def.get('inventory_size'))

        self.roles = RoleManager.instance().instantiate_roles(self)
        self.set_role("hauler")

        self.assignment = None
        self.show = True
        self.path_cells = [self.rect.center]
        self.target = None
        # self.path_cells.append(self.target.rect.center)
        self.trigger_distance = 5

        self.debug_repr_sprite = Text(config.TINY_FONT, colors.WHITE, colors.BLACK, 0, -5)

        # TIMERS
        self.assignment_change_time = 0
        self.assignment_change_allowed = False
        self.assignment_change_cooldown = 1000

    def update(self):
        # logger.info(f"colonist state is {self.state}")
        super().update()
        self.cooldowns()
        self.role.update()
        self.handle_target()

        match self.state:
            case COLONIST_STATE.IDLE:
                self.move()
                if self.target:
                    self.set_state(COLONIST_STATE.TARGET_MODE)
            case COLONIST_STATE.TARGET_MODE:
                self.move()
            case COLONIST_STATE.WORKING:
                if not self.target:
                    # logger.info("no target in colonist.update, setting to COLONIST_STATE.IDLE")
                    self.set_state(COLONIST_STATE.IDLE)
                elif self.target.target_type == TARGET_TYPE.DUMMY and self.check_target_distance():
                    self.role.trigger_working_state()

        self.update_repr()
        self.animate()

    def set_state(self,new_state):
        # log_stack()

        # if self.target:
        #     self.check_target_distance()
        # else:
        #     logger.info("no target")

        if self.state == new_state:
            return
        if new_state == COLONIST_STATE.WORKING and not self.check_target_distance():
            # logger.info(f"{self.role.archetype_repr()} NOT CLOSE ENOUGH TO TARGET {self.target.name}")
            return

        # logger.info(f'Colonist.set_state(): {self.role.archetype_name}:' + \
        #     f'{self.role.name}: setting state from {self.state} to {new_state}; ' + \
        #     f'target is {self.target.name if self.target else "no target"}; role state is {self.role.state}; ' + \
        #     f'inventory is {self.inventory}'
        # )

        self.state = new_state

        match self.state:
            case COLONIST_STATE.WORKING:
                # logger.info(f"triggering working state on {self.target.name}")
                self.role.trigger_working_state()

    def set_target(self,new_target):
        if self.target == new_target:
            return

        # logger.info(f"Colonist.set_target() setting target from {self.target.name if self.target else None} to {new_target.name if new_target else None}")
        # log_stack()

        self.target = new_target
        self.update_distance_vars()

    def import_player_assets(self):
        character_path = '../graphics/npcs/colonist1/'
        self.animations = {'idle': [], 'up': [], 'down': [], 'left': [], 'right': [],
            'up_idle': [], 'down_idle': [], 'left_idle': [], 'right_idle': [],
            'up_attack': [], 'down_attack': [], 'left_attack': [], 'right_attack': []}

        for animation in self.animations.keys():
            full_path = character_path + animation
            # self.animations[animation] = import_folder(full_path,scale=(2,2))
            self.animations[animation] = import_folder(full_path)

    def draw_extra(self, screen):
        show_line = True
        show_line = self.target and hasattr(self.target,'context_pane') and self.target.context_pane.show
        show_line = show_line or self.debug_repr_sprite.show

        if show_line and self.target:
            pygame.draw.line(
                screen,
                self.target.color,
                config.camera.apply(self).center,
                config.camera.apply(self.target).center,
                width=1
            )
        self.draw_debug()

    def draw_debug(self):
        self.role.draw_debug()

    def move(self):
        super().move()

    def animate(self):
        self.status = 'right_idle'
        if self.direction.x < 0:
            self.status = 'left_idle'

        animation = self.animations[self.status]
        # print('setting animation to ' + self.status)

        self.frame_index += self.animation_speed
        if self.frame_index >= len(animation):
            if self.status == 'attack':
                self.can_attack = False
            self.frame_index = 0

        self.surf = animation[int(self.frame_index)]
        self.image = self.surf
        self.rect = self.surf.get_rect(center = self.hitbox.center)

    def check_target_distance(self):
        # logger.info(f"target distance: {self.distance_to_target}; trigger distance: {self.trigger_distance}; pos: {self.rect.center}, target: {self.target.rect.center}")
        return self.distance_to_target < self.trigger_distance

    def set_idle(self):
        self.set_state(COLONIST_STATE.IDLE)

    def set_role(self,role_name):
        self.assign_role(self.roles.get(role_name))

    def assign_role(self,new_role):
        # initialized role
        self.role = new_role
        self.drop_all_inventory()

    def unassign_role(self):
        # log_stack()
        # logger.info(f"unassigning colonist from {self.action_archetype} {self.assignment} {self.role}")
        self.action_archetype = None
        self.assignment = None
        self.role = self.roles.get('hauler')
        self.assignment_change_allowed = False
        self.assignment_change_time = pygame.time.get_ticks()

    def handle_target(self):
        self.set_target(self.role.target)
        self.update_distance_vars()

        # logger.info(f'distance_to_target {self.distance_to_target}, trigger distance {self.trigger_distance}')
        if not self.target or not self.check_target_distance():
            if self.state == COLONIST_STATE.WORKING:
                self.set_idle()
            return

        if self.check_target_distance():
            self.set_state(COLONIST_STATE.WORKING)
        else:
            self.set_state(COLONIST_STATE.TARGET_MODE)

    def in_structure_range(self, target_obj):
        # default to True, so non-restricted roles like hauler don't have a range limitation
        in_range = True

        if self.assignment and self.assignment.influence.range > 1:
            in_range = self.assignment.influence.rect.colliderect(target_obj.rect)
        return in_range

    def drop_all_inventory(self):
        if not self.inventory.is_empty():
            for key, value in self.inventory.contents.items():
                self.colony.attempt_product_add(key,value,self.position)
            self.inventory.reset()

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        if not self.assignment_change_allowed:
            if current_time - self.assignment_change_time >= self.assignment_change_cooldown:
                self.assignment_change_allowed = True

    def update_repr(self):
        self.debug_repr_sprite.update(self.role.name)
        pos = (self.rect.x, self.rect.y - 10)
        size = (self.rect.width, self.rect.height)
        self.debug_repr_sprite.rect.update(pos, size)
        self.status_message = self.state
        # if self.state.hungry:
        #     self.status_message = 'hungry'

    def __repr__(self):
        return f"{self.role}/{self.target}/{self.assignment}/{self.assignment_change_allowed}"
