import random

import pygame

from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from util.base_sprite import BaseSprite


class Target(BaseSprite):
    def __init__(self):
        super().__init__()
        unit = config.tile_size
        self.surf = pygame.Surface((unit, unit))
        multiplier = unit
        # divide screen dimensions by multiplier to ensure
        # targets aren't too close together
        x = random.randrange(int(config.max_width / multiplier)) * multiplier
        y = random.randrange(int(config.max_height / multiplier)) * multiplier
        self.start = (x, y)
        self.rect = self.surf.get_rect(topleft=self.start)
        self.hitbox = self.rect.inflate(0,-10)
        self.target_type = TARGET_TYPE.DUMMY
        self.label = 'dummy'
        self.name = 'dummy'
        self.cultivator = False
        self.claimed = False
        self.active = True
        self.expired = False
        self.show = True
        self.color = (random.randrange(120, 220), random.randrange(120, 220), random.randrange(120, 220))
        self.surf.fill(self.color)
        self.target_size = 1

    def work(self):
        return True

    def __repr__(self):
        return 'dummy target'

    def destroy(self):
        self.kill()
