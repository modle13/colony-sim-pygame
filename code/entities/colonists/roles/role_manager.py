import logging

import pygame

# from entities.colonists.colonist import Colonist
# from settings import config
# from util.events.event_queue import EventQueue
from util.singleton import Singleton
from entities.colonists.roles.assignments.builder import Builder
from entities.colonists.roles.assignments.composter import Composter
from entities.colonists.roles.assignments.forager import Forager
from entities.colonists.roles.assignments.forester import Forester
from entities.colonists.roles.assignments.hauler import Hauler
from entities.colonists.roles.assignments.hunter import Hunter
from entities.colonists.roles.assignments.kitchen import Kitchen
from entities.colonists.roles.assignments.quarry import Quarry
from entities.colonists.roles.assignments.sawmill import Sawmill
from entities.colonists.roles.assignments.thresher import Thresher
from entities.colonists.roles.assignments.trading_post import TradingPost
from entities.colonists.roles.assignments.wheat_farmer import WheatFarmer
from entities.colonists.roles.assignments.woodcutter import Woodcutter


logger = logging.getLogger(__name__)


@Singleton
class RoleManager:
    def __init__(self):
        self.role_mapping = {
            'builder': Builder,
            'composter': Composter,
            'forager': Forager,
            'forester': Forester,
            'hauler': Hauler,
            'hunter': Hunter,
            'kitchen': Kitchen,
            'quarry': Quarry,
            'sawmill': Sawmill,
            'thresher': Thresher,
            'trading_post': TradingPost,
            'wheat_farmer': WheatFarmer,
            'woodcutter': Woodcutter,
        }

    def map_role(self,role):
        return self.role_mapping.get(role)

    def instantiate_roles(self,owner):
        instantiated = {}
        for name,role in self.role_mapping.items():
            instantiated[name] = role(owner)
        return instantiated
