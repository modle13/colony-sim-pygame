import logging

import pygame

from entities.colonists.target_enum import TARGET_TYPE
from entities.colonists import movement
from entities.colonists.roles.archetypes.role import Role
from entities.colonists.roles.archetypes.role_state import ROLE_STATE
from settings import config
from util.events.event_queue import EventQueue

from entities.colonists.actions.find_storage_with_plantable import FindStorageWithPlantable


logger = logging.getLogger(__name__)

"""
action priority
- (COMMON ACTION, all roles) if target is none or dummy (similar to GATHERER FIND_RESOURCE)
    - assign assignment as target if it is workable -> GO_TO_WORK
    - go to assignment (do nothing; colonist class will move it)
- (COMMON ACTION, all roles) if WORKING, work target
- (COMMON ACTION, farmer and processor) if (assignment is none or assignment is dummy or assignment not workable) and
    - if have inventory and if inventory is input items for workable assignment and
        - if assignment has room in input inventory
            - set target to assignment
            - go to assignment
            - put items in assignment input inventory
            - reset state to check if assignment is workable
        - else drop inventory
- (COMMON ACTION, farmer and processor) if don't have inventory
    - if assignment input has room
        - get assignment input items and find nearest storage containing them
- (COMMON ACTION, farmer and processor) if found input item
    - set that storage as target
    - move to storage
- (FARMER ACTION) - find plantable target
- (FARMER ACTION) - plant at target - should be handled by standard work operation?
"""


class Farmer(Role):
    def __init__(self,colonist,name):
        super().__init__(colonist,name)
        self.colonist = colonist
        self.archetype_name = 'Farmer'
        self.subtype_name = name
        self.state = ROLE_STATE.TARGET_CHECK_DELAY
        self.state_sequence = []
        self.target_item_kind = ''
        self.target_item_amt = 0
        logger.info(f'{self.subtype_name}:{self.archetype_name} initialized')

    def update(self):
        # return
        # logger.info(f'triggering update in Porter with state {self.state}; target is {self.target}')

        # logger.info(f'states {self.state_sequence}')

        match self.state:
            case ROLE_STATE.TARGET_CHECK_DELAY:
                # logger.info(f'self.target_check_ready: {self.target_check_ready}, self.target: {self.target}, self.target.name: {self.target.name if self.target else ""}')
                if self.target_check_ready:
                    # target_check_ready will stay True until there's no target
                    if not self.target or self.target.target_type == TARGET_TYPE.DUMMY:
                        if self.colonist.inventory.is_empty() and self.get_plantable_if_available():
                            # logger.info("attempting to find available plantable target")
                            # find plantable item if it is available
                            if self.target:
                                self.set_state(ROLE_STATE.MOVE_TO_STORAGE)
                        elif self.has_plantable():
                            # have materials needed to plant, find plantable target
                            # logger.info("farmer has plantable item")
                            self.find_plantable_target()
                            super().start_timer('target_check')
                        elif self.colonist.assignment.workable:
                            # work assignment if workable
                            # logger.info("target is workable")
                            self.target = self.colonist.assignment
                            self.set_state(ROLE_STATE.MOVE_TO_CRAFTABLE)
                            super().start_timer('target_check')
                        elif self.colonist.inventory.has_any_of(self.colonist.assignment.consumes):
                            # move inventory to assignment input if have any
                            # logger.info("colonist has inventory target needs")
                            self.target = self.colonist.assignment
                            self.set_state(ROLE_STATE.MOVE_TO_DELIVER_TO_INPUT)
                            super().start_timer('target_check')
                        elif self.colonist.inventory.is_empty():
                            # find items for assignment input
                            # logger.info("colonist does not have inventory")
                            self.set_state(ROLE_STATE.FIND_OUTPUT_COLLECT)
                            super().start_timer('target_check')
                        else:
                            # do nothing
                            logger.info("no action state conditions met")

                        super().start_timer('target_check')

                        #  and self.colonist.assignment.output_inventory.has_room():
            case ROLE_STATE.PLANT_ITEM:
                complete = self.target.work()
                if complete:
                    self.plant_item()
                    self.target = None
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
            case _:
                super().update()
                return

        self.common_update()

    def has_plantable(self):
        kind = self.colonist.assignment.cultivate_data.get('name')
        unit_amount = self.colonist.inventory.contains(kind)
        kind_available = unit_amount != None and unit_amount > 0
        # logger.info(f"farmer inventory found {unit_amount} of target {kind}")
        return kind_available

    def find_plantable_target(self):
        if self.colonist.target and self.colonist.target.target_type == 'grid':
            return

        unpopulated_squares = self.colonist.colony.get_unpopulated_tiles()
        filtered_targets = list(filter(lambda x: self.colonist.in_structure_range(x), unpopulated_squares))
        found = movement.find_closest_sprite(self.colonist, filtered_targets)
        if found:
            # logger.info(f"found plantable target at {found.rect.center}; range is {self.colonist.assignment.rect}; pos is {self.colonist.rect.center}")
            self.set_state(ROLE_STATE.MOVE_TO_PLANTABLE)
            self.target = found

    def plant_item(self):
        # TODO: this should move somewhere else; maybe Tile?
        # publish event to event queue
        resource_id = self.colonist.assignment.cultivate_data.get('event_id')
        EventQueue.instance().publish("ADD_RESOURCE",[resource_id,False,self.colonist.target.rect.center])

        name = self.colonist.assignment.cultivate_data.get('name')
        self.colonist.inventory.remove(name, 1)
        EventQueue.instance().publish('ADD_MESSAGE',[f'{self.archetype_repr()} planted a {name}'])
        # self.handle_work_complete()

    def get_plantable_if_available(self):
        self.target,self.target_item_kind,self.target_item_amt = FindStorageWithPlantable.instance().execute(
            self.colonist,
            self.colonist.inventory.get_capacity(),
            self.colonist.assignment.cultivate_data.get('name')
        )

        if self.target_item_kind:
            return True

        return False
