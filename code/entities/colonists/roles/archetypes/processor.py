import logging

import pygame

from entities.colonists.target_enum import TARGET_TYPE

from entities.colonists.roles.archetypes.role import Role
from entities.colonists.roles.archetypes.role_state import ROLE_STATE

logger = logging.getLogger(__name__)

"""
action priority
- (COMMON ACTION, all roles) if target is none or dummy (similar to GATHERER FIND_RESOURCE)
    - assign assignment as target if it is workable -> GO_TO_WORK
    - go to assignment (do nothing; colonist class will move it)
- (COMMON ACTION, all roles) if WORKING, work target
- (COMMON ACTION, farmer and processor) if have inventory and item is assignment output item
    - (because would have taken from output)
    - find other storage
    - deliver to other storage
- (COMMON ACTION, farmer and processor) if (assignment is none or assignment is dummy or assignment not workable) and
    - if have inventory and if inventory is input items for workable assignment and
        - if assignment has room in input inventory
            - set target to assignment
            - go to assignment
            - put items in assignment input inventory
            - reset state to check if assignment is workable
        - else drop inventory
- (COMMON ACTION, farmer and processor) if don't have inventory
    - if assignment input has room
        - get assignment input items and find nearest storage containing them
    - else if assignment output does not have room
        - get item from assignment output
- (COMMON ACTION, farmer and processor) if found input item
    - set that storage as target
    - move to storage
- (COMMON ACTION, all roles; need to add this to gatherer as a lowest priority) if at storage
    - get item from output_inventory
    - maybe find another if inventory is not full
    - reset state to cycle through to find assignment to deliver inventory
"""


class Processor(Role):
    def __init__(self,colonist,name):
        super().__init__(colonist,name)
        self.colonist = colonist
        self.archetype_name = 'Processor'
        self.subtype_name = name
        self.state = ROLE_STATE.TARGET_CHECK_DELAY
        self.state_sequence = []
        self.target_item_kind = ''
        logger.info(f'{self.subtype_name}:{self.archetype_name} initialized')

    def update(self):
        # logger.info(f'triggering update in Porter with state {self.state}; target is {self.target}')

        # logger.info(f'states {self.state_sequence}')

        match self.state:
            case ROLE_STATE.TARGET_CHECK_DELAY:
                # prioritize inventory delivery

                # TODO: possibly do all target assignment, including Target(), through this state update;
                #   this would simplify the target check and allow timers to be used every check;
                #   currently if the target is set elsewhere, both conditions can be false, or
                #   ready can be true and `not target` can be false, and resources are rarely searched for;
                #   both are rarely true together
                # logger.info(f'self.target_check_ready: {self.target_check_ready}, self.target: {self.target}, self.target.name: {self.target.name if self.target else ""}')
                if self.target_check_ready:
                    # target_check_ready will stay True until there's no target
                    if not self.target or self.target.target_type == TARGET_TYPE.DUMMY:
                        if self.colonist.assignment.workable:
                            # logger.info("target is workable")
                            self.target = self.colonist.assignment
                            self.set_state(ROLE_STATE.MOVE_TO_CRAFTABLE)
                            super().start_timer('target_check')
                        elif self.colonist.inventory.has_any_of(self.colonist.assignment.consumes):
                            # logger.info("colonist has inventory target needs")
                            self.target = self.colonist.assignment
                            self.set_state(ROLE_STATE.MOVE_TO_DELIVER_TO_INPUT)
                            super().start_timer('target_check')
                        elif self.colonist.inventory.is_empty():
                            # logger.info("colonist does not have inventory")
                            self.set_state(ROLE_STATE.FIND_OUTPUT_COLLECT)
                            super().start_timer('target_check')
                        else:
                            logger.info(f"{self.colonist.id} no action state conditions met, unassigning")
                            self.colonist.assignment.unassign()

                        super().start_timer('target_check')

                        #  and self.colonist.assignment.output_inventory.has_room():

            case _:
                super().update()
                return

        self.common_update()
