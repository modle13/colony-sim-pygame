from enum import Enum


class ROLE_STATE(Enum):
    TARGET_CHECK_DELAY = 0   # base state; checks timers, performs primary target assignment

    FIND_MATERIAL = 1        # check in storage/fallen items for assignment needed material
    FIND_RESOURCE = 2        # find tree, mushroom, wheat, etc. to harvest
    FIND_FALLEN_PRODUCT = 3  # find item on ground; hauler will find any, others will find only what they produce
    FIND_PLANTABLE_TILE = 4  # find plantable grid tile
    FIND_OUTPUT_COLLECT = 5  # generic output find; locates full outputs that need transfered to storage
    FIND_OUTPUT_DELIVER = 6  # set target to assignment; specific conditions for checking output inventory
    FIND_INPUT_DELIVER = 7   # set target to assignment or workable structure that needs materials; specific conditions for checking input inventory
    FIND_STORAGE_DELIVER = 8 #
    FIND_NEEDY_CRAFTABLE = 9 # find active work stations that have input room

    CRAFT_ITEM = 10          # work structure, similar to harvest, but against assignment target
    HARVEST = 11             # work resource
    PLANT_ITEM = 12          # plant item at target
    GATHER_ITEM = 13         # 'work' item on ground; picks it up
    TAKE_FROM_STORAGE = 14   # remove item from target structure output inventory
    TAKE_FROM_STORAGE_FOR_INPUT = 15 # next state = move to secondary target assignment input

    MOVE_TO_STORAGE = 16     # move state used to know what next state is on arrival
    MOVE_TO_CRAFTABLE = 17   # move state used to know what next state is on arrival
    MOVE_TO_TARGET = 18      # move state used to know what next state is on arrival
    MOVE_TO_RESOURCE = 19    # move state used to know what next state is on arrival
    MOVE_TO_PRODUCT = 20     # move state used to know what next state is on arrival
    MOVE_TO_PLANTABLE = 21   # move state used to know what next state is on arrival
    MOVE_TO_OUTPUT = 22      # move state used to know what next state is on arrival
    MOVE_TO_COLLECT_OUTPUT = 23
    MOVE_TO_COLLECT_FOR_INPUT = 24
    MOVE_TO_DELIVER_TO_INPUT = 25
    MOVE_TO_BUILDABLE = 26

    ADD_TO_OUTPUT = 27       # transfer colonist inventory to target output inventory
    ADD_TO_INPUT = 28        # transfer colonist inventory to target input inventory

    BUILD = 29
    FIND_BUILDABLE_STRUCTURE = 30
