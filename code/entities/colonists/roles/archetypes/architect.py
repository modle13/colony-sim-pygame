from enum import Enum
import logging

import pygame

from entities.colonists.target_enum import TARGET_TYPE

from entities.colonists.roles.archetypes.role import Role
from entities.colonists.roles.archetypes.role_state import ROLE_STATE


logger = logging.getLogger(__name__)


class Architect(Role):
    def __init__(self,colonist,name):
        super().__init__(colonist,name)
        self.colonist = colonist
        self.archetype_name = 'Architect'
        self.subtype_name = name
        self.state = ROLE_STATE.TARGET_CHECK_DELAY
        self.state_sequence = []
        logger.info(f'{self.subtype_name}:{self.archetype_name} initialized')

    def update(self):
        # cloned from Porter
        # logger.info(f'triggering update in Porter with state {self.state}; target is {self.target}')

        # logger.info(f'states {self.state_sequence}')

        match self.state:
            case ROLE_STATE.TARGET_CHECK_DELAY:
                # prioritize inventory delivery
                # kind of like processor
                    # first 3 handled by all classes
                        # get build materials
                        # move to structure
                        # add materials
                    # work structure until built

                if self.target_check_ready:
                    # target_check_ready will stay True until there's no target
                    if not self.target or self.target.target_type == TARGET_TYPE.DUMMY:
                        # workable means it can be built
                        if self.colonist.assignment and self.colonist.assignment.workable:
                            # logger.info("target is workable")
                            self.target = self.colonist.assignment
                            self.set_state(ROLE_STATE.MOVE_TO_BUILDABLE)
                            super().start_timer('target_check')
                        elif self.colonist.assignment and self.colonist.inventory.has_any_of(self.colonist.assignment.consumes):
                            # logger.info("colonist has inventory target needs")
                            self.target = self.colonist.assignment
                            self.set_state(ROLE_STATE.MOVE_TO_DELIVER_TO_INPUT)
                            super().start_timer('target_check')
                        elif self.colonist.inventory.is_empty():
                            # logger.info("colonist does not have inventory")
                            self.set_state(ROLE_STATE.FIND_OUTPUT_COLLECT)
                            super().start_timer('target_check')
                        else:
                            pass
                            # logger.info("no action state conditions met")

                        super().start_timer('target_check')

            case ROLE_STATE.ADD_TO_INPUT:
                # logger.debug(f"adding {self.colonist.inventory} to input of {self.target.name};")
                self.target.store(self.colonist.inventory.contents, "input")
                self.set_target(None)
                # self.target = None
                self.target_item_kind = ''
                self.assignment_to_source_input_for = None
                self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
            case _:
                super().update()
                return

        self.common_update()
