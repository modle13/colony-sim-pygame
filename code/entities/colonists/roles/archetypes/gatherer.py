import logging

import pygame

from entities.colonists.colonist_state import COLONIST_STATE
from entities.colonists.roles.archetypes.role import Role
from entities.colonists.roles.archetypes.role_state import ROLE_STATE
from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from util.debug import debug


logger = logging.getLogger(__name__)


class Gatherer(Role):
    def __init__(self,colonist,name):
        super().__init__(colonist,name)
        self.colonist = colonist
        self.archetype_name = 'Gatherer'
        self.subtype_name = name
        self.state = ROLE_STATE.TARGET_CHECK_DELAY
        self.state_sequence = []
        logger.info(f'{self.subtype_name}:{self.archetype_name} initialized')

    def update(self):
        # logger.info(f'triggering update in Gatherer with state {self.state}; target is {self.target}')

        # logger.info(f'states {self.state_sequence}')

        match self.state:
            case ROLE_STATE.TARGET_CHECK_DELAY:
                # prioritize inventory delivery

                # TODO: possibly do all target assignment, including Target(), through this state update;
                #   this would simplify the target check and allow timers to be used every check;
                #   currently if the target is set elsewhere, both conditions can be false, or
                #   ready can be true and `not target` can be false, and resources are rarely searched for;
                #   both are rarely true together
                # logger.info(f'self.target_check_ready: {self.target_check_ready}, self.target: {self.target}, self.target.name: {self.target.name if self.target else ""}')
                if self.target_check_ready:
                    # target_check_ready will stay True until there's no target
                    if not self.colonist.inventory.has_room():
                        # logger.info("no inventory room left!")
                        self.set_state(ROLE_STATE.FIND_OUTPUT_DELIVER)
                        super().start_timer('target_check')
                    elif not self.target or self.target.target_type == TARGET_TYPE.DUMMY:
                        # logger.info("triggering state set FIND_RESOURCE")
                        self.set_state(ROLE_STATE.FIND_RESOURCE)
                        super().start_timer('target_check')
            case ROLE_STATE.FIND_RESOURCE:
                self.find_resource()

                if self.target and self.target.target_type != TARGET_TYPE.DUMMY:
                    self.set_state(ROLE_STATE.MOVE_TO_RESOURCE)
                else:
                    # just unassign here; will become a hauler
                    # logger.info(f"{self.name} ROLE_STATE.FIND_RESOURCE did not find a valid target")
                    self.colonist.assignment.unassign()

                super().start_timer('target_check')
            case ROLE_STATE.FIND_FALLEN_PRODUCT:
                if self.colonist.inventory.has_room():
                    self.find_storage_product()
                    if self.target:
                        self.set_state(ROLE_STATE.MOVE_TO_PRODUCT)
                    else:
                        if self.colonist.inventory.is_empty():
                            # reset the state if no inventory
                            self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                        else:
                            # find output to deliver to
                            self.set_state(ROLE_STATE.FIND_OUTPUT_DELIVER)
                super().start_timer('target_check')
            case _:
                super().update()
                return

        self.common_update()
