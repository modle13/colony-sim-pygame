from enum import Enum
import logging

import pygame

from entities.colonists.actions.find_fallen_product import FindFallenProduct
from entities.colonists.actions.find_input_deliver import FindInputDeliver
from entities.colonists.actions.find_output_retrieve import FindOutputRetrieve
from entities.colonists.target_enum import TARGET_TYPE

from entities.colonists.roles.archetypes.role import Role
from entities.colonists.roles.archetypes.role_state import ROLE_STATE


logger = logging.getLogger(__name__)


class Porter(Role):
    def __init__(self,colonist,name):
        super().__init__(colonist,name)
        self.colonist = colonist
        self.archetype_name = 'Porter'
        self.subtype_name = name
        self.state = ROLE_STATE.TARGET_CHECK_DELAY
        self.state_sequence = []
        logger.info(f'{self.subtype_name}:{self.archetype_name} initialized')

    def update(self):
        # logger.info(f'triggering update in Porter with state {self.state}; target is {self.target}')

        # logger.info(f'states {self.state_sequence}')

        match self.state:
            case ROLE_STATE.TARGET_CHECK_DELAY:
                # prioritize inventory delivery

                if self.target_check_ready:
                    # target_check_ready will stay True until there's no target
                    # logger.info('checking porter target')
                    if not self.colonist.inventory.has_room():
                        # logger.info("no inventory room left!")
                        self.set_state(ROLE_STATE.FIND_STORAGE_DELIVER)
                        super().start_timer('target_check')
                        return
                    elif not self.target or self.target.target_type == TARGET_TYPE.DUMMY:
                        # logger.info("triggering state set ROLE_STATE.FIND_FALLEN_PRODUCT")
                        self.set_state(ROLE_STATE.FIND_NEEDY_CRAFTABLE)
                        super().start_timer('target_check')
                        return

            case ROLE_STATE.FIND_NEEDY_CRAFTABLE:
                """
                FindInputDeliver, needs target item, but is unused anywhere, could be adapted
                check all crafting_station structures
                check if their input inventories have room
                find the closest one, set it to assignment_to_source_input_for
                find storage with needed item
                set to MOVE_TO_COLLECT_FOR_INPUT
                if ROLE_STATE.TAKE_FROM_STORAGE_FOR_INPUT, take normally, then
                    set target to assignment_to_source_input_for

                bonus: find needy crafting station with least distance to storage
                    with needed item.
                        i.e. distance from colonist to crafting station + distance from that station to
                        storage with needed item
                """
                # logger.info(f"FindInputDeliver invocation here: {pygame.time.get_ticks()}")
                # FIXME: This is really inefficent to have every colonist filter target inputs
                    # should have a TargetManager class to track available types that need input
                target,item,amt = FindInputDeliver.instance().execute(self.colonist)
                # logger.info(f"FindInputDeliver invocation DONE: {pygame.time.get_ticks()}")
                if not target or not item:
                    # logger.info(f"did not find what was sought: target: {target}; item: {item}")
                    self.set_state(ROLE_STATE.FIND_FALLEN_PRODUCT)
                    return

                self.assignment_to_source_input_for = target
                target_amt = min(self.colonist.inventory.get_capacity(),amt)
                found,item_name,retrieved_amt = FindOutputRetrieve.instance().execute(self.colonist,target_amt,item)
                # logger.info(f"found output for {found}/{item_name}/{retrieved_amt}")

                if found:
                    # point at target and set claim on input of source
                    self.set_target(found)
                    self.assignment_to_source_input_for.input_inventory.set_claimed(item_name,retrieved_amt)
                    # self.target = found
                    self.target_item_kind = item
                    self.set_state(ROLE_STATE.MOVE_TO_COLLECT_FOR_INPUT)
                else:
                    self.set_state(ROLE_STATE.FIND_FALLEN_PRODUCT)

            case ROLE_STATE.FIND_OUTPUT_COLLECT:
                # TODO: search eligible output structures for materials to retrieve and
                #   move to dedicated storage
                # logger.info('setting ROLE_STATE.TARGET_CHECK_DELAY')
                # logger.info("porter ROLE_STATE.FIND_OUTPUT_COLLECT UPDATE")
                found = self.find_output_to_clear()
                if found and self.target:
                    # logger.info(f"found storage target to clear; target is {self.target}")
                    self.set_state(ROLE_STATE.MOVE_TO_STORAGE)
                else:
                    # logger.info(f"did not find storage target to clear")
                    if not self.colonist.inventory.is_empty():
                        # did not find fallen product, so put stuff in storage
                        self.set_state(ROLE_STATE.FIND_STORAGE_DELIVER)
                        super().start_timer('target_check')
                    else:
                        self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)

            case ROLE_STATE.TAKE_FROM_STORAGE_FOR_INPUT:
                # take normally, then
                #   set target to assignment_to_source_input_for
                self.get_item_from_storage()
                self.set_target(self.assignment_to_source_input_for)
                self.colonist.set_target(self.assignment_to_source_input_for)
                # self.target = self.assignment_to_source_input_for
                # logger.info(f'new target in porter.update ROLE_STATE.TAKE_FROM_STORAGE_FOR_INPUT is {self.target.name}')
                self.set_state(ROLE_STATE.MOVE_TO_DELIVER_TO_INPUT)
                self.colonist.set_idle()

            case ROLE_STATE.ADD_TO_INPUT:
                if self.target.name != 'dummy':
                    # logger.info(f"adding {self.colonist.inventory} to input of {self.target.name}; target is {self.target}")
                    self.target.store(self.colonist.inventory.contents, "input")
                else:
                    pass
                    # logger.info(f"NOT adding {self.colonist.inventory} to input of {self.target.name}; target is {self.target}")

                self.set_target(None)
                # self.target = None
                self.target_item_kind = ''
                self.assignment_to_source_input_for = None
                self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
            case _:
                super().update()
                return

        self.common_update()
