from enum import Enum
import logging

import pygame

from entities.colonists.roles.archetypes.role_state import ROLE_STATE
from entities.colonists.target import Target
from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from util.debug import debug
from util.debug import log_stack
from util.events.event_queue import EventQueue


# import actions as needed for behavior
# from entities.colonists.actions.blah import Blah
# collection actions; put items in output of assignment structure
from entities.colonists.actions.find_fallen_product import FindFallenProduct
from entities.colonists.actions.find_output_retrieve import FindOutputRetrieve
from entities.colonists.actions.find_output_deliver import FindOutputDeliver
from entities.colonists.actions.gather_item import GatherItem
from entities.colonists.actions.retrieve_from_storage import RetrieveFromStorage

# harvest actions; find and harvest resource
from entities.colonists.actions.find_resource import FindResource
from entities.colonists.actions.harvest import Harvest


logger = logging.getLogger(__name__)


# role operations classes will handle target and behavior calculations
class Role:
    def __init__(self,colonist,name):
        self.colonist = colonist
        self.name = name

        # self.target = Target()
        self.target = None

        # cooldowns
        self.timer_defs = { # time is in ms
            'target_check':  {'flag': False,'last_time': 0,'cooldown': 500,},
            'target_update': {'flag': False,'last_time': 0,'cooldown': 500,},
        }
        self.target_check_ready = True
        self.target_update_ready = True
        self.assignment_to_source_input_for = None

        # self.target_check_time = 0
        # self.target_check_cooldown = 500 # ms
        # self.target_update_time = 0
        # self.target_update_cooldown = 500 # ms

        self.state = None
        self.state_history = []

        # logger.info('role initialized')

    def update(self):
        # order of operations is:
            # core action, production, harvest etc
            # find and add input processing items (producer, hauler)
            # find and store harvested item (harvester), or general storage (everyone else)
            # find buildable target

        match self.state:
            case ROLE_STATE.ADD_TO_INPUT:
                # logger.debug(f"adding {self.colonist.inventory} to input of {self.target.name};")
                self.target.store(self.colonist.inventory.contents, "input")
                self.set_target(None)
                # self.target = None
                self.target_item_kind = ''
                self.assignment_to_source_input_for = None
                self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
            case ROLE_STATE.FIND_FALLEN_PRODUCT:
                # NOTE: moving this here may break farmer or processor
                # used by all roles
                if self.colonist.inventory.has_room():
                    # find storage with capacity, get product name, target_product
                    # self.target_storage, target_product = self.
                    # TODO: Move this to role.py
                    new_target = FindFallenProduct.instance().execute(self.colonist)

                    if new_target:
                        self.set_target(new_target)
                        # self.target = new_target
                        self.set_state(ROLE_STATE.MOVE_TO_PRODUCT)
                        # logger.info(f"found new target {new_target}; setting ROLE_STATE.MOVE_TO_PRODUCT")
                    else:
                        # logger.info('no fallen product found, setting ROLE_STATE.FIND_OUTPUT_COLLECT')
                        self.set_state(ROLE_STATE.FIND_OUTPUT_COLLECT)
                self.start_timer('target_check')

                return
            case ROLE_STATE.HARVEST:
                complete = self.target.work()
                if complete:
                    self.harvest_resource()
                    self.target = None
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
            case ROLE_STATE.CRAFT_ITEM:
                complete = self.target.work()
                if complete:
                    self.colonist.assignment.process_work_complete()
                    self.set_target(None)
                    # self.target = None
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                    self.colonist.set_idle()
            case ROLE_STATE.GATHER_ITEM:
                # used by all roles
                # logger.info(f"triggering ROLE_STATE.GATHER_ITEM on {self.target}")
                complete = self.target.work()
                if complete:
                    # logger.info(f"work complete, gathering product: {self.target}")
                    result = self.gather_product()
                    if result:
                        # logger.info(f"gather result is {result}; inventory count is {self.colonist.inventory.contents[self.target.name]}")
                        self.set_target(None)
                        if self.colonist.inventory.has_room():
                            # logger.info(f"inventory has room: {self.colonist.inventory}")
                            self.set_state(ROLE_STATE.FIND_FALLEN_PRODUCT)
                        else:
                            self.set_state(ROLE_STATE.FIND_STORAGE_DELIVER)
                    self.colonist.set_idle()
            case ROLE_STATE.FIND_OUTPUT_COLLECT:
                """
                Find items needed for assignment to be workable.
                """
                self.find_output_collect()

                if not self.target or self.target.target_type == TARGET_TYPE.DUMMY:
                    # logger.info(f"ROLE_STATE.FIND_OUTPUT_COLLECT did not find a valid target")
                    self.colonist.assignment.unassign()
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                    self.colonist.set_idle()
                else:
                    self.set_state(ROLE_STATE.MOVE_TO_STORAGE)

                self.start_timer('target_check')
            case ROLE_STATE.FIND_STORAGE_DELIVER:
                # used by porter to deposit produced items
                # logger.info("updated FIND_STORAGE_DELIVER state")
                target = self.find_closest_storage()
                if target:
                    self.set_target(target)
                    # might need a new state, since MOVE_TO_STORAGE is for collecting item
                    self.set_state(ROLE_STATE.MOVE_TO_OUTPUT)
                else:
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                    self.colonist.set_idle()
            case ROLE_STATE.FIND_OUTPUT_DELIVER:
                # used by farmer, gatherer, processor to return produced items to assignment structure
                # logger.info("updated FIND_OUTPUT_DELIVER state")
                # if did not have room in output of assignment, reset the state
                if self.colonist.assignment.output_inventory.has_room():
                    self.set_target(self.colonist.assignment)
                    self.set_state(ROLE_STATE.MOVE_TO_OUTPUT)
                else:
                    self.colonist.drop_all_inventory()
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                    self.colonist.set_idle()
            case ROLE_STATE.ADD_TO_OUTPUT:
                if not self.colonist.check_target_distance():
                    # FIXME: hauler gets where when moving to input of workable station
                    #   they are not assigned to; unsure how ROLE_STATE.ADD_TO_OUTPUT is getting set
                    #   so just ignore it and return for now
                    # logger.info('Role.update(): ROLE_STATE.ADD_TO_OUTPUT: not close enough to target')
                    return
                # used by farmer, gatherer, processor
                self.target.store(self.colonist.inventory.contents)
                self.set_target(None)
                self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                self.colonist.set_idle()
            case ROLE_STATE.TAKE_FROM_STORAGE:
                # logger.info(f"{self.archetype_repr()} taking from storage; target: {self.target.name}; target_item_kind: {self.target_item_kind}")
                self.get_item_from_storage()
                self.set_target(None)
                self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                self.colonist.set_idle()
            case ROLE_STATE.BUILD:
                # used by all roles
                # logger.info(f"triggering ROLE_STATE.GATHER_ITEM on {self.target}")
                if self.target.can_build():
                    complete = self.target.build()
                    if complete:
                        self.colonist.assignment.finish_build()
                        self.set_target(None)
                        # self.target = None
                        self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
                        self.colonist.set_idle()
            case _:
                pass
                # logger.error(f"Role state update not defined for {self.state}")

        self.common_update()

    def set_state(self,new_state):
        # logger.info(f'{self.colonist.id}: {self.archetype_name}.set_state(): setting state from {self.state} to {new_state}')

        # log_stack()

        self.state = new_state

        self.track_state()
        # debug stuff
        # if self.archetype_name == 'Porter':
        #     filler = '0'
        #     ticks_string = f'{pygame.time.get_ticks():{filler}>6}'
        #     self.state_sequence.insert(0,f'{ticks_string} - {new_state}')
        #     if len(self.state_sequence)>20:
        #         self.state_sequence.pop()

    def track_state(self):
        pass
        # EventQueue.instance().publish('ADD_MESSAGE',[f'{pygame.time.get_ticks()}:{self.state}'])
        # EventQueue.instance().publish('ADD_MESSAGE',[f'{self.state}'])
        # self.state_history.append(f'{pygame.time.get_ticks()}:{self.state}')
        # if len(self.state_history) > 7:
        #     self.state_history.pop(0)
        # print(self.state_history)

    def common_update(self):
        self.cooldowns()
        self.update_target()

    def update_target(self):
        if self.target and not self.target_update_ready:
            # logger.info('target update not ready')
            return

        # logger.info("TARGET UPDATE IS READY FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")

        self.start_timer('target_update')

        # This will set a dummy target
        if not self.target:
            # logger.info("NO target setting dummy")
            self.set_target(Target())

    def trigger_target(self):
        if not self.target:
            return False
        # logger.info(f'{self.archetype_repr()} triggering target {self.target.name}')
        if self.target.name == 'dummy':
            # logger.info('that is a dummy, killing')
            self.target.kill()
            self.set_target(None)
            return True
        # logger.info(f"that's a real target")
        # why return False?
        return False

    def cooldowns(self):
        # IDK if this is better performance than just having separate vars for flag,time,delay,
        #   but it is more concise and simpler to define new timers.
        #   One strong argument FOR this approach is it's impossible to set the time and
        #       forget to also set the flag, as with the previous pattern.
        current_time = pygame.time.get_ticks()

        for k,v in self.timer_defs.items():
            v['flag'] = self.timer(current_time, **v)
            # logger.info(f'timer {k} in role is {v}')

        self.target_check_ready = self.timer_defs.get('target_check').get('flag')
        self.target_update_ready = self.timer_defs.get('target_update').get('flag')

    def timer(self,ticks,flag,last_time,cooldown):
        if self.target and self.target.name != 'dummy':
            return
        if not flag:
            if ticks - last_time >= cooldown:
                return True
        return False

    def start_timer(self,timer_name):
        target_timer = self.timer_defs.get(timer_name)

        target_timer['flag'] = False
        target_timer['last_time'] = pygame.time.get_ticks()

    def draw_debug(self):
        for index,ea in enumerate(self.state_sequence):
            debug(f'{ea}',y=25*(index+1))

    def find_resource(self):
        """
        Finds mature resource to work.
        """
        found_target = FindResource.instance().execute(self.colonist,self.name)
        if found_target:
            # don't want to set it if it's None; keep dummy target
            # logger.info(f"{self.archetype_repr()} FindResource found target {found_target}")
            self.set_target(found_target)
        else:
            pass
            # logger.info(f"FindResource did not find target")

    def harvest_resource(self):
        """
        Works target resource.

        Eventually will add default behavior to haulers to chop marked trees.
        """
        # target will get destroyed here if successfully harvested
        Harvest.instance().execute(self.target,self.name)
        if not self.target and self.colonist.inventory.has_room():
            # try to find product immediately after harvesting; keeps them from running way out of range
            self.set_state(ROLE_STATE.FIND_FALLEN_PRODUCT)

    def find_storage_product(self):
        """
        Finds fallen product that can be stored in this colonist's assignment output.
        """
        # logger.info(f'checking for fallen {self.colonist.assignment.storage_types}')
        found_target = FindFallenProduct.instance().execute(self.colonist,self.colonist.assignment.storage_types)
        if found_target:
            # logger.info(f"FindFallenProduct found a valid target {found_target}")
            self.set_target(found_target)
        else:
            pass
            # logger.info(f"FindFallenProduct did not find a valid target, checking for {self.colonist.assignment.storage_types}")

    def gather_product(self):
        """
        Gathers fallen product.
        """
        target_name = self.target.label
        # target will get destroyed here if successfully gathered
        gathered = GatherItem.instance().execute(self.target,self.colonist.inventory)
        # logger.info(f"RESULT of GatherItem execute {self.name}:{self.archetype_name}: gathered? {gathered}")
        if not gathered:
            # logger.info(f"{self.archetype_name} DID NOT gather target item")
            return False
        EventQueue.instance().publish('ADD_MESSAGE',[f'{self.name}:{self.archetype_name} collected a {target_name}'])
        return True

    def find_output_collect(self):
        needed = self.colonist.assignment.check_needed_input()
        # logger.info(f"{self.colonist} NEEDED ITEMS ARE {needed}")
        for key,value in needed.items():
            # find in storages
            # this needs to reserve the target output items
                # AND set the target structure input item reservation
                # AND give the colonist an amount to track like with set_target_item
            found,item_name,amt = FindOutputRetrieve.instance().execute(
                self.colonist,
                max(value,self.colonist.inventory.get_capacity()),
                key
            )
            if found:
                # logger.info(f"FOUND {found}:{item_name}:{amt}")
                self.colonist.assignment.input_inventory.set_claimed(item_name,amt)
                # assign storage target
                self.target_item_kind = key
                self.set_target(found)
                return

    def find_output_to_clear(self):
        # find any non-dedicated storage contents
        found,item_name,amt = FindOutputRetrieve.instance().execute(
            self.colonist,
            self.colonist.inventory.get_capacity()
        )
        if found:
            # logger.info(f"found target to clear {found.name}")
            # assign storage target
            self.target_item_kind = ""
            self.set_target(found)
        else:
            # logger.info("did not find target to clear")
            pass
        return found

    def find_closest_storage(self):
        return FindOutputDeliver.instance().execute(self.colonist,dedicated_storage=True)

    def get_item_from_storage(self):
        """
        Retrieves target item from storage and places it in colonist's inventory.
        """
        RetrieveFromStorage.instance().execute(
            self.colonist,
            self.target,
            self.target_item_kind
        )

    def trigger_working_state(self):
        if not self.target or not self.colonist.check_target_distance():
            self.colonist.set_idle()
            return

        if not self.colonist.check_target_distance():
            # logger.info(f"{self.archetype_repr()} NOT CLOSE ENOUGH TO TARGET {self.target.name}")
            self.colonist.set_idle()
            return

        # logger.info(f'Role.trigger_working_state; {self.archetype_repr()} target type: {self.target.target_type}; state: {self.state}')

        match self.target.target_type:
            case TARGET_TYPE.PRODUCT:
                if self.state != ROLE_STATE.GATHER_ITEM:
                    self.set_state(ROLE_STATE.GATHER_ITEM)
            case TARGET_TYPE.RESOURCE:
                if self.state != ROLE_STATE.HARVEST:
                    self.set_state(ROLE_STATE.HARVEST)
            case TARGET_TYPE.HARVEST:
                match self.state:
                    case ROLE_STATE.MOVE_TO_DELIVER_TO_INPUT:
                        self.set_state(ROLE_STATE.ADD_TO_INPUT)
                    case ROLE_STATE.MOVE_TO_BUILDABLE:
                        self.set_state(ROLE_STATE.BUILD)
            case TARGET_TYPE.STORAGE:
                # logger.info(f"matching state {self.state} on {self.target.target_type}")
                # log_stack()

                match self.state:
                    case ROLE_STATE.MOVE_TO_STORAGE:
                        self.set_state(ROLE_STATE.TAKE_FROM_STORAGE)
                    case ROLE_STATE.MOVE_TO_COLLECT_OUTPUT:
                        self.set_state(ROLE_STATE.TAKE_FROM_STORAGE)
                    case ROLE_STATE.MOVE_TO_COLLECT_FOR_INPUT:
                        # logger.info("setting state to ROLE_STATE.TAKE_FROM_STORAGE_FOR_INPUT")
                        self.set_state(ROLE_STATE.TAKE_FROM_STORAGE_FOR_INPUT)
                    case ROLE_STATE.MOVE_TO_DELIVER_TO_INPUT:
                        # logger.info("setting state to ROLE_STATE.ADD_TO_INPUT")
                        self.set_state(ROLE_STATE.ADD_TO_INPUT)
                    case ROLE_STATE.MOVE_TO_BUILDABLE:
                        self.set_state(ROLE_STATE.BUILD)
                    case ROLE_STATE.MOVE_TO_CRAFTABLE:
                        self.set_state(ROLE_STATE.CRAFT_ITEM)
                    case ROLE_STATE.ADD_TO_OUTPUT:
                        # logger.info(f"ADD_TO_OUTPUT, state case, doing nothing")
                        pass
                    case _:
                        # logger.info(f"triggering _ state case for {self.state}")
                        self.set_state(ROLE_STATE.ADD_TO_OUTPUT)
            case TARGET_TYPE.DUMMY:
                if self.target and self.target.target_type == TARGET_TYPE.DUMMY:
                    self.target.kill()
                    self.set_target(None)
                    self.colonist.set_idle()

                if self.state != ROLE_STATE.TARGET_CHECK_DELAY:
                    # logger.info('target was dummy in Role.trigger_working_state; setting ROLE_STATE.TARGET_CHECK_DELAY')
                    self.set_state(ROLE_STATE.TARGET_CHECK_DELAY)
            case TARGET_TYPE.GRID:
                # grid squares are currently only targeted for planting operations
                # logger.info("grid target in working state trigger")
                self.set_state(ROLE_STATE.PLANT_ITEM)

    def set_target(self,new_target):
        # logger.info(f"Role.set_target() {self.archetype_repr()} setting target from {self.target.name if self.target else None} to {new_target.name if new_target else None}")
        # log_stack()
        self.target = new_target
        self.colonist.set_target(self.target)

    def archetype_repr(self):
        return f'{self.archetype_name}:{self.name}'

    def state_history_repr(self):
        return ",".join(self.state_history)

    def __repr__(self):
        return self.name
