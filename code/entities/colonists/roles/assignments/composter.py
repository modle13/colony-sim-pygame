import sys

import logging

import pygame

from entities.colonists.roles.archetypes.processor import Processor
from settings import config
from util.events.event_queue import EventQueue


logger = logging.getLogger(__name__)


class Composter(Processor):
    """
    1. Finds input resources (compost)
    2. Retrieves input resources
    3. Processes input resources into output resources (fertilizer)
    """

    def __init__(self,colonist):
        super().__init__(colonist,'composter')

    def update(self):
        super().update()
