import logging

import pygame

from entities.colonists.roles.archetypes.farmer import Farmer
from settings import config


logger = logging.getLogger(__name__)


class WheatFarmer(Farmer):
    """
    1. Retrieves input items (fertilizer, wheat_germ)
    1. Creates wheat_seed (fertilizer + wheat_germ = wheat_seed)
    2. Finds plantable tile
    3. Plants wheat_seed
    """

    def __init__(self,colonist):
        super().__init__(colonist,'wheat_farmer')

    def update(self):
        super().update()
