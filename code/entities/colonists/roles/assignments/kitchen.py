import logging

import pygame

from entities.colonists.roles.archetypes.processor import Processor
from settings import config


logger = logging.getLogger(__name__)


class Kitchen(Processor):
    """
    1. Finds input resources (mushrooms, grain)
    2. Retrieves input resources
    3. Processes input resources into output resources (soup)
    """

    def __init__(self,colonist):
        super().__init__(colonist,'kitchen')

    def update(self):
        super().update()
