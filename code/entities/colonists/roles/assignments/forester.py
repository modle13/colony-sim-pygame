import logging

import pygame

from entities.colonists.roles.archetypes.farmer import Farmer
from settings import config


logger = logging.getLogger(__name__)


class Forester(Farmer):
    """
    1. Creates saplings
    2. Finds plantable tile
    3. Plants saplings
    ??? + compost = sapling
    """
    
    def __init__(self,colonist):
        super().__init__(colonist,'forester')

    def update(self):
        super().update()
