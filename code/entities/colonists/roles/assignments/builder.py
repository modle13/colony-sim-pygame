import logging

import pygame

from entities.colonists.roles.archetypes.architect import Architect
from settings import config


logger = logging.getLogger(__name__)


class Builder(Architect):
    """
    Builders are more specific Architects.
    """
    def __init__(self,colonist):
        super().__init__(colonist,'builder')

    def update(self):
        super().update()
