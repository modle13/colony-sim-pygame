import logging

import pygame

from entities.colonists.roles.archetypes.processor import Processor
from settings import config


logger = logging.getLogger(__name__)


class TradingPost(Processor):
    """
    1. Finds input resources (filtered item target)
    2. Retrieves input resources
    3. Processes input resources into output resources (coins)
    """

    def __init__(self,colonist):
        super().__init__(colonist,'trader')

    def update(self):
        super().update()
