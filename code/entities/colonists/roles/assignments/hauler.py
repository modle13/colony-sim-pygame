import logging

import pygame

from entities.colonists.roles.archetypes.porter import Porter
from settings import config


logger = logging.getLogger(__name__)


class Hauler(Porter):
    """
    Haulers are more specific Porters, can have target item type filtered.

    1. Finds items on the ground
    2. Picks up items from ground
    3. Delivers items to storage
    4. Retrieves items from outputs
    5. Delivers items to inputs (from storage or outputs)
    """
    def __init__(self,colonist):
        super().__init__(colonist,'hauler')

    def update(self):
        super().update()
