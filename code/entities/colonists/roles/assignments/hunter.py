import logging

import pygame

from entities.colonists.roles.archetypes.gatherer import Gatherer
from settings import config


logger = logging.getLogger(__name__)


class Hunter(Gatherer):
    """
    1. Finds target resource (animal)
    2. Moves to target resource
    3. Harvests target resource (pew pew)
    """

    def __init__(self,colonist):
        super().__init__(colonist,'hunter')

    def update(self):
        super().update()
