import logging

import pygame

from entities.colonists.roles.archetypes.gatherer import Gatherer
from settings import config


logger = logging.getLogger(__name__)


class Forager(Gatherer):
    """
    1. Finds target resource (mushrooms)
    2. Moves to target resource
    3. Harvests target resource
    """

    def __init__(self,colonist):
        super().__init__(colonist,'forager')

    def update(self):
        super().update()
