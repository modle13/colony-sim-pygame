import logging

import pygame

from entities.colonists.roles.archetypes.gatherer import Gatherer
from settings import config


logger = logging.getLogger(__name__)


class Thresher(Gatherer):
    """
    1. Finds target resource (mature wheat)
    2. Moves to target resource
    3. Harvests target resource
    """

    def __init__(self,colonist):
        super().__init__(colonist,'thresher')

    def update(self):
        super().update()
