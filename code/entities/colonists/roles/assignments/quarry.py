import logging

import pygame

from entities.colonists.roles.archetypes.processor import Processor
from settings import config


logger = logging.getLogger(__name__)


class Quarry(Processor):
    """
    1. Finds target resource (stone)
    2. Moves to target resource
    3. Harvests target resource
    """

    def __init__(self,colonist):
        super().__init__(colonist,'quarry')

    def update(self):
        super().update()
