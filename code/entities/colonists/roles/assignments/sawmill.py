import logging

import pygame

from entities.colonists.roles.archetypes.processor import Processor
from settings import config


logger = logging.getLogger(__name__)


class Sawmill(Processor):
    """
    1. Finds input resources (log)
    2. Retrieves input resources
    3. Processes input resources into output resources (planks, sawdust)
    """

    def __init__(self,colonist):
        super().__init__(colonist,'sawmill')

    def update(self):
        super().update()
