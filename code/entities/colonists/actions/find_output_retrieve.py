import logging

from entities.colonists import movement
from entities.colonists.actions.action import Action
from settings import config
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindOutputRetrieve(Action):
    """
    Finds an output/storage with a given item.

    Similar to find_output_deliver, but in reverse.
    """

    def __init__(self):
        super().__init__()

    def execute(self,source,amt,target_item=None):
        super().execute()
        # TODO: these return params should probably be a struct
        if target_item:
            return self.get_nearest_storage(source,amt,target_item)
        else:
            return self.get_nearest_storage_with_any(source,amt)

    def get_nearest_storage(self,source,amt,target_item):
        # logger.info(f"{source} GETTING NEAREST STORAGE for {target_item}:{amt}")
        eligible_targets = source.colony.get_storage_structures()

        # AT LEAST 1
        eligible_targets = list(filter(
            lambda x: x.output_inventory.has_amount_of_kind(target_item,1) and x.is_ready(),
            source.colony.get_storage_structures()
        ))

        # logger.info(f'{source.role.archetype_name}:{source.role.name} storage structures are:')
        # for ea in config.game.storage_structures:
        #     logger.info(f'{ea.name}; dedicated: {ea.dedicated_storage}; inv empty: {ea.output_inventory.is_empty()}')

        # logger.info(f'{source.role.archetype_name}:{source.role.name} elegible targets are:')
        # for ea in eligible_targets:
        #     logger.info(f'{ea.name}')

        if not eligible_targets:
            return None, None, None

        # logger.info(f'storage structures are {source.colony.get_storage_structures()}')
        # logger.info(f'filtered storages are {eligible_targets}')
        target = movement.find_closest_sprite(source, eligible_targets)
        claimed_amt = target.output_inventory.claim_inventory(target_item,amt)
        source.inventory.set_claimed(target_item,claimed_amt)

        return target, target_item, claimed_amt

    def get_nearest_storage_with_any(self,source,amt):
        # logger.info(f"GETTING NEAREST OUTPUT STORAGE with any item")

        # filters storages that are not dedicated; i.e. a production storage that may have
        # produced items in its output that need moved to dedicated storage
        eligible_targets = list(filter(
            lambda x:
                not x.dedicated_storage and
                not x.output_inventory.is_empty() and
                x.output_has_finished_product(),
            source.colony.get_storage_structures()
        ))

        # logger.info(f'{source.role.archetype_name}:{source.role.name} storage structures are:')
        # for ea in source.colony.get_storage_structures():
        #     logger.info(f'{ea.name}; dedicated: {ea.dedicated_storage}; inv empty: {ea.output_inventory.is_empty()}')

        # logger.info(f'{source.role.archetype_name}:{source.role.name} elegible targets are:')
        # for ea in eligible_targets:
        #     logger.info(f'{ea.name}')

        if not eligible_targets:
            return None, None, None

        # logger.info(f'storage structures are {config.game.storage_structures}')
        # logger.info(f'filtered storages are {eligible_targets}')
        target = movement.find_closest_sprite(source, eligible_targets)
        claimed_name, claimed_amt = target.output_inventory.claim_any_inventory(amt)
        source.inventory.set_claimed(claimed_name,claimed_amt)

        return target, claimed_name, claimed_amt
