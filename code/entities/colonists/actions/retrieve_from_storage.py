import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class RetrieveFromStorage(Action):
    """
    Retrieves an item from storage in proximity.
    """

    def __init__(self):
        super().__init__()

    def execute(self,colonist,target,kind):
        super().execute()
        self.retrieve_from_storage(colonist,target,kind)

    def retrieve_from_storage(self,colonist,target,kind):
        capacity = colonist.inventory.get_capacity()

        if capacity <= 0:
            # logger.info("colonist already has a full inventory, not retrieving")
            return

        amount = 0

        # porters would not have an assignment
        # only collect up to amount target assignment can hold
        if colonist.assignment and kind in colonist.assignment.input_inventory.get_kinds():
            amount = colonist.assignment.input_inventory.get_capacity(kind)

        input_name = colonist.assignment.name if colonist.assignment else ''
        input_name = input_name or colonist.role.assignment_to_source_input_for.name \
            if colonist.role.assignment_to_source_input_for else 'NONE'

        # logger.info(f"{input_name} input capacity is {amount}; colonist inventory capacity is {capacity}")

        amount = amount if amount > 0 and amount < capacity else capacity

        # logger.info(f'{colonist} retrieving {amount} of {kind} from {target.name}')

        if kind:
            retrieved = target.retrieve_specific(kind, amount, colonist.inventory.claimed)
        else:
            retrieved = target.retrieve_any(amount)

        for key,value in retrieved.items():
            target.output_inventory.remove_claim(key,value)

        # logger.info(f'{colonist} retrieved {retrieved} of {kind} from {target.name}')

        if retrieved:
            remaining_to_add = colonist.inventory.add_all(retrieved,allow_new_key=True)
            # logger.info(f"attempted add of {retrieved} {kind} to colonist inventory; remaining to add {remaining_to_add}")
