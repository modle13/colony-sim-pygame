import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class ReachTarget(Action):
    """
    Triggers when a colonist has reached the given target.
    """

    def __init__(self):
        super().__init__()

    def execute(self):
        super().execute()
        self.reach_target()

    def reach_target(self):
        # do target collision stuff?

        # to be handled by colonist? or via event
        # self.handle_work_complete()
        pass