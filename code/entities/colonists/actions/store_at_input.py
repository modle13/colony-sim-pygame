import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class StoreAtInput(Action):
    """
    Stores an item in input in proximity.
    """

    def __init__(self):
        super().__init__()

    def execute(self):
        super().execute()
        self.add_to_input_inventory(inventory,target)

    def add_to_input_inventory(self,inventory,target):
        for kind, amount in inventory.contents.items():
            if amount:
                added = target.add_to_input_inventory(kind, amount)
                if added:
                    inventory.remove(kind, amount)
                else:
                    # see issue #83 for details (pre-colonist-refactor issue)
                    # https://gitlab.com/modle13/colony-sim-pygame/-/issues/83
                    # something went wrong, so just give up and try something else
                    # possible outcome is target does not have input inventory
                    # this seems to trigger about once every 5 minutes on average
                    #     with a single wheat_farmer
                    logger.info(
                        f'triggering state reset due to failure to add to input inventory: '\
                        f'{amount} of {kind} to {target.label}'
                    )
                    return False
                    # self.trigger_state_reset()

        return True
