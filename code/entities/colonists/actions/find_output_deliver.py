import logging

from entities.colonists import movement
from entities.colonists.actions.action import Action
from settings import config
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindOutputDeliver(Action):
    """
    Finds an output to deliver a target item to.

    e.g. a woodcutter has a log to deliver to the woodcutter output inventory.

    Similar to find_output_retrieve, but in reverse.
    """

    def __init__(self):
        super().__init__()

    def execute(self,source,target_item=None,dedicated_storage=False):
        super().execute()
        return self.get_nearest_storage(source,target_item)

    def get_nearest_storage(self,source,target_item=None,dedicated_storage=False):
        # this is for use in finding ANY storage that will take the target item
        # other cases such as `woodcutter`, which will only store at THEIR `assignment` structure
        # just need to reference their `assignment` propery to find their storage target

        eligible_targets = source.colony.get_storage_structures()
        # logger.info(f"target item is {target_item}")
        # logger.info(f"storage structures are {config.game.storage_structures}")
        if target_item:
        # turn this config.game.storage_structures check into an event
            eligible_targets = list(filter(
                lambda x: x.output_inventory.has_room(target_item) and x.is_ready(),
                source.colony.get_storage_structures()
            ))
        else:
            eligible_targets = list(filter(
                lambda x: x.dedicated_storage and x.is_ready(),
                source.colony.get_storage_structures()
            ))

        if not eligible_targets:
            # logger.info("no eligible storage targets")
            return None

        # TODO: maybe add pending incoming resources similar to pending to remove resources
        target = movement.find_closest_sprite(source, eligible_targets)
        return target
