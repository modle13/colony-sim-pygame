import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class StoreAtStorage(Action):
    """
    Stores an item in storage in proximity.
    """

    def __init__(self):
        super().__init__()

    def execute(self,target,inventory):
        super().execute()
        self.deposit_in_storage(target,inventory)

    def deposit_in_storage(self,target,inventory):
        # adding to storage
        target.store(inventory.contents)
        # drop anything left
        #   next cycle will determine if it should be picked up again
        #   if there is available storage

        # will be handled in colonist logic
        # self.drop_all_inventory()
        # self.handle_work_complete()