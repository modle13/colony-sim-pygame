import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindPlantable(Action):
    """
    Finds a plantable tile of a given type.
    """

    def __init__(self):
        super().__init__()

    def execute(self,assigned_structure):
        super().execute()
        self.get_random_cultivable(assigned_structure)

    def get_random_cultivable(self,assignment):
        unpopulated_squares = assignment.region.get_unpopulated_tiles()
        filtered_targets = list(filter(
            lambda x: self.in_assignment_range(x,assignment),
            unpopulated_squares
        ))
        found = assignment.region.get_unpopulated_tiles.get_random_grid_square(squares=filtered_targets)
        return found

    def in_assignment_range(self, assignment, target):
        # default to True, so non-restricted roles like hauler don't have a range limitation
        in_range = True
        if assignment and assignment.influence.range > 1:
            in_range = assignment.influence.rect.colliderect(target.rect)
        return in_range
