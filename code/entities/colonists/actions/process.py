import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class Process(Action):
    """
    Processes a target workable building.
    """

    def __init__(self):
        super().__init__()

    def execute(self):
        super().execute()
        self.complete_processing()

    def complete_processing(self):
        # do processing stuff?

        # to be handled by colonist? or via event
        # self.handle_work_complete()
        pass