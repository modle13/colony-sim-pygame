import logging

from entities.colonists import movement
from entities.colonists.actions.action import Action
from settings import config
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindFallenProduct(Action):
    """
    Finds fallen products to be collected.

    This will primarily be a hauler, but can also be:
    - an idle role related to the target item
        - e.g. a Woodcutter with items a Woodcutter produces and stores at the Woodcutter structure.
    - a processor role that is missing input inventory for target process
        - e.g. a Cook lacking mushrooms
    """

    def __init__(self):
        super().__init__()

    def execute(self,source,target_products=[]):
        super().execute()
        return self.get_target(source,target_products)

    def get_target(self,source,target_products=[]):
        # logger.info(f'there are this many products {source.colony.get_products()}')
        # for entry in source.colony.get_products():
            # logger.info(f'checking {entry.name} against target products {target_products}')

        eligible_targets = []

        if target_products:
            # needs to be unclaimed, in structure range, and in target product list
            eligible_targets = list(
                filter(
                    lambda x: not x.claimed and source.in_structure_range(x) and x.name in target_products,
                    source.colony.get_products()
                )
            )
        else:
            # just needs to be unclaimed
            eligible_targets = list(
                filter(
                    lambda x: not x.claimed, source.colony.get_products()
                )
            )

        valid_target = movement.find_closest_sprite(source, eligible_targets)

        if valid_target:
            valid_target.claimed = True

        return valid_target
