import logging

from entities.colonists import movement
from entities.colonists.actions.action import Action
from settings import config
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindResource(Action):
    """
    Finds a ready resource of a given type.
    """

    def __init__(self):
        super().__init__()

    def execute(self,source,target_type):
        super().execute()
        return self.get_target(source,target_type)

    def get_target(self,source,target_type):
        # turn this config.active_targets check into an event
        targets = source.colony.get_active_targets(target_type)
        # logger.info(f'active targets for {target_type} are {targets}')

        filtered_targets = list(filter(
            lambda x: not x.claimed and not x.expired and source.in_structure_range(x),
            targets
        ))
        # logger.info(f'filtered targets are {filtered_targets}')

        valid_target = movement.find_closest_sprite(source, filtered_targets)

        # logger.info(f'valid target is {valid_target}')

        return valid_target
