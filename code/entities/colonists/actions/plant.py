import logging

from entities.colonists.actions.action import Action
from settings import config
from util.events.event_queue import EventQueue
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class Plant(Action):
    """
    Plants in a target workable tile.
    """

    def __init__(self):
        super().__init__()

    def execute(self,role,assignment,target,inventory):
        super().execute()
        self.plant_item(role,assignment,target,inventory)

    def plant_item(self,role,assignment,target,inventory):
        # creating resource spawn event
        resource_id = assignment.cultivate_data.get('event_id')
        EventQueue.instance().publish("ADD_RESOURCE",[resource_id,False,target.rect.center])

        # post event to event queue
        name = assignment.cultivate_data.get('name')
        inventory.remove(name, 1)
        EventQueue.instance().publish('ADD_MESSAGE',[f'{role} planted a {name}'])

        # to be handled by colonist? or via event
        # self.handle_work_complete()
