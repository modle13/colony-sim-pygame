import logging

from entities.colonists import movement
from entities.colonists.actions.action import Action
from settings import config
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindInputDeliver(Action):
    """
    Finds an input to deliver a target item to.

    e.g. a hauler/cook has a mushroom to deliver to the kitchen.
    """

    def __init__(self):
        super().__init__()

    def execute(self,colonist,structure=None):
        super().execute()
        return self.get_nearest_input_deliver_item_needed(colonist,structure=None)

    def get_nearest_input_deliver_item_needed(self,colonist,structure=None):
        """
        Returns target structure and item needed by target structure.
        """
        # this will change based on whether a structure is passed
        target = structure
        target_item = ''

        # if no structure is passed, find the nearest one with missing/low inputs
        if not target:
            # TODO: turn this config.game.structures check into an event
            # logger.info(f'original structures list is {colonist.colony.get_structures()}')
            filtered_structures = list(filter(
                lambda x: hasattr(x,'input_inventory') and x.input_inventory.has_room() and x.check_needed_input(),
                colonist.colony.get_structures()
            ))
            # logger.info(f'filtered structures are {filtered_structures}')
            if not filtered_structures:
                # logger.info("no filtered structures found")
                return None, None, 0
            target = movement.find_closest_sprite(colonist, filtered_structures)

        needed_to_make = 0

        if target:
            # get least amount item needed by consumes
            target_item = ''
            least_amount = 999
            for key, value in target.consumes.items():
                count = target.input_inventory.get_available_count(kind=key)
                if count < least_amount:
                    least_amount = count
                    target_item = key

            needed_to_make = target.check_needed_input(item_name=target_item).get(target_item,0)

        # logger.info(f'{colonist}; target is {target}; target_item is {target_item}; needed to make is {needed_to_make}')

        return target, target_item, needed_to_make
