import logging

from entities.colonists.actions.action import Action
from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from util.events.event_queue import EventQueue
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class Harvest(Action):
    """
    Harvests a target workable tile.
    """

    def __init__(self):
        super().__init__()

    def execute(self,target,role):
        super().execute()
        self.remove_resource(target,role)

    def remove_resource(self,target,role):
        if target.target_type != TARGET_TYPE.DUMMY:
            EventQueue.instance().publish('ADD_MESSAGE',[f'{role} wrecked a {target.label}'])
        target.destroy()
