import logging

from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class GatherItem(Action):
    """
    Gathers fallen items.
    """

    def __init__(self):
        super().__init__()

    def execute(self,target,inventory):
        super().execute()
        return self.retrieve_fallen_product(target,inventory)

    def retrieve_fallen_product(self,target,inventory):
        # picking up product
        # logger.info(f"attempting to add {target.product_type} to inventory {inventory}")
        added = inventory.add(target.product_type, 1, allow_new_key=True)

        if added:
            # logger.info(f"added {target.product_type} to inventory {inventory}")
            target.destroy()
            return True
        else:
            # logger.info(f"DID NOT ADD {target.product_type} to inventory {inventory}")
            target.claimed = False
            return False
