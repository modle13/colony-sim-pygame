import logging

from entities.colonists import movement
from entities.colonists.actions.action import Action
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class FindStorageWithPlantable(Action):
    """
    Finds a storage with item of a given type and returns amt found if any.
    """

    def __init__(self):
        super().__init__()

    def execute(self,source,amt,target_item):
        super().execute()
        return self.get_target(source,amt,target_item)

    def get_target(self,source,amt,target_item):
        # logger.info(f"searching for plantable item {target_item}:{amt}")
        filtered_storages = list(filter(lambda x: x.check_inventory_for(target_item), source.colony.get_storage_structures()))

        target = None
        found_item = None

        if filtered_storages:
            target = movement.find_closest_sprite(source, filtered_storages)

        if target:
            claimed_amt = target.output_inventory.claim_inventory(target_item,amt)
            source.inventory.set_claimed(target_item,claimed_amt)
            return target,target_item,claimed_amt

        return None,None,None
