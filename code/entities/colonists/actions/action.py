import logging


logger = logging.getLogger(__name__)


# do these need to be separate classes? grouped by type/operation? idk
# see how many imports/properties need to be shared
# if many imports and not much sharing, keep separate classes


class Action:
    def __init__(self):
        pass

    def execute(self):
        pass
