#!/usr/bin/env python

from collections import Counter
import logging
import os
import random
import sys
import traceback

import pygame
#import pygame_menu

from entities.colonists.target_enum import TARGET_TYPE
from settings import config
from state.game_state_manager import GameStateManager
from state.game_state_enum import GAME_STATE
from structures.placement_object import PlacementObject
from ui.crt import CRT
from ui.menu import Menu
from ui.reticle import Reticle
from ui.screens.screen_manager import ScreenManager
from util import colors
from util import keys
from util.camera import Camera
from util.debug import debug
from util.events.event_manager import EventManager
from util.events.event_queue import EventQueue
from util.events.flow_events import FlowEvents
from util.events.hotkey_events import HotkeyEvents
from util.input_manager import InputManager
from world.world import World


logging.basicConfig(level=(logging.DEBUG if config.debug else logging.INFO),
    format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S')

logger = logging.getLogger(__name__)


class Game():
    def __init__(self):
        if getattr(sys, 'frozen', False):
            # pyinstaller frozen path
            # https://pyinstaller.readthedocs.io/en/latest/runtime-information.html#using-file
            self.HERE = sys._MEIPASS
        else:
            self.HERE = os.path.dirname(__file__)

        # Setup the clock for a decent framerate
        self.clock = pygame.time.Clock()
        self.ticks = 0
        self.tick_start_time = 0
        self.tick_end_time = 0
        self.frame_tick_time = 0
        self.min_frame_tick_time = 0
        self.max_frame_tick_time = 0
        self.avg_frame_tick_time = 0

        # Create the screen object
        self.screen_size = (config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
        self.screen = pygame.display.set_mode(self.screen_size)
        self.game_surf = pygame.Surface((config.SCREEN_WIDTH,config.SCREEN_HEIGHT))
        self.scale = 1

        # Create groups to hold sprites
        # - all_sprites is used for rendering
        # move this stuff to level class
        self.all_sprites = pygame.sprite.Group()
        self.game_objects = pygame.sprite.Group()
        # self.colonists = pygame.sprite.Group()

        # for click detection on menu elements; used by events.py (move it to events.py? or screen_manager.py?)
        self.menu_sprites = pygame.sprite.Group()


        # set up game state
        # TODO: make separate class for state management: StateManager
        self.show_grid = False
        self.placement_object = None
        EventQueue.instance().publish("SET_GAME_MODE",['COLONISTS_ACTIVE',True])

        # generate initial sprites and other objects
        # TODO: handle grid in separate class

        self.reticle = Reticle()
        # TODO: make camera a Singleton
        config.camera = Camera(config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2)

        # TODO: move inventory to individual Colony instances
        # self.inventory = Inventory(limit_type='unlimited', kinds=config.storage_items)

        # set up crt grid
        # TODO: use separate class
        tv_sprite = pygame.image.load(f'../sprite/tv.png').convert_alpha()
        crt_sprite = pygame.image.load(f'../sprite/green_gradient.png').convert_alpha()
        self.crt = CRT(config.SCREEN_WIDTH, config.SCREEN_HEIGHT, tv_sprite, crt_sprite)

        self.visible_area = pygame.Rect(0,0,0,0)
        FlowEvents.instance()
        PlacementObject.instance()
        HotkeyEvents.instance()
        self.subscribe_to_events()

    def set_difficulty(self, *args):
        print('difficulty set', *args)

    def run(self):
        # Main loop
        while self.running:
            try:
                self.run_main_loop()
            except Exception:
                logger.error(traceback.format_exc())
        self.quit()

    def run_main_loop(self):
        # logger.info(f"{GameStateManager.instance().ticks} frame START")
        self.tick_start_time = pygame.time.get_ticks()

        # update state
        self.update()

        # draw sprites
        self.draw()
        # logger.info(f"{GameStateManager.instance().ticks} frame END")

        # update the frame
        self.update_frame()

    def update(self):
        # logger.debug(f"GameStateManager.instance().state is {GameStateManager.instance().state}")
        # logger.debug("calling Game.update in main.py")
        EventManager.instance().update()
        InputManager.instance().update()
        HotkeyEvents.instance().update()
        GameStateManager.instance().update()
        # update keys collection
        # self.pressed_keys = pygame.key.get_pressed()

        # update events
        # self.events.update()
        # logger.debug("calling EventQueue instance update from main")
        EventQueue.instance().update()

        # update ui elements
        # if not GameStateManager.instance().is_running():
        #     self.menu.mainloop(self.screen)

        # print(GameStateManager.instance().state)

        match GameStateManager.instance().state:
            case GAME_STATE.INIT:
                pass
            case GAME_STATE.GAMEPLAY:
                self.update_gameplay_elements()
                # logger.info("updating world")
                World.instance().update()
            case GAME_STATE.GAME_PAUSED:
                pass

    def update_gameplay_elements(self):
        # update camera elements
        self.reticle.update()
        x,y = self.reticle.position_rect.center
        self.visible_area = pygame.Rect(
            x - config.HALF_WIDTH,
            y - config.HALF_HEIGHT,
            config.SCREEN_WIDTH,# - 100, # to test sprite draw area
            config.SCREEN_HEIGHT,# - 100, # to test sprite draw area
        )
        config.camera.update(self.reticle.position_rect)

        # update ui elements
        self.update_cursor()

    def draw(self):
        # Fill the screen with black
        self.game_surf.fill(colors.BLACK)
        # debug(f'{GameStateManager.instance().state} : {pygame.time.get_ticks() - GameStateManager.instance().state_time}', y=-1)

        # Draw title and return if show_title
        if not self.started:
            return

        ## DRAW ALL THE SPRITES

        self.draw_gameplay_sprites()

        ## DRAW THE UI

        # do UI draws after the scaled sprite draws
        self.draw_ui_sprites()

    def draw_gameplay_sprites(self):
        if not GameStateManager.instance().is_running():
            return

        ## DRAW ALL THE SPRITES

        World.instance().draw()

        # Draw the grid
        if self.show_grid:
            self.crt.draw(self.game_surf, 5)

        ## SCALE EVERYTHING

        self.game_surf = pygame.transform.scale(
            self.game_surf,
            (self.screen_size[0]*self.scale,self.screen_size[1]*self.scale),
        )

        self.screen.blit(pygame.transform.scale(self.game_surf,self.screen_size),(0,0))

    def draw_ui_sprites(self):
        self.screen.blit(self.reticle.surf,self.reticle.rect)
        GameStateManager.instance().draw()
        ScreenManager.instance().update()

    def blit(self, sprite, follow_camera=True):
        if not sprite.show:
            return
        if hasattr(sprite, 'draw'):
            sprite.draw(self.game_surf, camera=(config.camera if follow_camera else None))
        if follow_camera:
            if sprite.rect.colliderect(self.visible_area):
                self.game_surf.blit(sprite.surf, config.camera.apply(sprite))
        else:
            self.game_surf.blit(sprite.surf, sprite.rect)

    def update_frame(self):
        # track tick time
        self.handle_tick_data()
        # logger.info('GAME TICK PROCESSED')

        # make the updated display frame visible
        pygame.display.flip()

        # reset the frame tick timer (milliseconds)
        self.clock.tick(30)

    def handle_tick_data(self):
        self.ticks += 1
        GameStateManager.instance().ticks = self.ticks
        self.tick_end_time = pygame.time.get_ticks()
        self.frame_tick_time = self.tick_end_time - self.tick_start_time
        self.max_frame_tick_time = max(self.max_frame_tick_time, self.frame_tick_time)

        if self.ticks == 1:
            self.avg_frame_tick_time = self.frame_tick_time
        else:
            self.avg_frame_tick_time = ((self.avg_frame_tick_time*(self.ticks - 1)) + self.frame_tick_time)/self.ticks

        debug(f'ticks: {self.ticks}',align='topleft',y=50)
        debug(f'frame time: {self.frame_tick_time} ms',align='topleft',y=70)
        debug(f'avg frame time: {self.avg_frame_tick_time:.2f} ms',align='topleft',y=90)
        debug(f'max frame time: {self.max_frame_tick_time} ms',align='topleft',y=110)
        debug(f'scale: {self.scale}',align='topleft',y=130)
        debug(f'viewport: {config.camera.position}',align='topleft',y=150)
    def start(self):
        #self.menu.disable()
        #self.started = True
        self.paused = False
        self.started = True

    def quit(self):
        pygame.mixer.music.stop()
        pygame.mixer.quit()

    def update_cursor(self):
        mouse_visible = False if self.placement_object else True
        pygame.mouse.set_visible(mouse_visible)

    def set_scale(self,new_scale):
        self.scale = new_scale

    def exit_application(self):
        logger.info("exit_application() called in Game")
        pygame.quit()
        sys.exit()

    def subscribe_to_events(self):
        EventQueue.instance().subscribe("SCALE_WITH_ZOOM",self.set_scale)
        EventQueue.instance().subscribe('EXIT_GAME_TO_DESKTOP',self.exit_application)


def init_game():
    # Initialize pygame
    # game = Game.instance()
    game = Game()

    # for use in other modules
    config.game = game

    # Variable to keep the main loop running
    game.running = True
    game.started = False
    game.start()
    return game


if __name__ == '__main__':
    game = init_game()
    logger.info('game initialized')
    game.run()
