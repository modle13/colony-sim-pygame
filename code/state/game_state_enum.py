from enum import Enum


class GAME_STATE(Enum):
    INIT = 0
    LOADING = 1
    MENUS = 2
    GAMEPLAY = 3
    SIMULATION_PAUSED = 4
    GAME_PAUSED = 5
