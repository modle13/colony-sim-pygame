from enum import Enum
import logging

import pygame

from settings import config
from state.game_state_enum import GAME_STATE

from ui.ui_sprites_manager import UISpritesManager
from util.debug import debug
from util.events.event_queue import EventQueue
from util.events.flow_events_enum import FLOW_EVENTS
from util.input_manager import InputManager
from util.singleton import Singleton


logger = logging.getLogger(__name__)


@Singleton
class GameStateManager:
    def __init__(self):
        self.state = GAME_STATE.INIT

        # TODO: these need to be moved from main.Game
        self.show_grid = False
        self.placement_mode_on = False
        # self.placement_object = None

        self.ticks = 0

        self.active_panel = None
        self.title = "Game State"

        self.state_time = pygame.time.get_ticks()
        self.init_cooldown = 500
        self.init_ready = False
        self.loading_cooldown = 1000
        self.loaded = False

        self.subscribe_to_events()

    def set_state(self,new_state):
        logger.info(f"setting game state from {self.state} to {new_state}")
        old_state = self.state
        self.state = new_state
        self.state_time = pygame.time.get_ticks()

        match self.state:
            case GAME_STATE.INIT:
                pass
            case GAME_STATE.LOADING:
                EventQueue.instance().publish('OPEN_SCREEN',['loading_screen'])
            case GAME_STATE.MENUS:
                pass
                # EventQueue.instance().publish('OPEN_SCREEN',['init_screen'])
            case GAME_STATE.GAMEPLAY:
                if old_state == GAME_STATE.INIT:
                    EventQueue.instance().publish("GAME_INITIALIZED")
                    # EventQueue.instance().publish('OPEN_SCREEN',['gameplay_screen'])
                elif old_state == GAME_STATE.SIMULATION_PAUSED:
                    EventQueue.instance().publish('ADD_MESSAGE',['resuming simulation'])
                    logger.info("resuming simulation")
                elif old_state == GAME_STATE.GAME_PAUSED:
                    EventQueue.instance().publish('OPEN_SCREEN',['gameplay_screen'])
                    logger.info("unpausing game")
            case GAME_STATE.SIMULATION_PAUSED:
                EventQueue.instance().publish('ADD_MESSAGE',['pausing simulation'])
                logger.info("pausing simulation")
            case GAME_STATE.GAME_PAUSED:
                self.placement_mode_on = False
                self.show_grid = False
                EventQueue.instance().publish('OPEN_SCREEN',['pause_screen'])

    def update(self):
        self.cooldowns()

        match self.state:
            case GAME_STATE.INIT:
                if self.init_ready:
                    logger.debug('init ready, setting state to running')
                    self.set_state(GAME_STATE.LOADING)
            case GAME_STATE.LOADING:
                if self.loaded:
                    logger.debug('init ready, setting state to running')
                    EventQueue.instance().publish("FLOW_EVENT",[FLOW_EVENTS.SHOW_INIT])
                    # self.set_state(GAME_STATE.MENUS)
            case GAME_STATE.GAMEPLAY:
                if InputManager.instance().get_escape():
                    self.handle_escape()
                if InputManager.instance().get_lmb_down():
                    self.handle_left_click()
                if InputManager.instance().get_space_down():
                    self.set_state(GAME_STATE.SIMULATION_PAUSED)
            case GAME_STATE.SIMULATION_PAUSED:
                if InputManager.instance().get_escape():
                    self.handle_escape()
                if InputManager.instance().get_lmb_down():
                    self.handle_left_click()
                if InputManager.instance().get_space_down():
                    self.set_state(GAME_STATE.GAMEPLAY)
            case GAME_STATE.GAME_PAUSED:
                if InputManager.instance().get_escape():
                    self.handle_escape()

    def cooldowns(self):
        current_time = pygame.time.get_ticks()
        match self.state:
            case GAME_STATE.INIT:
                if current_time - self.state_time >= self.init_cooldown:
                    self.init_ready = True
            case GAME_STATE.LOADING:
                if current_time - self.state_time >= self.loading_cooldown:
                    self.loaded = True

    def draw(self):
        debug(f'{self.state} : {pygame.time.get_ticks() - self.state_time}')

    def handle_escape(self):
        logger.debug("triggered handle_escape")
        # if selection mode, cancel that
        # elif paused, unpause
        # elif unpaused, pause

        # close screens first

        had_active_element = UISpritesManager.instance().hide_sprite_context_panes()
        # reset placement if screens not open (maybe inverse this ordering)
        had_active_element = had_active_element or self.reset_placement_mode()

        if not had_active_element:
            if self.state == GAME_STATE.GAMEPLAY:
                EventQueue.instance().publish("HIDE_CONTEXT_PANES",[False])
                self.set_state(GAME_STATE.GAME_PAUSED)
            elif self.state == GAME_STATE.GAME_PAUSED:
                self.set_state(GAME_STATE.GAMEPLAY)

    def reset_placement_mode(self):
        deactivated = False
        logger.info(f"placement object is {config.game.placement_object}")
        if config.game.placement_object:
            deactivated = True
            self.placement_mode_on = False
            # need to kill to remove sprite from sprite list
            config.game.placement_object.destroy()
            # need to dereference to cause mouse to reappear
            config.game.placement_object = None
        return deactivated

    def pause_game(self):
        logger.info("pause game called")
        match self.state:
            case GAME_STATE.SIMULATION_PAUSED:
                self.set_state(GAME_STATE.GAMEPLAY)
            case GAME_STATE.GAMEPLAY:
                self.set_state(GAME_STATE.SIMULATION_PAUSED)

    def unpause_game(self):
        logger.info("unpausing game")
        self.set_state(GAME_STATE.GAMEPLAY)

    def is_running(self):
        return self.state == GAME_STATE.GAMEPLAY

    def handle_left_click(self):
        # TODO: this all needs to move to a mouse handler/cursor class
        print("HANDLING LEFT CLICK")
        mouse_pos = pygame.mouse.get_pos()
        # is this a UI menu click?
        if self.placement_mode_on:
            logging.info('placement mode is on')
            EventQueue.instance().publish("PROCESS_PLACEMENT_CLICK",[config.camera.apply_pos_vector(mouse_pos)])
        else:
            # print("found a sprite target")
            EventQueue.instance().publish("CHECK_FOR_SPRITE_CLICK",[config.camera.apply_pos_vector(mouse_pos)])

    def check_for_menu_click(self, position):
        # TODO: Move somewhere else, maybe gameplay_screen
        for entry in config.game.menu_sprites:
            if entry.show and entry.rect.collidepoint(position):
                if entry.label in config.definitions.structures:
                    logging.info(f'found a placeable: {entry.label}')
                    return entry
        return None

    def subscribe_to_events(self):
        EventQueue.instance().subscribe('SET_GAME_STATE',self.set_state)
        EventQueue.instance().subscribe('PAUSE_GAME',self.pause_game)
        EventQueue.instance().subscribe('UNPAUSE_GAME',self.unpause_game)
        EventQueue.instance().subscribe("RESET_PLACEMENT_MODE",self.reset_placement_mode)
